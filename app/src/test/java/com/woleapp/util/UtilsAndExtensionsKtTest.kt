package com.woleapp.util

import com.google.gson.JsonObject
import org.hamcrest.CoreMatchers
import org.junit.Assert.assertThat
import org.junit.Test
import java.util.*

class UtilsAndExtensionsKtTest {

    @Test
    fun getSixDigitRandomNumberTest() {
        for (i in 1..100) {
            val randomSixDigit = getSixDigitRandomNumber()
            println(randomSixDigit)
            assertThat(randomSixDigit.length, CoreMatchers.`is`(6))
        }
    }

    @Test
    fun encode() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("ref", "SC${getSixDigitRandomNumber()}")
        //jsonObject.addProperty("id", "11")
        jsonObject.addProperty("a_id", "b416c77f-a697-41f9-b6bd-ba40b80767c6")
        jsonObject.addProperty("amount", "30")
        //jsonObject.addProperty("p_name","Food")
        jsonObject.addProperty("type","card")
        //jsonObject.addProperty("")
        val data = jsonObject.toString()
        val encodedString =
            "https://paylink.netpluspay.com?p=${encodeQueryValue(
                Base64.getEncoder().encodeToString(data.toByteArray())
            )}"
        println(data)
        println("--------------------------------------------------------------------")
        println(encodedString)
        assertThat(encodedString, CoreMatchers.`is`(CoreMatchers.notNullValue()))
    }

}