package com.woleapp

import com.auth0.android.jwt.JWT
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will transferFunds on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val jwt = JWT("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdG9ybUlkIjoiYWEyMTYzMWUtNWUzNS0xMWVhLTk1N2MtZjIzYzkyOWIwMDU3IiwidGVybWluYWxJZCI6IjIwNThHVzUzIiwiYnVzaW5lc3NOYW1lIjoiZGFwb0B3ZWJtYWxsbmcuY29tIiwiaXNNZXJjaGFudCI6ZmFsc2UsImlzQWdlbnQiOnRydWUsImlzQWRtaW4iOmZhbHNlLCJyb2xlcyI6WyJhZ2VudCJdLCJhcHBuYW1lIjoic3Rvcm1fYXBwIiwicGVybWlzc2lvbnMiOlsicm9sZTphZ2VudCJdLCJpYXQiOjE1ODQwMTM4MTEsImV4cCI6MTU4NDAyODIxMSwiaXNzIjoic3Rvcm06YWNjb3VudHMiLCJzdWIiOiJhdXRoIn0.veHYof3ZjzB7fu3m_k-Jb_-rWn2ceuzG2sZcpRYqAVk");
        System.out.println(jwt.claims.get("stormId"));
        assertEquals(4, 2 + 2)
    }

}
