package com.woleapp.network

import com.google.gson.JsonObject
import com.woleapp.model.kayodeStormImplementation.* // ktlint-disable no-wildcard-imports
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.* // ktlint-disable no-wildcard-imports

interface NewStormApiService {
    // New Storm Service implementation
    @POST("/api/v1/transaction/{storm_id}")
    fun sendTransactionToServer(
        @Path("storm_id") stormId: String,
        @Body payload: JsonObject
    ): Single<Response<JsonObject>>

    @POST("/api/v1/auth/login")
    fun loginUser(
        @Body loginCredentials: StormLogin
    ): Observable<Response<StormLoginResponse>>

    @POST("/api/v1/auth/register")
    fun registerNewUser(
        @Body payload: JsonObject
    ): Observable<Response<Any>>

    @GET("/api/v1/wallet/{storm_id}")
    fun getWalletBalanceFromNewStormService(
        @Path("storm_id") stormId: String
    ): Observable<Response<GetStormWalletBalanceResponseBody>>

    @GET("/api/v1/banks/")
    fun getListOfBanksSupportedByeTransact(): Observable<Response<List<GetBankListResponse>>>

    @POST("/api/v1/wallet/getname")
    fun verifyAccountName(
        @Body payload: VerifyAccountNameRequestBody
    ): Observable<Response<VerifyAccountNameResponseBody>>

    @POST("/api/v1/wallet/{storm_id}")
    fun debitUserWallet(
        @Path("storm_id") stormId: String,
        @Body payload: DebitUserWalletRequestBody
    ): Single<Response<DebitUserWalletResponseBody>>

    @GET("/api/v1/transaction/{stormId}")
    fun getDebitTransactions(
        @Path("stormId") stormId: String,
        @Query("page") page: Int
    ): Single<GetTransactionsResponse>

    @GET("/api/v1/transaction/{stormId}")
    fun searchTransactionsByRrnOrReference(
        @Path("stormId") stormId: String,
        @Query("page") page: Int,
        @Query("rrn") rrn: String,
        @Query("reference") reference: String
    ): Single<FinalDataClassForSearch>

    @GET("/api/v1/transaction/{stormId}")
    fun searchTransactionsByDate(
        @Path("stormId") stormId: String,
        @Query("page") page: Int,
        @Query("dateLowerBound") startDate: String,
        @Query("dateUpperBound") EndDate: String
    ): Single<GetNewResponse>
}
