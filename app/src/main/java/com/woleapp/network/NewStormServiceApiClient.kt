package com.woleapp.network

import android.content.Context
import com.woleapp.BuildConfig
import com.woleapp.network.StormAPIClient.BASE_URL_FOR_NEW_STORM_SERVICE
import okhttp3.* // ktlint-disable no-wildcard-imports
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NewStormServiceApiClient {
    private var stormApiService: NewStormApiService? = null

    fun newInstance(): NewStormApiService? {
        return stormApiService
    }

    @Synchronized
    fun create(appToken: String, context: Context): NewStormApiService {
        if (stormApiService == null) {
            stormApiService =
                createRetrofit(providesOKHTTPClient(context, appToken)).create(
                    NewStormApiService::class.java
                )
        }
        return stormApiService!!
    }

    private fun createRetrofit(okhttp: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL_FOR_NEW_STORM_SERVICE)
            .client(okhttp)
            .build()

    private fun providesOKHTTPClient(
        context: Context,
        appToken: String
    ): OkHttpClient {
        val cacheSize = (5 * 1024 * 1024).toLong()
        val mCache = Cache(context.cacheDir, cacheSize)
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        }
        return if (BuildConfig.DEBUG) {
            OkHttpClient().newBuilder()
                .retryOnConnectionFailure(true)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build()
        } else {
            OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(loggingInterceptor)
                .build()
        }
    }

    // NEW STORM SERVICE IMPLEMENTATION STARTS HERE
//    fun loginToNewStormService(
//        credentials: StormLogin?,
//        context: Context?
//    ): Observable<retrofit2.Response<StormLoginResponse?>?>? {
//        return NewStormServiceApiClient.create("", context!!).loginUser(credentials)
//            ?.subscribeOn(Schedulers.io())
//            ?.observeOn(AndroidSchedulers.mainThread())
//    }

//    fun sendTransactionToServerViaNewStorm(
//        appToken: String?,
//        transaction: JsonObject?,
//        stormId: String?,
//        context: Context?
//    ): Observable<retrofit2.Response<JsonObject?>?>? {
//        return Objects.requireNonNull(
//            create(
//                appToken!!,
//                context!!
//            ).sendTransactionToServer(appToken, stormId, transaction)
//        )
//            ?.subscribeOn(Schedulers.io())
//            ?.observeOn(AndroidSchedulers.mainThread())
//    }
//
//    fun registerNewUser(
//        userData: JsonObject?,
//        context: Context?
//    ): Observable<retrofit2.Response<Any?>?>? {
//        return Objects.requireNonNull(
//            Objects.requireNonNull(
//                create(
//                    "",
//                    context!!
//                ).registerNewUser(userData)
//            )
//                ?.subscribeOn(Schedulers.io())
//                ?.observeOn(AndroidSchedulers.mainThread())
//        )
//    }
//
//    fun getWalletBalanceForNewStormService(
//        appToken: String?,
//        stormId: String?,
//        context: Context?
//    ): Observable<retrofit2.Response<GetStormWalletBalanceResponseBody?>?>? {
//        return Objects.requireNonNull(
//            Objects.requireNonNull(
//                create(
//                    appToken!!, context!!
//                ).getWalletBalanceFromNewStormService(
//                    stormId
//                )
//            )
//                ?.subscribeOn(Schedulers.io())
//                ?.observeOn(AndroidSchedulers.mainThread())
//        )
//    }
    // NEW STORM SERVICE IMPLEMENTATION ENDS HERE
}

class InterceptorForNewStormService(var appToken: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        chain.request()
        val request: Request = if (appToken.isNotEmpty()) {
            chain.request()
                .newBuilder()
                .addHeader("x-api-key", BuildConfig.NEW_STORM_SERVICE_KEY)
                .addHeader("Authorization", appToken)
                .build()
        } else {
            chain.request()
                .newBuilder()
                .addHeader("x-api-key", BuildConfig.NEW_STORM_SERVICE_KEY)
                .build()
        }
        return chain.proceed(request)
    }
}
