package com.woleapp.network

import com.woleapp.model.GetPartnerInterSwitchThresholdResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface GetThresholdService {
    @GET("/partners/{partnerId}/isw_threshold")
    fun getPartnerInterSwitchThreshold(
        @Path("partnerId") partnerId: String
    ): Single<GetPartnerInterSwitchThresholdResponse?>
}
