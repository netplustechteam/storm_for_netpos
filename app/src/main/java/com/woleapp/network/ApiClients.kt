package com.woleapp.network

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.woleapp.BuildConfig
import com.woleapp.util.SharedPrefManager
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

object StormUtilitiesApiClient {

    // private const val BASE_URL_TEST = "http://storm-utilities.test.netpluspay.com/"
    // private const val BASE_URL_LIVE = "https://storm-utilities.netpluspay.com/
    private const val BASE_URL = BuildConfig.BASE_URL_STORM_UTILITIES

    @Volatile
    private var INSTANCE: StormUtilitiesApiService? = null
    fun getStormUtilitiesApiClientInstance(context: Context): StormUtilitiesApiService {
        return INSTANCE ?: synchronized(this) {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getOkHttpClient(context))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(StormUtilitiesApiService::class.java)
                .also {
                    INSTANCE = it
                }
        }
    }
}

object SmsApiClient {
    private var smsServiceInstance: SmsService? = null
    fun getSmsServiceInstance(): SmsService = smsServiceInstance ?: synchronized(this) {
        smsServiceInstance ?: Retrofit.Builder()
            .baseUrl("https://sms.netpluspay.com")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(SmsService::class.java)
            .also {
                smsServiceInstance = it
            }
    }
}

fun getCustomGsonObject(): Gson = GsonBuilder()
    .run {
        excludeFieldsWithoutExposeAnnotation()
        create()
    }

@JvmOverloads
fun getOkHttpClient(context: Context? = null): OkHttpClient = OkHttpClient.Builder()
    .addInterceptor(
        HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        }
    )
    .addInterceptor(
        HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.HEADERS)
        }
    )
    .addInterceptor(TokenInterceptor())
    .build()

@JvmOverloads
fun getOkHttpClient(longTimeOut: Boolean): OkHttpClient = OkHttpClient.Builder()
    .addInterceptor(
        HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        }
    )
    .addInterceptor(
        HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.HEADERS)
        }
    )
    .addInterceptor(TokenInterceptor())
    .writeTimeout(60, TimeUnit.SECONDS)
    .readTimeout(60, TimeUnit.SECONDS)
    .connectTimeout(60, TimeUnit.SECONDS)
    .build()

object MerchantsApiClient {
    // private const val BASE_URL_LIVE = "https://storm-merchants.netpluspay.com"
    // private const val BASE_URL = "http://storm-merchants.test.netpluspay.com"
    private const val BASE_URL = BuildConfig.BASE_URL_STORM_MERCHANTS

    @Volatile
    private var INSTANCE: MerchantsApiService? = null

    @JvmStatic
    fun getMerchantApiService(context: Context): MerchantsApiService {
        return INSTANCE ?: synchronized(this) {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getOkHttpClient(context))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getCustomGsonObject()))
                .build()
                .create(MerchantsApiService::class.java)
                .also {
                    INSTANCE = it
                }
        }
    }

    private fun getBaseOkhttpClientBuilder(): OkHttpClient.Builder {
        val okHttpClientBuilder = OkHttpClient.Builder()
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okHttpClientBuilder.addInterceptor(loggingInterceptor)
        return okHttpClientBuilder
    }

    private const val BASE_URL_FOR_LOGGING_TO_BACKEND = "https://device.netpluspay.com/"
    private var LOGGING_INSTANCE: MerchantsApiService? = null
    fun getStormApiLoginInstance(): MerchantsApiService = LOGGING_INSTANCE ?: synchronized(this) {
        LOGGING_INSTANCE ?: Retrofit.Builder()
            .baseUrl(BASE_URL_FOR_LOGGING_TO_BACKEND)
            .client(getBaseOkhttpClientBuilder().build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MerchantsApiService::class.java)
            .also {
                LOGGING_INSTANCE = it
            }
    }
}

object HealthCheckerClient {
    private const val BASE_URL = "http://healthcheck.test.netpluspay.com/api/"
    private const val BASE_URL_TEMP = "https://1aa4-197-210-64-216.ngrok.io/api/"
    private const val BASE_URL_LIVE = "https://dokitoronline.com/api/"
    @Volatile
    private var INSTANCE: HealthCheckerServices? = null

    @JvmStatic
    fun getHealthCheckerInstance(): HealthCheckerServices = INSTANCE ?: synchronized(this) {
        Retrofit.Builder()
            .client(getOkHttpClient(longTimeOut = true))
            .baseUrl(BASE_URL_LIVE)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(HealthCheckerServices::class.java)
            .also {
                INSTANCE = it
            }
    }
}

class TokenInterceptor(context: Context? = null) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val token: String? = SharedPrefManager.getUserToken()
        val request = chain.request()
        val method = request.method
        if (method == "POST" || method == "PUT") {
            val reqBody = request.body!!
            val contentLength = reqBody.contentLength()
        }
        // Log.e("interceptor", "token $token")
        // val headersInReq = request.headers()
        // Log.e("interceptor", "headers: ${headersInReq.size()}")
        // headersInReq.names().forEach { Log.e("Header", "$it : ${headersInReq.get(it)}") }
        val response = chain.proceed(
            request.newBuilder().run {
                token?.let {
                    addHeader(
                        "Authorization",
                        "Bearer $it"
                    )
                }
                build()
            }
        )
        val body = response.body
        val bodyString = body?.string()
        Timber.e(bodyString!!)
        return response.newBuilder().body(ResponseBody.create(body.contentType(), bodyString))
            .build()
    }
}
