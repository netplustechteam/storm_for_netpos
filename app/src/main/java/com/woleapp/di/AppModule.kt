package com.woleapp.di

import android.content.Context
import com.google.gson.Gson
import com.woleapp.BuildConfig
import com.woleapp.db.AppDatabase
import com.woleapp.db.dao.TransactionsDao
import com.woleapp.network.GetThresholdService
import com.woleapp.network.NewStormApiService
import com.woleapp.network.StormAPIClient.BASE_URL_FOR_GETTING_THRESHOLD
import com.woleapp.network.StormAPIClient.BASE_URL_FOR_NEW_STORM_SERVICE
import com.woleapp.util.SharedPrefManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    @Named("defaultApiService")
    fun providesNewStormApiService(
        @Named("defaultRetrofit") retrofit: Retrofit
    ): NewStormApiService =
        retrofit.create(NewStormApiService::class.java)

    @Provides
    @Singleton
    @Named("getThresholdService")
    fun providesNewGetThresholdService(
        @Named("thresholdRetrofit") retrofit: Retrofit
    ): GetThresholdService =
        retrofit.create(GetThresholdService::class.java)

    @Provides
    @Singleton
    @Named("defaultUrl")
    fun providesBaseUrl(): String = BASE_URL_FOR_NEW_STORM_SERVICE

    @Provides
    @Singleton
    @Named("thresholdBaseUrl")
    fun providesBaseUrlForGettingThreshold(): String = BASE_URL_FOR_GETTING_THRESHOLD

    @Provides
    @Singleton
    @Named("defaultRetrofit")
    fun providesRetrofit(
        @Named("defaultOkHttp") okhttp: OkHttpClient,
        @Named("defaultUrl") baseUrl: String
    ): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(baseUrl)
            .client(okhttp)
            .build()

    @Provides
    @Singleton
    @Named("thresholdRetrofit")
    fun providesRetrofitForGettingThreshold(
        @Named("thresholdOkHttp") okhttp: OkHttpClient,
        @Named("thresholdBaseUrl") baseUrl: String
    ): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(baseUrl)
            .client(okhttp)
            .build()

    @Provides
    @Singleton
    @Named("defaultOkHttp")
    fun providesOKHTTPClient(
        @Named("loggingInterceptor") loggingInterceptor: Interceptor,
        @Named("headerInterceptor") headerInterceptor: Interceptor
    ): OkHttpClient =
        OkHttpClient().newBuilder()
            .connectTimeout(200, TimeUnit.SECONDS)
            .readTimeout(200, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(headerInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()

    @Provides
    @Singleton
    @Named("thresholdOkHttp")
    fun providesOKHTTPClientForGettingThreshold(
        @Named("loggingInterceptor") loggingInterceptor: Interceptor,
        @Named("headerInterceptor") headerInterceptor: Interceptor
    ): OkHttpClient =
        OkHttpClient().newBuilder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(headerInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()

    @Provides
    @Singleton
    @Named("loggingInterceptor")
    fun providesLoggingInterceptor(): Interceptor = HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Singleton
    @Named("headerInterceptor")
    fun providesHeaderInterceptor(): Interceptor = Interceptor { chain ->
        val originalRequest = chain.request()
        val userToken = SharedPrefManager.getUserToken() ?: ""
        val requestWithInterceptor = originalRequest.newBuilder()
            .addHeader("x-api-key", BuildConfig.NEW_STORM_SERVICE_KEY)
            .addHeader("Authorization", userToken)
            .build()
        chain.proceed(requestWithInterceptor)
    }

    @Provides
    @Singleton
    fun providesAppDatabase(@ApplicationContext context: Context): AppDatabase =
        AppDatabase.getInstance(context)

    @Provides
    @Singleton
    fun providesLocalDbDao(appDb: AppDatabase): TransactionsDao = appDb.agentTransactionDao()

    @Provides
    @Singleton
    fun providesGson(): Gson = Gson()
}
