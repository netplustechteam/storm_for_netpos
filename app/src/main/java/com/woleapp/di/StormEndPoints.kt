package com.woleapp.di

/*

SEND TRANSACTION TO STORM
@POST("/api/v1/transaction/{storm_id}")

LOGIN
@POST("/api/v1/auth/login")

REGISTER
@POST("/api/v1/auth/register")

GET WALLET BALANCE
@GET("/api/v1/wallet/{storm_id}")

@GET("/api/v1/banks/")
GET LIST OF BANKS SUPPORTED BY e-Transact

@POST("/api/v1/wallet/getname")
VERIFY ACCOUNT NAME
request Payload
{
    "accountNumber" : "String",
    "bankCode" : "String",
    "stormId" : "String"
}


@POST("/api/v1/wallet/{storm_id}")
DEBIT USER WALLET
Request Payload
{
    "accountNumber" : "String",
    "amount" : 7000,
    "amount" : "String",
    "endPoint" : "String",
    "pin" : "String",
    "recieverName" : "String",
    "senderName" : "String",
    "stormId" : "String"
}


@GET("/api/v1/transaction/{stormId}")
GET TRANSACTIONS DONE BY USER

@GET("/partners/{partnerId}/isw_threshold")
GET PARTNER'S INTERSWITCH THRESHOLD


 */
