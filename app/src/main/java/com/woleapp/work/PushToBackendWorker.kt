package com.woleapp.work

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.woleapp.db.AppDatabase
import com.woleapp.db.dao.TransactionsDao
import com.woleapp.model.TrackTransactionTable
import com.woleapp.model.mapToStormStructure
import com.woleapp.network.NewStormApiService
import com.woleapp.util.SharedPrefManager
import com.woleapp.util.Singletons
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class PushToBackendWorker(
    context: Context,
    workParams: WorkerParameters
) : Worker(context, workParams) {
    private val stormClientForWorker: NewStormApiService =
        StormClientForWorker().create()
    private val transDao: TransactionsDao = getTransDao(context)
    private val appXApiKey = SharedPrefManager.getXapiKey()
    private val user = Singletons.getCurrentlyLoggedInUser()
    private val compositeDisposable = CompositeDisposable()

    override fun doWork(): Result {
        // Query the Database to check for the onces that hasn't been pushed successfully
        val transYetToSaveToBackend =
            transDao.getAllTrackingTransactions()
        var counter = transYetToSaveToBackend.size

        Timber.d("DATA_TO_SEND_TO_BACK_END%s", Gson().toJson(transYetToSaveToBackend))
        Timber.d("SIZE_DATA_TO_SEND_TO_BACK_END%s", counter.toString())

        transYetToSaveToBackend.forEach {
            sendOneTransactionToServer(it) {
                --counter
            }
        }

        /*

        {"status":"PENDING","transactionResponse":{"AID":"","STAN":"960840","TSI":"","TVR":"","accountType":"SAVINGS","acquiringInstCode":"","additionalAmount_54":"","amount":900,"appCryptogram":"","authCode":"","cardExpiry":"2412","cardHolder":"ADEBAYO/OLOYEDE","cardLabel":"Master card","id":0,"localDate_13":"0909","localTime_12":"121003","maskedPan":"5399834420826497","merchantId":"4ea02209-14ad-42e4-893f-2f7502e9bb20","originalForwardingInstCode":"","otherAmount":0,"otherId":"","responseCode":"99","responseDE55":"","rrn":"368256847873","terminalId":"2035BVV7","transactionTimeInMillis":916989448,"transactionType":"PURCHASE","transmissionDateTime":"2022-09-09 12:15:17"}}


        {"rrn":"368256847873","status":"Not sufficent funds","transactionResponse":{"AID":"","STAN":"121517","TSI":"","TVR":"","accountType":"SAVINGS","acquiringInstCode":"200002","additionalAmount_54":"1053566C000000000000","amount":900,"appCryptogram":"","authCode":"608612","cardExpiry":"2412","cardHolder":"ADEBAYO/OLOYEDE","cardLabel":"Master card","id":0,"localDate_13":"0909","localTime_12":"121517","maskedPan":"539983xxxxxx6497","merchantId":"2035LA121032755","originalForwardingInstCode":"627821","otherAmount":0,"otherId":"","responseCode":"51","responseDE55":"9F3602044D910a50D1FF585192E3E50000","rrn":"220909121517","terminalId":"2035BVV7","transactionTimeInMillis":569773800,"transactionType":"PURCHASE","transmissionDateTime":"2022-09-09 12:15:19"}}
        * */

//        compositeDisposable.add(
//            transDao.deleteAllConfirmedTransactions()
//                .subscribeOn(Schedulers.io())
//                .subscribe { t1, t2 ->
//                    t1?.let {
//                    }
//                    t2?.let {
//                        Timber.d("DELETE_ERROR%s", it.localizedMessage)
//                    }
//                }
//        )

        return if (transDao.getAllTrackingTransactions().isEmpty() && counter == 0) {
            Result.success()
        } else Result.retry()
    }

    private fun providesAppDatabase(context: Context): AppDatabase =
        AppDatabase.getInstance(context)

    private fun getTransDao(context: Context): TransactionsDao =
        providesAppDatabase(context).agentTransactionDao()

    override fun onStopped() {
        super.onStopped()
        compositeDisposable.clear()
    }

    private fun sendOneTransactionToServer(
        transaction: TrackTransactionTable,
        decrementCount: () -> Unit
    ) {
        val dataToLog = transaction.mapToStormStructure(
            appXApiKey,
            transaction.routingChannel,
            user!!.netplus_id,
            transaction.transactionStatus,
            user.user_type_for_new_storm_service
        )
        compositeDisposable.add(
            stormClientForWorker.sendTransactionToServer(
                user.netplus_id,
                dataToLog
            ).subscribeOn(Schedulers.io())
                .subscribe { t1, t2 ->
                    t1?.let {
                        Timber.d("DATA_FROM_WORKER==> ${Gson().toJson(dataToLog)}")
                        if (it.code() == 200 || it.code() == 409) {
                            transDao.deleteSingleConfirmedTransactions(transaction) // Deletes the transaction from the tracking table
                            decrementCount()
                        }
                    }
                    t2?.let {
                        Timber.d("SEND_TRANS_TO_BACKEND_ERROR%s", it.localizedMessage)
                    }
                }
        )
    }
}
