package com.woleapp.work

import com.woleapp.BuildConfig
import com.woleapp.network.NewStormApiService
import com.woleapp.network.StormAPIClient.BASE_URL_FOR_NEW_STORM_SERVICE
import com.woleapp.util.SharedPrefManager
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class StormClientForWorker {

    private var apiService: NewStormApiService? = null

    @Synchronized
    fun create(): NewStormApiService {
        if (apiService == null) {
            apiService = Retrofit.Builder()
                .baseUrl(BASE_URL_FOR_NEW_STORM_SERVICE)
                .client(providesOKHTTPClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(NewStormApiService::class.java)
        }
        return apiService!!
    }

    private fun providesOKHTTPClient(): OkHttpClient =
        OkHttpClient().newBuilder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(providesHeaderInterceptor())
            .addInterceptor(providesLoggingInterceptor())
            .build()

    private fun providesLoggingInterceptor(): Interceptor = HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    private fun providesHeaderInterceptor(): Interceptor = Interceptor { chain ->
        val originalRequest = chain.request()
        val userToken = SharedPrefManager.getUserToken() ?: ""
        val requestWithInterceptor = originalRequest.newBuilder()
            .addHeader("x-api-key", BuildConfig.NEW_STORM_SERVICE_KEY)
            .addHeader("Authorization", userToken)
            .build()
        chain.proceed(requestWithInterceptor)
    }
}
