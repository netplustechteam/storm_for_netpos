package com.woleapp.work

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.woleapp.mqtt.MqttHelper
import timber.log.Timber

class StormWork(val context: Context, workerParameters: WorkerParameters) : Worker(
    context,
    workerParameters
) {
    override fun doWork(): Result {
        Timber.e("doing work")
        //createNotification(context, "Hello", "Doing work", null)
        MqttHelper.init<Nothing>(context = context)
        //check last time and call home here
        return Result.success()
    }
}