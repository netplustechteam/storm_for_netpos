@file:Suppress("DEPRECATION")

package com.woleapp.kozenext

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.view.LayoutInflater
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.danbamitale.epmslib.entities.CardData
import com.danbamitale.epmslib.utils.IsoAccountType
import com.netpluspay.netpossdk.emv.CardReaderEvent
import com.netpluspay.netpossdk.emv.CardReaderService
import com.woleapp.R
import com.woleapp.databinding.DialogSelectAccountTypeBinding
import com.woleapp.util.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

data class ICCCardHelper(
    val customerName: String? = null,
    val cardScheme: String? = null,
    var accountType: IsoAccountType? = null,
    val cardData: CardData? = null,
    val error: Throwable? = null
)

fun showCardDialog(
    context: Activity,
    amount: Long,
    cashBackAmount: Long
): LiveData<Event<ICCCardHelper>> {
    val liveData = MutableLiveData<Event<ICCCardHelper>>()
    val dialog = ProgressDialog(context)
        .apply {
            setMessage("Waiting for card")
            setCancelable(false)
        }
    val cardService = CardReaderService(context)
    var iccCardHelper: ICCCardHelper? = null
    val c = cardService.initiateICCCardPayment(
        amount,
        cashBackAmount
    )
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
            when (it) {
                is CardReaderEvent.CardRead -> {
                    val cardResult = it.data
                    val card = CardData(
                        track2Data = cardResult.track2Data!!,
                        nibssIccSubset = cardResult.nibssIccSubset,
                        panSequenceNumber = cardResult.applicationPANSequenceNumber!!,
                        posEntryMode = "051"
                    )
                    if (cardResult.encryptedPinBlock.isNullOrEmpty().not()) {
                        card.apply {
                            pinBlock = cardResult.encryptedPinBlock
                        }
                    }
                    iccCardHelper = ICCCardHelper(
                        customerName = cardResult.cardHolderName,
                        cardScheme = cardResult.cardScheme,
                        cardData = card
                    )
//                        val cardReaderMqttEvent = CardReaderMqttEvent(
//                            cardExpiry = cardResult.expirationDate,
//                            cardHolder = cardResult.cardHolderName,
//                            maskedPan = StringUtils.overlay(
//                                cardResult.applicationPANSequenceNumber,
//                                "xxxxxx",
//                                6,
//                                12
//                            )
//                        )
//                        viewModel.sendCardEvent("SUCCESS", "00", cardReaderMqttEvent)
                }
                is CardReaderEvent.CardDetected -> {
                    dialog.setMessage("Reading Card Please Wait")
                }
            }
        }, {
            it?.let {
//                    val cardReaderMqttEvent = CardReaderMqttEvent(readerError = it.localizedMessage)
//                    viewModel.sendCardEvent("ERROR", "99", cardReaderMqttEvent)
                dialog.dismiss()
                liveData.value = Event(ICCCardHelper(error = it))
            }
        }, {
            dialog.dismiss()
            showSelectAccountTypeDialog(context, iccCardHelper!!, liveData)
        })

    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Stop") { d, _ ->
        cardService.transEnd(message = "Cancelled")
        d.dismiss()
    }
    dialog.show()
    return liveData
}

private fun showSelectAccountTypeDialog(
    context: Activity,
    iccCardHelper: ICCCardHelper,
    liveData: MutableLiveData<Event<ICCCardHelper>>
) {
    var dialogSelectAccountTypeBinding: DialogSelectAccountTypeBinding
    val dialog = AlertDialog.Builder(context)
        .apply {
            dialogSelectAccountTypeBinding =
                DialogSelectAccountTypeBinding.inflate(LayoutInflater.from(context), null, false)
                    .apply {
                        executePendingBindings()
                    }
            setView(dialogSelectAccountTypeBinding.root)
            setCancelable(false)
        }.create()
    dialogSelectAccountTypeBinding.accountTypes.setOnCheckedChangeListener { _, checkedId ->
        val accountType = when (checkedId) {
            R.id.savings_account -> IsoAccountType.SAVINGS
            R.id.current_account -> IsoAccountType.CURRENT
            R.id.credit_account -> IsoAccountType.CREDIT
            R.id.bonus_account -> IsoAccountType.BONUS_ACCOUNT
            R.id.investment_account -> IsoAccountType.INVESTMENT_ACCOUNT
            R.id.universal_account -> IsoAccountType.UNIVERSAL_ACCOUNT
            else -> IsoAccountType.DEFAULT_UNSPECIFIED
        }
        dialog.dismiss()
        if (accountType != IsoAccountType.DEFAULT_UNSPECIFIED) {
            iccCardHelper.apply {
                this.accountType = accountType
            }
            liveData.value = Event(iccCardHelper)
        }
    }
    dialogSelectAccountTypeBinding.cancelButton.setOnClickListener {
        dialog.dismiss()
        liveData.value = Event(ICCCardHelper(error = Throwable("Operation was canceled")))
    }
    dialog.show()
}
