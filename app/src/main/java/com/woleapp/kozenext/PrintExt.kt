package com.woleapp.kozenext

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.danbamitale.epmslib.entities.TransactionResponse
import com.danbamitale.epmslib.entities.responseMessage
import com.danbamitale.epmslib.extensions.formatCurrencyAmount
import com.netpluspay.netpossdk.NetPosSdk.getPrinterManager
import com.netpluspay.netpossdk.printer.PrinterResponse
import com.netpluspay.netpossdk.printer.ReceiptBuilder
import com.pos.sdk.printer.POIPrinterManage
import com.pos.sdk.printer.models.BitmapPrintLine
import com.pos.sdk.printer.models.PrintLine
import com.pos.sdk.printer.models.TextPrintLine
import com.woleapp.BuildConfig
import com.woleapp.R
import com.woleapp.model.Transactions
import com.woleapp.model.TransferModel
import com.woleapp.network.soap.request.FundsTransferRequestModel
import com.woleapp.util.SharedPrefManager
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.* // ktlint-disable no-wildcard-imports

private fun printReceipt(
    context: Context,
    name: String,
    requestModel: FundsTransferRequestModel,
    status: String
) {
    val printerManage = getPrinterManager(context)
    printerManage.setLineSpace(1)
    printerManage.setPrintGray(3000)
    var bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.logo)
    bitmap = Bitmap.createScaledBitmap(bitmap!!, 120, 120, false)
    val bitmapPrintLine = BitmapPrintLine()
    bitmapPrintLine.setType(PrintLine.BITMAP)
    bitmapPrintLine.bitmap = bitmap
    bitmapPrintLine.setPosition(PrintLine.CENTER)
    printerManage.addPrintLine(bitmapPrintLine)
    printerManage.addPrintLine(getPrintLine("", PrintLine.CENTER, TextPrintLine.FONT_NORMAL, false))
    printerManage.addPrintLine(
        getPrintLine(
            "TRANSFER",
            PrintLine.CENTER,
            TextPrintLine.FONT_LARGE,
            true
        )
    )
    printerManage.addPrintLine(getPrintLine("", PrintLine.CENTER, TextPrintLine.FONT_NORMAL, false))
    printerManage.addPrintLine(
        getPrintLine(
            "Merchant: ",
            PrintLine.CENTER,
            TextPrintLine.FONT_NORMAL,
            false
        )
    )
    printerManage.addPrintLine(
        getPrintLine(
            "Destination Account No: " + requestModel.transaction.destination,
            PrintLine.LEFT,
            TextPrintLine.FONT_NORMAL,
            false
        )
    )
    printerManage.addPrintLine(
        getPrintLine(
            "Destination Account Name: $name",
            PrintLine.LEFT,
            TextPrintLine.FONT_NORMAL,
            false
        )
    )
    printerManage.addPrintLine(
        getPrintLine(
            "Amount " + requestModel.transaction.amount,
            PrintLine.LEFT,
            TextPrintLine.FONT_NORMAL,
            false
        )
    )
    printerManage.addPrintLine(
        getPrintLine(
            "Reference " + requestModel.transaction.reference,
            PrintLine.LEFT,
            TextPrintLine.FONT_NORMAL,
            false
        )
    )
    printerManage.addPrintLine(
        getPrintLine(
            "Description " + requestModel.transaction.description,
            PrintLine.LEFT,
            TextPrintLine.FONT_NORMAL,
            false
        )
    )
    printerManage.addPrintLine(getPrintLine("", PrintLine.CENTER, TextPrintLine.FONT_NORMAL, false))
    printerManage.addPrintLine(
        getPrintLine(
            status,
            PrintLine.CENTER,
            TextPrintLine.FONT_LARGE,
            true
        )
    )
    printerManage.addPrintLine(getPrintLine("", PrintLine.CENTER, TextPrintLine.FONT_NORMAL, false))
    printerManage.addPrintLine(
        getPrintLine(
            "Powered By Netplus",
            PrintLine.CENTER,
            TextPrintLine.FONT_NORMAL,
            false
        )
    )
    printerManage.beginPrint(object : POIPrinterManage.IPrinterListener {
        override fun onError(p0: Int, p1: String?) {
        }

        override fun onFinish() {
        }

        override fun onStart() {
        }
    })
}

private fun getPrintLine(text: String, position: Int, font: Int, isBold: Boolean): TextPrintLine? {
    val textPrintLine = TextPrintLine()
    textPrintLine.setType(PrintLine.TEXT)
    textPrintLine.content = text
    textPrintLine.isBold = isBold
    textPrintLine.isItalic = false
    textPrintLine.setPosition(position)
    textPrintLine.size = font
    return textPrintLine
}

fun List<TransactionResponse>.printAll(
    context: Context,
    isMerchantCopy: Boolean
): Observable<PrinterResponse> {
    var emitter: ObservableEmitter<PrinterResponse>? = null
    val printerListener = object : POIPrinterManage.IPrinterListener {
        override fun onError(p0: Int, p1: String?) {
            emitter?.let {
                if (it.isDisposed.not())
                    it.onError(Throwable("message: $p1 - code: $p0"))
            }
        }

        override fun onFinish() {
            emitter?.let {
                if (it.isDisposed.not())
                    it.onNext(PrinterResponse())
            }
        }

        override fun onStart() {
        }
    }
    return Observable.create {
        emitter = it
        forEach { transactionResponse ->
            if (it.isDisposed.not())
                transactionResponse.print(context, printerListener, isMerchantCopy)
        }
        it.onComplete()
    }
}

fun TransactionResponse.buildSMSText(): StringBuilder = StringBuilder().apply {
    append("POS $transactionType ${if (responseCode == "00") "Approved" else "Declined"}\n\n")
    append("Response Code: $responseCode\n")
    append(
        "Message: ${
        try {
            responseMessage
        } catch (e: java.lang.Exception) {
            ""
        }
        }\n"
    )
    append("Amount: ${amount.div(100).formatCurrencyAmount("\u20A6")}\n")
    append("Date/Time: ${transactionTimeInMillis.formatDate()}\n")
    append("Auth Code: $authCode\n")
    append("RRN: $RRN\n")
    append("STAN: $STAN\n")
    append("Card: $cardLabel - $maskedPan\n")
    append("Card Owner: $cardHolder\n")
    append("Merchant: ${SharedPrefManager.getUser()?.business_name}\n")
    append("Terminal ID: $terminalId\n")
}

fun TransactionResponse.print(
    context: Context,
    printerListener: POIPrinterManage.IPrinterListener,
    isMerchantCopy: Boolean = true
) {
    buildReceipt(isMerchantCopy = isMerchantCopy, context = context).print(printerListener)
}

fun TransactionResponse.builder() = StringBuilder().apply {
    append("Merchant Name: ").append(SharedPrefManager.getUser().business_name)
    append("\nTERMINAL ID: ").append(terminalId).append("\n")
    append(transactionType).append("\n")
    append("DATE/TIME: ").append(transmissionDateTime).append("\n")
    append("AMOUNT: ").append(amount.div(100).formatCurrencyAmount("\u20A6")).append("\n")
    append(cardLabel).append(" Ending with").append(maskedPan.substring(maskedPan.length - 4))
        .append("\n")
    append("RESPONSE CODE: ").append(responseCode).append("\n").append(
        " : ${
        try {
            responseMessage
        } catch (ex: Exception) {
            "Error"
        }
        }"
    )
}

fun TransactionResponse.print(
    context: Context,
    description: String = "",
    isMerchantCopy: Boolean = false
) =
    buildReceipt(context, description, isMerchantCopy).print()

fun TransactionResponse.buildReceipt(
    context: Context,
    description: String = "",
    isMerchantCopy: Boolean = false
) =
    ReceiptBuilder(
        getPrinterManager(context).apply {
            cleanCache()
            setPrintGray(2000)
            setLineSpace(1)
        }
    ).also { builder ->
        val appVersion = if(BuildConfig.FLAVOR.contains("agency", true)) "Storm For Agents" else "Storm for Merchants"
        builder.appendLogo(BitmapFactory.decodeResource(context.resources, R.drawable.logo_black))
        builder.appendAID(AID)
        builder.appendAmount(
            amount.div(100).formatCurrencyAmount("\u20A6")
        )
        builder.appendDescription(description)
        builder.appendAppName(appVersion)
        builder.appendAppVersion(BuildConfig.VERSION_NAME)
        builder.appendAuthorizationCode(authCode)
        builder.appendCardHolderName(cardHolder)
        builder.appendCardNumber(maskedPan)
        builder.appendCardScheme(cardLabel)
        builder.appendDateTime(transactionTimeInMillis.formatDate())
        builder.appendRRN(RRN)
        builder.appendStan(STAN)
        builder.appendTerminalId(terminalId)
        builder.appendTransactionType(transactionType.name)
        builder.appendTransactionStatus(if (responseCode == "00") "Approved" else "Declined")
        builder.appendResponseCode(
            "${responseCode}\nMessage: ${
            try {
                responseMessage
            } catch (ex: Exception) {
                "Error"
            }
            }"
        )
        if (isMerchantCopy)
            builder.isMerchantCopy
        else builder.isCustomerCopy
    }

fun TransferModel.builder(): StringBuilder {
    Timber.e("amount $amount")
    return StringBuilder().apply {
        append("TRANSFER\n\n")
        append("Merchant: ").append(sourceName)
        append("\nDest Acc No: ").append(destinationAccount)
        append("\nDest Acc Name: ").append(destinationName)
        append("\nAmount: ").append(amount)
        append("\n..............${if (status == true) "SUCCESS" else "FAILED"}............\n")
        append(message)
    }
}

fun Transactions.builder(): StringBuilder {
    return StringBuilder().apply {
        append(transaction_type.uppercase() + "\n")
        if (BuildConfig.FLAVOR.contains("agency")) {
            append("Agent: ${SharedPrefManager.getUser().business_name}")
        } else {
            append("Merchant: ${SharedPrefManager.getUser().business_name}")
        }
        append("\nTerminal Id: ${SharedPrefManager.getUser().terminal_id}")
        append("\nAmount: \u20A6 $amount")
        append("\n..............${status.uppercase()}..............\n")
        append("\nReference: ${if (reference_no_Etranzact != null) transaction_id else reference_no_Etranzact}")
        append("\nDate: $transaction_date")
        if (reference_no_Etranzact != null) {
            append("\nDest. Acc. No: $destination_account")
            append("\nDest. Acc. Name: $beneficiary_name")
        }
        description?.run {
            append("\nDescription: $description")
        } ?: append("\nDescription: N.A")

        val appVariant = if (BuildConfig.FLAVOR.contains("agency", true)) "AGENTS" else "MERCHANTS"
        append("\nSTORM FOR $appVariant ${BuildConfig.VERSION_NAME}")
        append("Powered By Netplus")
        append("...REPRINT...")
    }
}

private fun Transactions.reprintTransactionsReceiptBuilder(
    context: Context,
    isMerchantCopy: Boolean = false
): ReceiptBuilder {
    val formattedAmount = amount.toString()
    return ReceiptBuilder(
        getPrinterManager(context).apply {
            cleanCache()
            setPrintGray(2000)
            setLineSpace(1)
        }
    ).apply {
        appendLogo(BitmapFactory.decodeResource(context.resources, R.drawable.logo_black))
        appendTextEntityLargeFontCenter(SharedPrefManager.getUser().business_name)
        appendTextEntity("Terminal Id: ${SharedPrefManager.getUser().terminal_id}")
        appendTextEntityFontSixteenCenter(transaction_type.uppercase())
        if (BuildConfig.FLAVOR.contains("agency")) {
            appendTextEntity("Agent: ${SharedPrefManager.getUser().business_name}")
        } else {
            appendTextEntity("Merchant: ${SharedPrefManager.getUser().business_name}")
        }
        // ADD DESCRIPTION
        description?.run {
            appendTextEntity("Description: $description")
        } ?: appendTextEntity("Description: N.A")

        appendTextEntity("Amount: \u20A6 $formattedAmount")
        appendTextEntityFontSixteenCenter(".....${status.uppercase()}.....")
        appendTextEntity("Reference: ${if (reference_no_Etranzact != null) transaction_id else reference_no_Etranzact}")
        appendTextEntity("Date: ${transaction_date.replace("T", "  ").replace(".000Z", "")}")
        if (reference_no_Etranzact != null) {
            appendTextEntity("Dest. Acc. No: $destination_account")
            appendTextEntity("Dest. Acc. Name: $beneficiary_name")
        }
        val appVariant = if (BuildConfig.FLAVOR.contains("agency", true)) "AGENTS" else "MERCHANTS"
        appendTextEntityCenter("STORM FOR $appVariant ${BuildConfig.VERSION_NAME}")
        appendTextEntityBold("Powered By Netplus")
        if (isMerchantCopy) appendTextEntityCenter("**** MERCHANT ****") else appendTextEntityCenter(
            "**** CUSTOMER COPY ****"
        )
        appendTextEntityCenter("REPRINT")
    }
}

fun Transactions.printMethod(
    context: Context,
    askBeforePrinting: String? = "",
    showReceiptDialog: () -> Unit
) {
    when (askBeforePrinting) {
        "customer" -> {
            reprintTransactionsReceiptBuilder(context, false).print()
        }
        "merchant" -> {
            reprintTransactionsReceiptBuilder(context, true).print()
        }
        "both" -> {
            reprintTransactionsReceiptBuilder(context, true).print()
            reprintTransactionsReceiptBuilder(context, false).print()
        }
    }
}

fun Transactions.reprintTransaction(context: Context) =
    reprintTransactionsReceiptBuilder(context).print()

fun TransferModel.print(context: Context) = buildTransferReceipt(context).print()

fun TransferModel.buildTransferReceipt(context: Context): ReceiptBuilder {
    Timber.e("amount $amount")
    val aaa = amount
    Timber.e("aaa $aaa")
    return ReceiptBuilder(
        getPrinterManager(context).apply {
            cleanCache()
            setPrintGray(2000)
            setLineSpace(1)
        }
    ).apply {
        appendLogo(BitmapFactory.decodeResource(context.resources, R.drawable.logo_black))
        appendTextEntityFontSixteenCenter("TRANSFER\n\n")
        if (BuildConfig.FLAVOR.contains("Agency", true))
            appendTextEntity("Agent: $sourceName")
        else
            appendTextEntity("Merchant: $sourceName")
        appendTextEntity("Dest Acc No: $destinationAccount")
        appendTextEntity("Dest Acc Name: $destinationName")
        appendTextEntity(
            "Amount: \u20A6 $aaa"
        )
        appendTextEntityFontSixteenCenter(".......${if (status == true) "SUCCESS" else "FAILED"}.......")
        appendTextEntity(message)
    }
}

fun Long.formatDate(): String? =
    SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault()).format(Date(this))

/*fun NipNotification.print(printerListener: POIPrinterManage.IPrinterListener) {
    buildNipReceipt.print(printerListener)
}

fun NipNotification.print(): Single<PrinterResponse> {
    return buildNipReceipt.print()
}

val NipNotification.buildNipReceipt: ReceiptBuilder
    get() = ReceiptBuilder().apply {
        appendLogo()
        appendTextEntityFontSixteenCenter("BANK TRANSFER")
        appendTextEntity("\nBeneficiary Account Number: $beneficiaryAccountNumber")
        appendTextEntity("Source Name: $sourceName")
        appendTextEntity("Source Account Number: $sourceAccountNumber")
        appendTextEntity(
            "Amount: \u20A6${amount}"
        )
        appendTextEntity("Date: $createdAt")
    }


fun List<NipNotification>.printAllNotifications(): Observable<PrinterResponse> {
    var emitter: ObservableEmitter<PrinterResponse>? = null
    val printerListener = object : POIPrinterManage.IPrinterListener {
        override fun onError(p0: Int, p1: String?) {
            emitter?.let {
                if (it.isDisposed.not())
                    it.onError(Throwable("message:$p1 - code:$p0"))
            }
        }

        override fun onFinish() {
            emitter?.let {
                if (it.isDisposed.not())
                    it.onNext(PrinterResponse())
            }
        }

        override fun onStart() {
            Timber.e("Printing started")
        }
    }
    return Observable.create {
        emitter = it
        forEach { nipNotification ->
            if (it.isDisposed.not())
                nipNotification.print(printerListener)
        }
        it.onComplete()
    }
}*/
