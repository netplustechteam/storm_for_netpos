package com.woleapp.adapters;

public interface OnItemDetailsClicked {
    void onItemDetailsClicked(int position);
}
