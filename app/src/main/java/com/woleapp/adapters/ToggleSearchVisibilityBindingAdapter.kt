package com.woleapp.adapters

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("toggleSearchVisibility")
fun ImageView.toggleSearchVisibility(editText: EditText) {
    this.setOnClickListener {
        if (editText.visibility == View.VISIBLE) {
            editText.visibility = View.GONE
        } else {
            editText.visibility = View.VISIBLE
        }
    }
}
