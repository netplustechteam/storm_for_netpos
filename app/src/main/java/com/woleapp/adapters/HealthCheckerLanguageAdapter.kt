package com.woleapp.adapters

import android.content.Context

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import com.woleapp.model.Language

typealias LanguageChangeListener = (lang: String, checked: Boolean) -> Unit

class HealthCheckerLanguageAdapter(
    private val context: Context,
    private var languages: List<Language> = arrayListOf(),
    private var languageChangeListener: LanguageChangeListener
) :
    BaseAdapter() {
    override fun getCount(): Int =
        languages.size

    override fun getItem(position: Int): Language = languages[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View? = convertView
        if (view == null)
            view = CheckBox(context)
        val checkBox = view as CheckBox
        checkBox.text = getItem(position).language_name
        checkBox.setOnCheckedChangeListener { _, isChecked ->
            languageChangeListener.invoke(getItem(position).language_name, isChecked)
        }
        return view
    }

    fun newItems(newLanguages: List<Language>) {
        languages = newLanguages
        notifyDataSetChanged()
    }
}