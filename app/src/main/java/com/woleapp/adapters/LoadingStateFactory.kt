package com.woleapp.adapters

import com.woleapp.db.TransactionsBoundaryCallBack
import com.woleapp.db.loadStateListener
import dagger.assisted.AssistedFactory

@AssistedFactory
interface LoadingStateFactory {
    fun createLoadTransactionsBoundaryCallBack(
        loadState: loadStateListener
    ): TransactionsBoundaryCallBack
}
