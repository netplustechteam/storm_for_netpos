package com.woleapp.viewmodels

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.danbamitale.epmslib.entities.*
import com.danbamitale.epmslib.processors.TransactionProcessor
import com.danbamitale.epmslib.utils.IsoAccountType
import com.danbamitale.epmslib.utils.MessageReasonCode
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.isw.gateway.TransactionProcessorWrapper
import com.isw.iswclient.iswapiclient.getTokenClient
import com.isw.iswclient.request.IswParameters
import com.isw.iswclient.request.TokenPassportRequest
import com.netpluspay.netpossdk.NetPosSdk
import com.pixplicity.easyprefs.library.Prefs
import com.woleapp.BuildConfig
import com.woleapp.R
import com.woleapp.Repository
import com.woleapp.kozenext.buildSMSText
import com.woleapp.kozenext.print
import com.woleapp.model.*
import com.woleapp.network.SmsApiClient
import com.woleapp.nibss.NetPosTerminalConfig
import com.woleapp.nibss.PREF_CONFIG_DATA
import com.woleapp.nibss.PREF_KEYHOLDER
import com.woleapp.util.Constants.*
import com.woleapp.util.Event
import com.woleapp.util.RandomNumUtil.formattedTime
import com.woleapp.util.RandomNumUtil.generateRandomRrn
import com.woleapp.util.RandomNumUtil.getCurrentDateTime
import com.woleapp.util.RandomNumUtil.getDate
import com.woleapp.util.RandomNumUtil.mapDanbamitaleResponseToResponseX
import com.woleapp.util.RandomNumUtil.mapTransactionResponseToTransaction
import com.woleapp.util.SharedPrefManager
import com.woleapp.util.Singletons
import com.woleapp.util.TransactionsUtils.mapTransactionToTransactionResponse
import com.woleapp.util.disposeWith
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SalesViewModel @Inject constructor(
    private val repository: Repository,
    private val gson: Gson
) : ViewModel() {
    private val stormMD = if (BuildConfig.FLAVOR.contains(
            "agency",
            true
        )
    ) BuildConfig.STRING_STORM_FOR_AGENT_MD else BuildConfig.STRING_STORM_FOR_MERCHANT_MD
    private val partnerId =
        if (BuildConfig.FLAVOR.contains(
                "agency",
                true
            )
        ) BuildConfig.STRING_STORM_FOR_AGENT_PID else BuildConfig.STRING_STORM_FOR_MERCHANT_PID
    private val serialNumber = NetPosSdk.getDeviceSerial()
    private val terminalId = Singletons.getCurrentlyLoggedInUser()?.terminal_id ?: ""

    private var appToken: String
    var cardData: CardData? = null
    var transResp: TransactionResponse? = null
    var transRespForTracking: TransactionResponse? = null
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
    val transactionState = MutableLiveData(Event(STATE_PAYMENT_STAND_BY))
    var amount: MutableLiveData<String> = MutableLiveData<String>("10")
    private val _shouldRefreshNibssKeys = MutableLiveData<Event<Boolean>>()
    private val lastTransactionResponse = MutableLiveData<TransactionResponse>()
    private val _finish = MutableLiveData<Event<Boolean>>()
    val finish: LiveData<Event<Boolean>>
        get() = _finish
    private var _partnerThreshold: MutableLiveData<GetPartnerInterSwitchThresholdResponse> =
        MutableLiveData()
    private var amountLong = 0L
    private val customerName = MutableLiveData("")
    private var isoAccountType: IsoAccountType? = null
    private var cardScheme: String? = null
    private val _smsSent = MutableLiveData<Event<Boolean>>()
    private val _getCardData = MutableLiveData<Event<Boolean>>()
    private var user: User?
    private var description: String = ""
    val smsSent: LiveData<Event<Boolean>>
        get() = _smsSent
    private val _toastMessage = MutableLiveData<Event<String>>()
    val toastMessage: LiveData<Event<String>>
        get() = _toastMessage
    private val _message: MutableLiveData<Event<String>> by lazy {
        MutableLiveData<Event<String>>()
    }
    private val _done = MutableLiveData(Event(false))
    private var amountDbl: Double = 0.0
    val transactionDone: LiveData<Event<Boolean>>
        get() = _done
    private val _showPrintDialog = MutableLiveData<Event<String>>()
    val showPrintDialog: LiveData<Event<String>>
        get() = _showPrintDialog
    val message: LiveData<Event<String>>
        get() = _message
    private val _refreshNibssKeys = MutableLiveData<Event<Boolean>>()
    val refreshNibssKeys: LiveData<Event<Boolean>>
        get() = _refreshNibssKeys
    private val _showPrinterError = MutableLiveData<Event<String>>()

    val showPrinterError: LiveData<Event<String>>
        get() = _showPrinterError
    private val _logSalesOrder = MutableLiveData<Event<TransactionResponse>>()
    val logSalesOrder: LiveData<Event<TransactionResponse>>
        get() = _logSalesOrder
    private val _showReceiptTypeDialog = MutableLiveData<Event<Boolean>>()
    val showReceiptTypeDialog: LiveData<Event<Boolean>>
        get() = _showReceiptTypeDialog
    private lateinit var userType: String
    private lateinit var appXApiKey: String

    private var iswPaymentProcessorObject: TransactionProcessorWrapper? = null

    private val _showReceiptTypeMutableLiveData = MutableLiveData<Event<Boolean>>()

    val showReceiptType: LiveData<Event<Boolean>>
        get() = _showReceiptTypeMutableLiveData

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val untracked = repository.getAllTrackingTransactions()
            Log.d("UN_TRACKED", "${Gson().toJson(untracked)}")
        }
        if (SharedPrefManager.getUser().user_type_for_new_storm_service != null) {
            userType = SharedPrefManager.getUser().user_type_for_new_storm_service
        }
        appXApiKey = SharedPrefManager.getXapiKey()
        user = Singletons.getCurrentlyLoggedInUser()
        appToken = SharedPrefManager.getAppTokenForNewStormService()
        getThresholdFromLocalOrRemote()
    }

    fun validateField() {
        amountDbl = (
            amount.value!!.toDoubleOrNull() ?: kotlin.run {
                _message.value = Event("Enter a valid amount")
                return
            }
            ) * 100
        this.amountLong = amountDbl.toLong()
        _getCardData.value = Event(true)
    }

    private fun getThresholdFromLocalOrRemote() {
        val thresholdFromLocal =
            Prefs.getString(SharedPrefManager.getUser().netplus_id + "iswThreshold", "")
        if (thresholdFromLocal.isNotEmpty()) {
            val thresholdFromLocalAsObject = gson.fromJson(
                thresholdFromLocal,
                GetPartnerInterSwitchThresholdResponse::class.java
            )
            _partnerThreshold.postValue(thresholdFromLocalAsObject)
        } else {
            getThreshold()
        }
    }

    fun setCustomerName(name: String) {
        customerName.value = name
    }

    private fun processTransactionViaInterSwitchMakePayment(
        context: Context,
        transactionType: TransactionType = TransactionType.PURCHASE
    ) {
        val customRrn = generateRandomRrn(12)

        val configData: ConfigData = NetPosTerminalConfig.getConfigData() ?: kotlin.run {
            _message.value =
                Event("Terminal has not been configured, restart the application to configure")
            return
        }
        val keyHolder = NetPosTerminalConfig.getKeyHolder()!!

        // IsoAccountType.
        this.amountLong = amountDbl.toLong()
        val requestData =
            TransactionRequestData(
                transactionType,
                amountLong,
                0L,
                accountType = isoAccountType!!
            )

        if (Prefs.getString(SharedPrefManager.getUser().netplus_id + "iswThreshold", "")
            .isEmpty()
        ) {
            Toast.makeText(context, "Unable to identify partner", Toast.LENGTH_LONG).show()
        }

        val interSwitchObject =
            Prefs.getString(partnerId + "iswThreshold", "")
        val destinationAcc = if (interSwitchObject.isNotEmpty()) gson.fromJson(
            interSwitchObject,
            GetPartnerInterSwitchThresholdResponse::class.java
        ).bankAccountNumber else {
            getIswToken(context)
            getThreshold()
            _partnerThreshold.value?.bankAccountNumber ?: ""
        }

        val institutionCode = if (interSwitchObject.isNotEmpty()) gson.fromJson(
            interSwitchObject,
            GetPartnerInterSwitchThresholdResponse::class.java
        ).institutionalCode else {
            getIswToken(context)
            getThreshold()
            _partnerThreshold.value?.institutionalCode ?: ""
        }

        if (destinationAcc.isEmpty()) {
            Toast.makeText(context, "No destination account found", Toast.LENGTH_LONG).show()
            return
        }

        val iswParam = IswParameters(
            stormMD,
            user?.business_address ?: "",
            token = Prefs.getString(ISW_TOKEN, "error2"),
            "",
            terminalId = terminalId,
            terminalSerial = serialNumber,
            destinationAcc,
            receivingInstitutionId = institutionCode
        )

        requestData.apply {
            iswParameters = iswParam
        }

        iswPaymentProcessorObject =
            TransactionProcessorWrapper(
                stormMD,
                terminalId,
                requestData.amount,
                transactionRequestData = requestData,
                keyHolder = keyHolder,
                configData = configData
            )
        cardData?.let { cardData ->
            transactionState.value = Event(STATE_PAYMENT_STARTED)
            iswPaymentProcessorObject!!.processIswTransaction(cardData)
                .flatMap {
                    transResp = it
                    if (it.responseCode == "A3") {
                        Prefs.remove(PREF_CONFIG_DATA)
                        Prefs.remove(PREF_KEYHOLDER)
                        _shouldRefreshNibssKeys.postValue(Event(true))
                    }

                    it.cardHolder = customerName.value!!
                    it.cardLabel = cardScheme!!
                    it.amount = requestData.amount
                    lastTransactionResponse.postValue(it)
                    _message.postValue(Event(if (it.responseCode == "00") "Transaction Approved" else "Transaction Not approved"))
                    transRespForTracking = it
                    // Insert to database
                    repository.insertSingleTransaction(
                        mapTransactionResponseToTransaction(it)
                    )
                }.flatMap {
                    val resp: TransactionResponse = lastTransactionResponse.value!!
                    if (resp.responseCode == "00") {
                        logTransactionAfterConnectingToNibss(
                            mapDanbamitaleResponseToResponseX(resp),
                            "APPROVED",
                            "ISW"
                        )
                        Single.just(it)
                    } else {
                        logTransactionAfterConnectingToNibss(
                            transactionResponse = mapDanbamitaleResponseToResponseX(resp),
                            status = "DECLINED",
                            "ISW"
                        )
                        Single.just(it)
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {
                    transactionState.value = Event(STATE_PAYMENT_STAND_BY)
                    printReceipt(context)
                }.subscribe { t1, throwable ->
                    t1?.let {
                    }
                    throwable?.let {
                        _message.value = Event("Error: ${it.localizedMessage}")
                        Timber.e(it)
                    }
                }.disposeWith(compositeDisposable)
        }
    }

    @SuppressLint("CheckResult")
    private fun logTransactionAfterConnectingToNibss(
        transactionResponse: TransactionResponseX,
        status: String,
        routingChannel: String
    ) {
        transactionState.postValue(Event(STATE_PAYMENT_DONE_LOGGING_TO_BACKEND))
        var dataToLog: JsonObject
        with(transactionResponse) {
            dataToLog = transactionResponse.mapToStormStructure(
                appXApiKey,
                routingChannel,
                user!!.netplus_id,
                status,
                userType
            )
        }
        repository.sendTransactionToServer(
            user!!.netplus_id,
            dataToLog
        )
            .doFinally {
                transactionState.postValue(Event(STATE_PAYMENT_APPROVED))
            }
            .flatMap {
                Timber.d("DATA_CALLED0GOT_HERE0")
                if (it.code() != 200 && it.code() != 409) {
                    lastTransactionResponse.value?.let { lastTransResp ->
                        val transResponse =
                            lastTransResp.copy(transmissionDateTime = transactionResponse.transmissionDateTime)
                        transResponse.mapTransactionToTransactionResponse(
                            if (transResponse.responseCode == "00") "approved" else "declined",
                            routingChannel,
                            false
                        )
                            .let { it1 ->
                                saveTransactionForTracking(it1)
                            }
                    }
                }
                Single.just(it)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { data, error ->
                data?.let {
                    transactionState.postValue(Event(STATE_PAYMENT_APPROVED))
                    if (it.code() != 200 && it.code() != 409) {
                        lastTransactionResponse.value?.let { lastTransResp ->
                            val transResponse =
                                lastTransResp.copy(transmissionDateTime = transactionResponse.transmissionDateTime)
                            transResponse.mapTransactionToTransactionResponse(
                                if (transResponse.responseCode == "00") "approved" else "declined",
                                routingChannel,
                                false
                            )
                                .let { it1 ->
                                    saveTransactionForTracking(it1)
                                }
                        }
                    }
                }
                error?.let {
                    Timber.d("DATA_CALLED12==>${it.localizedMessage}")
                    lastTransactionResponse.value?.let { lastTransResp ->
                        val transResponse =
                            lastTransResp.copy(transmissionDateTime = transactionResponse.transmissionDateTime)
                        transResponse.mapTransactionToTransactionResponse(
                            if (transResponse.responseCode == "00") "approved" else "declined",
                            routingChannel,
                            false
                        )
                            .let { it1 ->
                                saveTransactionForTracking(it1)
                            }
                    }
                    transactionState.postValue(Event(STATE_PAYMENT_APPROVED))
                }
            }.disposeWith(compositeDisposable)
    }

    private fun saveTransactionForTracking(transaction: TrackTransactionTable) {
        repository.insertSingleTransactionForLaterTracking(
            transaction
        ).subscribe { data, error ->
            data?.let {
                Timber.d(it.toString())
            }
            error?.let {
                Timber.e(it.localizedMessage)
            }
        }.disposeWith(compositeDisposable)
    }

    private fun getIswToken(context: Context): String {
        val req = TokenPassportRequest(context.getString(R.string.userMD), user?.terminal_id!!)

        return try {
            var iswToken = ""
            compositeDisposable.add(
                getTokenClient.getToken(req)
                    .subscribeOn(Schedulers.io())
                    .subscribe { t1, t2 ->
                        t1?.let {
                            Prefs.putString(ISW_TOKEN, it.token)
                            iswToken = it.token
                        }
                        t2?.let {
                        }
                    }
            )
            iswToken
        } catch (e: Exception) {
            ""
        }
    }

    fun makePayment(
        context: Context,
        transactionType: TransactionType = TransactionType.PURCHASE
    ) {
        amount.value?.let { amount ->
            if (BuildConfig.FLAVOR.contains("merchant", true)) {
                makePaymentViaNibss(context, transactionType)
            } else {
//                processTransactionViaInterSwitchMakePayment(context, transactionType)
                _partnerThreshold.value?.interSwitchThreshold?.toDouble()?.let { thresHold ->
                    if (thresHold.toInt() == 0) {
                        makePaymentViaNibss(context, transactionType)
                    } else {
                        if (amount.toDouble() < thresHold) {
                            makePaymentViaNibss(context, transactionType)
                        } else {
                            processTransactionViaInterSwitchMakePayment(context, transactionType)
                        }
                    }
                }
            }
        }
    }

    private fun getThreshold() {
        compositeDisposable.add(
            repository.getThreshold()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { response, throwable ->
                    response?.let {
                        // Save the threshold to sharedPrefs
                        val thresholdObjectInString = gson.toJson(it)
                        Prefs.putString(
                            SharedPrefManager.getUser().netplus_id + "iswThreshold",
                            thresholdObjectInString
                        )
                        _partnerThreshold.postValue(it)
                    }
                    throwable?.let {
                        Timber.e(it)
                    }
                }
        )
    }

    private fun makePaymentViaNibss(
        context: Context,
        transactionType: TransactionType = TransactionType.PURCHASE
    ) {
        Timber.e(cardData.toString())
        val configData: ConfigData = NetPosTerminalConfig.getConfigData() ?: kotlin.run {
            _message.value =
                Event("Terminal has not been configured, restart the application to configure")
            return
        }
        val keyHolder = NetPosTerminalConfig.getKeyHolder()!!
        Timber.e("terminal id for transaction ${NetPosTerminalConfig.getTerminalId()}")
        val hostConfig = HostConfig(
            NetPosTerminalConfig.getTerminalId(),
            NetPosTerminalConfig.connectionData,
            keyHolder,
            configData
        )

        val customStan = generateRandomRrn(6)
        val customRrn = generateRandomRrn(12)
        val transTime = formattedTime.replace(":", "")
        val transDateTime = getCurrentDateTime()

        // IsoAccountType.
        this.amountLong = amountDbl.toLong()
        val requestData =
            TransactionRequestData(
                transactionType,
                amountLong,
                0L,
                accountType = isoAccountType!!
            )

        val transactionToLog = cardData?.expiryDate?.let {
            customerName.value?.let { it1 ->
                user?.netplus_id?.let { it2 ->
                    TransactionToLogBeforeConnectingToNibbs(
                        status = "PENDING",
                        TransactionResponseX(
                            AID = "",
                            rrn = customRrn,
                            STAN = customStan,
                            TSI = "",
                            TVR = "",
                            accountType = isoAccountType!!.name,
                            acquiringInstCode = "",
                            additionalAmount_54 = "",
                            amount = requestData.amount.toInt(),
                            appCryptogram = "",
                            authCode = "",
                            cardExpiry = it,
                            cardHolder = it1,
                            cardLabel = cardScheme.toString(),
                            id = 0,
                            localDate_13 = getDate(),
                            localTime_12 = transTime,
                            maskedPan = cardData!!.pan,
                            merchantId = it2,
                            originalForwardingInstCode = "",
                            otherAmount = requestData.otherAmount.toInt(),
                            otherId = "",
                            responseCode = "99",
                            responseDE55 = "",
                            terminalId = user!!.terminal_id!!,
                            transactionTimeInMillis = 0,
                            transactionType = requestData.transactionType.name,
                            transmissionDateTime = transDateTime
                        )
                    )
                }
            }
        }

        // Send to backend first
        val processor = TransactionProcessor(hostConfig)
        transactionState.value = Event(STATE_PAYMENT_STARTED)
        processor.processTransaction(context, requestData, cardData!!)
            .onErrorResumeNext {
                processor.rollback(context, MessageReasonCode.Timeout)
            }
            .flatMap {
                transResp = it
                if (it.responseCode == "A3") {
                    Prefs.remove(PREF_CONFIG_DATA)
                    Prefs.remove(PREF_KEYHOLDER)
                    _shouldRefreshNibssKeys.postValue(Event(true))
                }

                it.cardHolder = customerName.value!!
                it.cardLabel = cardScheme!!
                it.amount = requestData.amount
                lastTransactionResponse.postValue(it)
                Timber.e(it.toString())
                Timber.e(it.responseCode)
                Timber.e(it.responseMessage)
                _message.postValue(Event(if (it.responseCode == "00") "Transaction Approved" else "Transaction Not approved"))
                // Insert to database
                repository.insertSingleTransaction(
                    mapTransactionResponseToTransaction(it)
                )
            }.flatMap {
                val resp = lastTransactionResponse.value!!
                if (resp.responseCode == "00") {
                    logTransactionAfterConnectingToNibss(
                        mapDanbamitaleResponseToResponseX(resp),
                        "APPROVED",
                        "Nibss"
                    )
                    Single.just(resp)
                } else {
                    logTransactionAfterConnectingToNibss(
                        transactionResponse = mapDanbamitaleResponseToResponseX(resp),
                        status = "DECLINED",
                        "Nibss"
                    )
                    Single.just(resp)
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally {
                transactionState.value = Event(STATE_PAYMENT_STAND_BY)
                printReceipt(context)
            }.subscribe { t1, throwable ->
                t1?.let {
                    // _finish.value = Event(true)
                }
                throwable?.let {
                    _message.value = Event("Error: ${it.localizedMessage}")
                    Timber.e(it)
                }
            }.disposeWith(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun setAccountType(accountType: IsoAccountType) {
        this.isoAccountType = accountType
    }

    fun setCardScheme(cardScheme: String?) {
        this.cardScheme = if (cardScheme.equals("no match", true)) "VERVE" else cardScheme
    }

    fun finishTransaction() {
        _done.value = Event(true)
    }

    fun showReceiptDialog() {
        _showPrintDialog.value = Event(lastTransactionResponse.value!!.buildSMSText().toString())
    }

    fun showReceiptDialog(transactionResponse: TransactionResponse) {
        _showPrintDialog.value = Event(transactionResponse.buildSMSText().toString())
    }

    fun printReceipt(
        context: Context,
        isMerchantCopy: Boolean = false,
        printBoth: Boolean = false,
        selected: Boolean = false
    ) {
        lastTransactionResponse.value!!.print(
            context,
            description = description,
            isMerchantCopy = isMerchantCopy
        )
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { t1, throwable ->
                t1?.let {
                    if (printBoth) {
                        if (isMerchantCopy) {
                            finish()
                        } else {
                            printReceipt(context, isMerchantCopy = true, printBoth = true)
                        }
                    } else {
                        if (selected.not()) {
                            finish()
                        }
                    }
                }
                throwable?.let {
                    Timber.e(it)
                    _showPrinterError.value = Event(it.localizedMessage ?: "Error")
                    Timber.e("error: ${it.localizedMessage}")
                    _message.value = Event("Error: ${it.localizedMessage}")
                }
            }.disposeWith(compositeDisposable)
    }

    private fun printReceipt(context: Context) {
        val transactionResponse = lastTransactionResponse.value
        if (Build.MODEL.equals("Pro", true) || Build.MODEL.equals("P3", true)) {
            when (Prefs.getString(PREF_PRINTER_SETTINGS, PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY)) {
                PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY -> printReceipt(context, isMerchantCopy = false)
                PREF_VALUE_PRINT_CUSTOMER_AND_MERCHANT_COPY -> printReceipt(
                    context,
                    printBoth = true
                )
                PREF_VALUE_PRINT_SMS -> _showPrintDialog.postValue(
                    Event(transactionResponse?.buildSMSText().toString())
                )
                PREF_VALUE_PRINT_ASK_BEFORE_PRINTING -> _showReceiptTypeMutableLiveData.postValue(
                    Event(true)
                )
            }
        } else {
            _showPrintDialog.postValue(
                Event(transactionResponse?.buildSMSText().toString())
            )
        }
    }

    fun finish() {
        _finish.value = Event(true)
    }

    fun sendSmS(number: String) {
        val map = JsonObject().apply {
            addProperty("from", "NetPlus")
            addProperty("to", "+234${number.substring(1)}")
            addProperty("message", lastTransactionResponse.value!!.buildSMSText().toString())
        }
        Timber.e("payload: $map")
        val auth = "Bearer ${SharedPrefManager.getAppToken()}"
        val body: RequestBody = map.toString()
            .toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

        SmsApiClient.getSmsServiceInstance().sendSms(auth, body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { t1, t2 ->
                t1?.let {
                    _smsSent.value = Event(true)
                    Timber.e("Data $it")
                }
                t2?.let {
                    val httpException = it as? HttpException
                    httpException?.let { e ->
                        Timber.e(e)
                    }
                    _smsSent.value = Event(false)
                    _toastMessage.value = Event("Error: ${it.localizedMessage}")
                }
            }.disposeWith(compositeDisposable)
    }

    fun setDescription(description: String?) {
        description?.let {
            this.description = it
        }
    }
}
