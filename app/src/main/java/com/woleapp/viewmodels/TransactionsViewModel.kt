package com.woleapp.viewmodels

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.woleapp.BuildConfig
import com.woleapp.Repository
import com.woleapp.adapters.LoadingStateFactory
import com.woleapp.db.LoadState
import com.woleapp.db.PAGE_SIZE
import com.woleapp.db.TransactionsBoundaryCallBack
import com.woleapp.db.dao.TransactionsDao
import com.woleapp.model.Transactions
import com.woleapp.model.kayodeStormImplementation.GetNewResponse
import com.woleapp.model.kayodeStormImplementation.TransactionFromNewBackEndService
import com.woleapp.util.SharedPrefManager
import com.woleapp.util.disposeWith
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class TransactionsViewModel @Inject constructor(
    private val repository: Repository,
    private val transactionsDao: TransactionsDao
) : ViewModel() {
    lateinit var loadStateFactory: LoadingStateFactory

    private lateinit var boundaryCallBack: TransactionsBoundaryCallBack
    private var disposables: CompositeDisposable = CompositeDisposable()
    val loadState = MutableLiveData<LoadState>()

    //    private val fetchedAgentTransactions = MutableLiveData<AgentTransactions>()
    private val fetchedTransactions = MutableLiveData<List<TransactionFromNewBackEndService>>()

    fun getTransactions(): LiveData<PagedList<Transactions>> {
        boundaryCallBack =
//            loadStateFactory.createLoadTransactionsBoundaryCallBack { loadState.value = it }
            TransactionsBoundaryCallBack(
                repository,
                transactionsDao
            ) {
                loadState.value = it
            }
        val dataSourceFactory = transactionsDao.getTransactions()
        val pagedListBuilder = LivePagedListBuilder(dataSourceFactory, PAGE_SIZE)
            .apply {
                setBoundaryCallback(boundaryCallBack)
            }
        return pagedListBuilder.build()
    }

    fun fetchTransactionViaNewSource(
        fromWhere: String,
        startDate: String,
        endDate: String,
        rrn: String
    ) {
        Log.d("CALLED_FOR_NEW", "YESS")
        fetchTransactionFromDifferentSource(fromWhere, startDate, endDate, rrn)
//            .flatMap {
//                Timber.d("AAAANew=>$it")
//                SharedPrefManager.setNextAgentTransactionsPage(1)
//                fetchedTransactions.postValue(it.transaction)
//                transactionsDao.deleteOldTransactions()
//            }.flatMap {
//                transactionsDao.insertTransactions(fetchedTransactions.value!!.map { it.toAppTransactionStructure() })
//            }
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { loadState.value = LoadState.LOADING_INITIAL }
            .doOnError {
                loadState.value = LoadState.LOADING_ERROR
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { items, throwable ->
                throwable?.let {
                    loadState.value = LoadState.LOADING_FINISHED
                }
                items?.let {
                    val data = it.transaction.map { data -> data.toTransactionNormalStructure() }
                    fetchedTransactions.value = data
                    transactionsDao.deleteOldTransactions()
                    transactionsDao.insertTransactions(fetchedTransactions.value!!.map { comingData -> comingData.toAppTransactionStructure() })
                    loadState.value = LoadState.LOADING_FINISHED
                    boundaryCallBack.loadItemsAfterRefresh()
                }
            }.disposeWith(disposables)
    }

    private fun fetchTransactionFromDifferentSource(
        fromWhere: String,
        startDate: String,
        endDate: String,
        rrn: String
    ): Single<GetNewResponse> = when (fromWhere) {
        "byDate" -> {
            repository.getTransactionFromBackendByDate(
                SharedPrefManager.getUser().netplus_id,
                0,
                startDate,
                endDate
            )
        }
        else -> {
            repository.getTransactionFromBackendByRrn(
                SharedPrefManager.getUser().netplus_id,
                0,
                rrn
            ).flatMap {
                Single.just(it.toGetNewResponse())
            }
        }
    }

    fun forceRefresh() {
        repository.getTransactionFromBackend(
            SharedPrefManager.getUser().netplus_id,
            0
        ).doOnError {
        }.flatMap {
            Timber.d("AAAA=>$it")
            if (BuildConfig.FLAVOR.contains("agency")) SharedPrefManager.setNextAgentTransactionsPage(
                1
            ) else SharedPrefManager.setNextMerchantTransactionsPage(1)
            fetchedTransactions.postValue(it.transaction)
            transactionsDao.deleteOldTransactions()
        }.flatMap {
            transactionsDao.insertTransactions(fetchedTransactions.value!!.map { it.toAppTransactionStructure() })
        }.subscribeOn(Schedulers.io())
            .doOnSubscribe { loadState.value = LoadState.LOADING_INITIAL }
            .doOnError { loadState.value = LoadState.LOADING_ERROR }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { items, throwable ->
                throwable?.let {
                    loadState.value = LoadState.LOADING_FINISHED
                }
                items?.let {
                    loadState.value = LoadState.LOADING_FINISHED
                    boundaryCallBack.loadItemsAfterRefresh()
                }
            }.disposeWith(disposables)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
