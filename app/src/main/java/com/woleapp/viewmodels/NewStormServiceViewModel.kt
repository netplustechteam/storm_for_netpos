package com.woleapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.woleapp.Repository
import com.woleapp.model.kayodeStormImplementation.GetBankListResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@HiltViewModel
class NewStormServiceViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
    private var _supportedBanks: MutableLiveData<List<GetBankListResponse>> = MutableLiveData()
    val supportedBanks: LiveData<List<GetBankListResponse>> get() = _supportedBanks

    init {
        getBankList()
    }

    private fun getBankList() {
        compositeDisposable.add(
            repository.getListOfBanksSupportedByeTransact()
                .subscribe { response ->
                    response?.let {
                        if (it.isSuccessful && it.code() == 200) {
                            _supportedBanks.value = it.body()
                        }
                    }
                }
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
