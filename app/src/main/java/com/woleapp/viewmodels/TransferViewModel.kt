package com.woleapp.viewmodels

import android.content.Context
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.netpluspay.netpossdk.printer.PrinterResponse
import com.woleapp.kozenext.builder
import com.woleapp.kozenext.print
import com.woleapp.model.TransferModel
import com.woleapp.network.SmsApiClient
import com.woleapp.util.Event
import com.woleapp.util.SharedPrefManager
import com.woleapp.util.disposeWith
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import timber.log.Timber

class TransferViewModel : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val _smsSent = MutableLiveData<Event<Boolean>>()
    val smsSent: LiveData<Event<Boolean>>
        get() = _smsSent
    private val _toastMessage = MutableLiveData<Event<String>>()
    val toastMessage: LiveData<Event<String>>
        get() = _toastMessage

    private val _showPrinterError = MutableLiveData<Event<String>>()

    val showPrinterError: LiveData<Event<String>>
        get() = _showPrinterError

    var transferModel: TransferModel? = null

    private val _showPrintDialog = MutableLiveData<Event<String>>()

    val showPrintDialog: LiveData<Event<String>>
        get() = _showPrintDialog

    fun printTransfer(context: Context, transferModel: TransferModel) {
        this.transferModel = transferModel
        (
            if (Build.MODEL == "P3" || Build.MODEL.equals("Pro", true)) transferModel.print(context)
                .subscribeOn(Schedulers.io()) else {
                _showPrintDialog.postValue(
                    Event(
                        transferModel.builder().toString()
                    )
                )
                Single.just(PrinterResponse())
            }
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { t1, t2 ->
                t1?.let {
                }
                t2?.let {
                    Timber.e(it)
                    _showPrinterError.value = Event(it.localizedMessage ?: "Error")
                    Timber.e("error: ${it.localizedMessage}")
                    _toastMessage.value = Event("Error: ${it.localizedMessage}")
                }
            }.disposeWith(compositeDisposable)
    }

    fun showReceiptDialog() {
        _showPrintDialog.value = Event(transferModel!!.builder().toString())
    }

    fun sendSmS(number: String) {
        val map = JsonObject().apply {
            addProperty("from", "NetPlus")
            addProperty("to", "+234${number.substring(1)}")
            addProperty("message", transferModel?.builder().toString())
        }
        Timber.e("payload: $map")
        val auth = "Bearer ${SharedPrefManager.getAppToken()}"
        val body: RequestBody = map.toString()
            .toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

        SmsApiClient.getSmsServiceInstance().sendSms(auth, body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { t1, t2 ->
                t1?.let {
                    _smsSent.value = Event(true)
                    Timber.e("Data $it")
                }
                t2?.let {
                    val httpException = it as? HttpException
                    httpException?.let { e ->
                        Timber.e(e)
                    }
                    _smsSent.value = Event(false)
                    _toastMessage.value = Event("Error: ${it.localizedMessage}")
                }
            }.disposeWith(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
