package com.woleapp.viewmodels // ktlint-disable filename

import android.content.Context
import android.os.Build
import androidx.lifecycle.ViewModel
import com.netpluspay.netpossdk.printer.PrinterResponse
import com.woleapp.kozenext.printMethod
import com.woleapp.kozenext.reprintTransaction
import com.woleapp.model.Transactions
import com.woleapp.util.disposeWith
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class PrinterViewModel @Inject constructor() : ViewModel() {
    private val compositeDisposable = CompositeDisposable()

    fun printReceipt(context: Context, transactionToPrint: Transactions) {

        if (Build.MODEL.equals("P3") || Build.MODEL.equals("Pro")) transactionToPrint.reprintTransaction(
            context
        )
            .subscribeOn(Schedulers.io()) else {
            Single.just(PrinterResponse())
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { t1, t2 ->
                t1?.let {
                }
                t2?.let {
                    Timber.e(it)
                    Timber.e("error: ${it.localizedMessage}")
                }
            }.disposeWith(compositeDisposable)
    }

    fun variedPrinting(transactionToPrint: Transactions, context: Context, askBefore: String, action: () -> Unit) {
        transactionToPrint.printMethod(context, askBefore) {
            action()
        }
    }
}
