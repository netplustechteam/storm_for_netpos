package com.woleapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.danbamitale.epmslib.entities.PosMode
import com.danbamitale.epmslib.entities.TransactionType
import com.danbamitale.epmslib.utils.IsoAccountType

@Entity(tableName = "transRespForTracking")
data class TrackTransactionTable(
    var transactionType: TransactionType,
    var maskedPan: String,
    var amount: Long,

    var transmissionDateTime: String,
    var STAN: String,
    @PrimaryKey
    var RRN: String,
    var localTime_12: String,
    var localDate_13: String,
    var otherAmount: Long = 0,
    var acquiringInstCode: String = "",
    var originalForwardingInstCode: String = "",
    var authCode: String = "",
    var responseCode: String = "",
    var additionalAmount_54: String = "",
    var echoData: String? = null,

    var cardLabel: String = "",
    var cardExpiry: String = "",
    var cardHolder: String = "",
    var TVR: String = "",
    var TSI: String = "",
    var AID: String = "",
    var appCryptogram: String = "",
    var transactionTimeInMillis: Long = 0L,
    var accountType: IsoAccountType = IsoAccountType.DEFAULT_UNSPECIFIED,

    var terminalId: String = "",
    var merchantId: String = "",
    var otherId: String = "",
    var push_to_backendStatus: Boolean = false,
    var transactionStatus: String,
    var routingChannel: String,

    var id: Long = 0,
    var responseDE55: String? = null,
    var source: PosMode = PosMode.EPMS,
    var interSwitchThreshold: Long = 0L
)
