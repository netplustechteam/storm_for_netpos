package com.woleapp.model.kayodeStormImplementation

import com.woleapp.model.Transactions

data class TransactionXX(
    val amount: Int,
    val createdAt: String?,
    val description: String?,
    val destination: String?,
    val reference: String?,
    val rrn: String?,
    val settlement_status: String?,
    val storm_id: String?,
    val transaction_fee: Double?,
    val transaction_status: String?,
    val transaction_type: String?,
    val updatedAt: String?
) {
    fun toTransactionNormalStructure() =
        TransactionFromNewBackEndService(
            amount,
            createdAt ?: "",
            description ?: "Card payment",
            destination ?: "",
            reference ?: "",
            rrn ?: "",
            settlement_status ?: "",
            storm_id,
            transaction_fee ?: 0.0,
            transaction_status ?: "",
            transaction_type ?: "",
            updatedAt ?: ""
        )

    fun toAppTransactionStructure() =
        Transactions().apply {
            id = storm_id
            transaction_id = rrn.toString()
            reference_no_Etranzact = reference.toString()
            transaction_type = this@TransactionXX.transaction_type
            transaction_date = updatedAt
            this.description = this@TransactionXX.description.toString()
            beneficiary_name = this@TransactionXX.description.toString().split(" via ")[0]
                .split(" ")
                .last() ?: ""
            this.amount = this@TransactionXX.amount.toDouble()
            this.status = this@TransactionXX.transaction_status
            destination_account = destination.toString()
        }
}
