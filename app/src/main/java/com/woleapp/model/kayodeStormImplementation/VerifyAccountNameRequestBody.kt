package com.woleapp.model.kayodeStormImplementation

data class VerifyAccountNameRequestBody(
    val accountNumber: String,
    val bankCode: String,
    val stormId: String
)
