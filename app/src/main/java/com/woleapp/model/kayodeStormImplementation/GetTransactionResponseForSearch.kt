package com.woleapp.model.kayodeStormImplementation

data class GetTransactionResponseForSearch(
    val transaction: List<TransactionX>
) {
    fun List<TransactionX>.toNormalStructure() {
        this.map { it.toTransactionNormalStructure() }
    }
}
