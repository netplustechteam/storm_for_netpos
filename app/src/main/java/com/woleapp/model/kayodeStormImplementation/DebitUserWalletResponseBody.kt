package com.woleapp.model.kayodeStormImplementation

data class DebitUserWalletResponseBody(
    val code: String,
    val `data`: DataX,
    val message: String
)