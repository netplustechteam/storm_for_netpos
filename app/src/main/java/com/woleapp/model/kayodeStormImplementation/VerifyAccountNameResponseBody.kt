package com.woleapp.model.kayodeStormImplementation

data class VerifyAccountNameResponseBody(
    val code: String,
    val message: String
)