package com.woleapp.model.kayodeStormImplementation

data class DataX(
    val ledger_balance: Double,
    val wallet_balance: Double
)