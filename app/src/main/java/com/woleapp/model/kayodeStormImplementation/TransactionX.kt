package com.woleapp.model.kayodeStormImplementation

import com.woleapp.model.Transactions

data class TransactionX(
    val amount: Int,
    val createdAt: String,
    val description: Any?,
    val destination: Any?,
    val reference: Any?,
    val rrn: Any?,
    val settlement_status: String,
    val storm_id: String,
    val transaction_fee: Int,
    val transaction_status: String,
    val transaction_type: String,
    val updatedAt: String
) {
    fun toTransactionNormalStructure() =
        TransactionFromNewBackEndService(
            amount,
            createdAt,
            description?.toString(),
            destination?.toString(),
            reference?.toString(),
            rrn?.toString(),
            settlement_status,
            storm_id,
            transaction_fee.toDouble(),
            transaction_status,
            transaction_type,
            updatedAt
        )

    fun toAppTransactionStructure() =
        Transactions().apply {
            id = storm_id
            transaction_id = rrn?.toString() ?: reference.toString()
            reference_no_Etranzact = reference?.toString() ?: rrn!!.toString()
            transaction_type = this@TransactionX.transaction_type
            transaction_date = updatedAt
            this.description = this@TransactionX.description?.toString() ?: "Card payment"
            beneficiary_name = this@TransactionX.description?.toString()?.split(" via ")?.get(0)
                ?.split(" ")
                ?.last() ?: ""
            this.amount = this@TransactionX.amount.toDouble()
            this.status = this@TransactionX.transaction_status
            destination_account = destination?.toString() ?: ""
        }
}
