package com.woleapp.model.kayodeStormImplementation

data class User(
    val account_number: String,
    val business_name: String,
    val createdAt: String,
    val email: String,
    val mobile_number: String,
    val storm_id: String,
    val storm_wallet: StormWallet,
    val terminal_id: Any,
    val type: String,
    val updatedAt: String
)
