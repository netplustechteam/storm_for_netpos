package com.woleapp.model.kayodeStormImplementation

data class GetBankListResponse(
    val bank_code: String,
    val bank_name: String
)
