package com.woleapp.model.kayodeStormImplementation

data class StormRegisterUserResponse(
    val token: String,
    val user: UserX
)