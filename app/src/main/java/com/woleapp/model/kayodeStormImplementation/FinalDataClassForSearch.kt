package com.woleapp.model.kayodeStormImplementation

data class FinalDataClassForSearch(
    val transaction: TransactionXXX
) {
    fun toGetNewResponse() =
        GetNewResponse(
            transaction = listOf(this.transaction.toAppTransactionStructure())
        )
}
