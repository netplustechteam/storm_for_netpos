package com.woleapp.model.kayodeStormImplementation

data class GetTransactionsResponse(
    var transaction: List<TransactionFromNewBackEndService>
)
