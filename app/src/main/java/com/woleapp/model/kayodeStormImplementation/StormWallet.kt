package com.woleapp.model.kayodeStormImplementation

data class StormWallet(
    val createdAt: String,
    val ledger_balance: Double,
    val storm_id: String,
    val updatedAt: String,
    val wallet_balance: Double
)
