package com.woleapp.model.kayodeStormImplementation

data class StormLogin(
    val email: String,
    val password: String,
    var type: String
)
