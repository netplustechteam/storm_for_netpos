package com.woleapp.model.kayodeStormImplementation

data class SendTransactionToBackendResponse(
    val msg: String
)