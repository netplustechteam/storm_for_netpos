package com.woleapp.model.kayodeStormImplementation

data class StormLoginResponse(
    val token: String,
    val user: User
)
