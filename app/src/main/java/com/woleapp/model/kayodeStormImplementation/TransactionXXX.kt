package com.woleapp.model.kayodeStormImplementation

data class TransactionXXX(
    val amount: Int,
    val createdAt: String,
    val description: Any?,
    val destination: Any?,
    val reference: Any?,
    val rrn: String?,
    val settlement_status: String,
    val storm_id: String,
    val transaction_fee: Double,
    val transaction_status: String,
    val transaction_type: String,
    val updatedAt: String
) {
    fun toTransactionNormalStructure() =
        TransactionFromNewBackEndService(
            amount,
            createdAt,
            description?.toString(),
            destination?.toString(),
            reference?.toString(),
            rrn?.toString(),
            settlement_status,
            storm_id,
            transaction_fee.toDouble(),
            transaction_status,
            transaction_type,
            updatedAt
        )

    fun toAppTransactionStructure() =
        TransactionXX(
            amount,
            createdAt,
            description = this@TransactionXXX.description?.toString() ?: "Card payment",
            destination = this.destination?.toString() ?: "",
            reference = reference?.toString() ?: rrn!!,
            rrn = rrn ?: reference!!.toString(),
            settlement_status,
            storm_id,
            transaction_fee,
            transaction_status,
            transaction_type = this@TransactionXXX.transaction_type,
            updatedAt
        )
}
