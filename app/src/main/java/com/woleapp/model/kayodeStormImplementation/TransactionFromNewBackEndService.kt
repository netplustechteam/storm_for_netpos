package com.woleapp.model.kayodeStormImplementation

import com.woleapp.model.Transactions

data class TransactionFromNewBackEndService(
    val amount: Int,
    val createdAt: String,
    val description: String?,
    val destination: String?,
    val reference: String?,
    val rrn: String?,
    val settlement_status: String?,
    val storm_id: String?,
    val transaction_fee: Double?,
    val transaction_status: String?,
    val transaction_type: String?,
    val updatedAt: String?
) {
    fun toAppTransactionStructure() =
        Transactions().apply {
            id = storm_id
            transaction_id = rrn ?: reference
            reference_no_Etranzact = reference ?: rrn
            transaction_type = this@TransactionFromNewBackEndService.transaction_type ?: ""
            transaction_date = this@TransactionFromNewBackEndService.createdAt ?: ""
            this.description = this@TransactionFromNewBackEndService.description ?: "Card payment"
            beneficiary_name =
                this@TransactionFromNewBackEndService.description?.split(" via ")?.get(0)
                    ?.split(" ")
                    ?.last() ?: ""
            this.amount = this@TransactionFromNewBackEndService.amount.toDouble()
            this.status = this@TransactionFromNewBackEndService.transaction_status ?: ""
            this.destination_account = this@TransactionFromNewBackEndService.destination ?: "N.A"
        }
}
