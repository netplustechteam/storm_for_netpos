package com.woleapp.model.kayodeStormImplementation

data class StormRegisterModel(
    val accountNumber: String,
    val businessName: String,
    val bvn: String,
    val email: String,
    val mobileNumber: String,
    val password: String,
    val userType: String,
    var walletPin: String
)
