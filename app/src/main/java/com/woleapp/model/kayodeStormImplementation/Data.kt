package com.woleapp.model.kayodeStormImplementation

data class Data(
    val wallet_balance: Double,
    val ledger_balance: Double
)
