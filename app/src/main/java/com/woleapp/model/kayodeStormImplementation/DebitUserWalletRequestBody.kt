package com.woleapp.model.kayodeStormImplementation

data class DebitUserWalletRequestBody(
    val accountNumber: String,
    val amount: Int,
    val bankCode: String,
    val endPoint: String,
    var pin: String,
    val recieverName: String,
    val senderName: String,
    val stormId: String
)
