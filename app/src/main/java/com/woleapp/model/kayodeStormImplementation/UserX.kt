package com.woleapp.model.kayodeStormImplementation

data class UserX(
    val account_number: String,
    val business_name: String,
    val bvn: String,
    val createdAt: String,
    val email: String,
    val is_terminal_id: String,
    val mobile_number: String,
    val storm_id: String,
    val terminal_id: Any,
    val type: String,
    val updatedAt: String
)