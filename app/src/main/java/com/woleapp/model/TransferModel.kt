package com.woleapp.model

data class TransferModel @JvmOverloads constructor(
    val destinationAccount: String,
    val destinationName: String,
    val sourceName: String,
    val amount: String,
    val reference: String,
    val description: String,
    var status: Boolean? = null,
    var message: String? = null
)
