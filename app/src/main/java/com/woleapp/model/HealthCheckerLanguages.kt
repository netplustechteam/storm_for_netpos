package com.woleapp.model

import com.google.gson.annotations.SerializedName

data class HealthCheckerLanguages(
    @SerializedName("data") val languages: List<Language>?,
    val message: String
)

data class Language(
    val created_at: String,
    val language_name: String,
    val updated_at: String
)