package com.woleapp.ui.dialog_fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.databinding.DataBindingUtil;
import com.woleapp.R;
import com.woleapp.databinding.DialogFragmentBluetoothListPrinterBinding;


public class BluetoothDialogFragmentPrinter extends BaseDialogFragment {

    /*@BindView(R.id.cancel_btn) Button cancelBtn;
    @BindView(R.id.list) ListView list;
    @BindView(R.id.preferredTerminalCB) CheckBox preferredCB;
    @BindView(R.id.bluetoothTitleTv) TextView bluetoothTitleTv;*/


    DialogFragmentBluetoothListPrinterBinding binding;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_bluetooth_list_printer, container, false);
        return binding.getRoot();
        //return inflater.inflate(R.layout.dialog_fragment_bluetooth_list_printer, container, false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        d.setCancelable(false);
        return d;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*if (getMerchantComponent(getActivity()) != null) {
            VerifiedMerchantInfo merchantComponent = getMerchantComponent(getActivity()).provideMerchant();
            if (merchantComponent != null) {
                mPreferredTerminalUtils = new PreferredTerminalUtils(getActivity(), merchantComponent.getMerchantCode(), merchantComponent.getOperatorCode());
            }
        }*/

        binding.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                dismiss();
            }
        });

        binding.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
                getActivity().finish();
            }
        });
    }
}
