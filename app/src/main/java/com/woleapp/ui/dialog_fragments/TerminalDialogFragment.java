package com.woleapp.ui.dialog_fragments;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;

public class TerminalDialogFragment extends BluetoothDialogFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //cancelBtn
        binding.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                getActivity().finish();
            }
        });
    }
}
