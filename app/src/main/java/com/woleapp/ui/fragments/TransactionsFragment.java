package com.woleapp.ui.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.PagedList;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonObject;
import com.pixplicity.easyprefs.library.Prefs;
import com.woleapp.R;
import com.woleapp.Repository;
import com.woleapp.adapters.LoadingStateFactory;
import com.woleapp.adapters.TransactionAdapter;
import com.woleapp.databinding.DialogPrintTypeBinding;
import com.woleapp.databinding.DialogTransactionResultBinding;
import com.woleapp.databinding.LayoutInventoryListBinding;
import com.woleapp.db.LoadState;
import com.woleapp.db.dao.TransactionsDao;
import com.woleapp.model.Transactions;
import com.woleapp.model.User;
import com.woleapp.network.StormAPIClient;
import com.woleapp.util.CalendarUtil;
import com.woleapp.util.Constants;
import com.woleapp.util.JWTHelper;
import com.woleapp.util.OnItemClickListener;
import com.woleapp.util.SharedPrefManager;
import com.woleapp.util.TransactionsUtils;
import com.woleapp.util.Utilities;
import com.woleapp.viewmodels.PrinterViewModel;
import com.woleapp.viewmodels.TransactionsViewModel;
import com.woleapp.viewmodels.TransferViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

@AndroidEntryPoint
public class TransactionsFragment extends Fragment implements Constants, View.OnClickListener {
    private static AlertDialog receiptDialog;
    public DialogTransactionResultBinding receiptDialogBinding;
    public DialogPrintTypeBinding dialogPrintTypeBinding;
    private PrinterViewModel printerViewModel;
    private TransferViewModel transferViewModel;

    private AlertDialog printTypeDialog;

    @Inject
    public TransactionsFragment() {

    }

    @Inject
    Repository repository;
    @Inject
    TransactionsDao transactionsDao;

    @Inject
    LoadingStateFactory loadStateFactory;

    //Declaration of variables
    private LayoutInventoryListBinding binding;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    Drawable customErrorDrawable;
    private Utilities utilities;
    private ProgressDialog mProgressDialog;
    private TransactionAdapter adapter;
    String customer_type = "customer";
    TransactionsViewModel transactionsViewModel;
    private SearchView searchView;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Prefs.putString(MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, FETCH_BY_DEFAULT);

        //return inflater.inflate(R.layout.layout_introduction, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_inventory_list, container, false);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.executePendingBindings();
        //((GPHMainActivity)getActivity()).createBackButton(job_title);

        transferViewModel = new ViewModelProvider(this).get(TransferViewModel.class);
        printerViewModel = new PrinterViewModel();

        dialogPrintTypeBinding = DialogPrintTypeBinding.inflate(inflater, container, false);

//        receiptDialogBinding
//                .closeBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                receiptDialog.dismiss();
//            }
//        });
//        receiptDialogBinding
//                .sendButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (receiptDialogBinding.telephone.getText().toString().length() != 11) {
//                    Toast.makeText(
//                            requireContext(),
//                            "Please enter a valid phone number",
//                            Toast.LENGTH_LONG
//                    ).show();
//                    return;
//                }
//                transferViewModel.sendSmS(
//                        receiptDialogBinding.telephone.getText().toString()
//                );
//                receiptDialogBinding.sendButton.setEnabled(false);
//            }
//        });

        printTypeDialog = new AlertDialog.Builder(requireContext()).setCancelable(false)

                .setView(dialogPrintTypeBinding.getRoot()).create();

        receiptDialogBinding = DialogTransactionResultBinding.inflate(inflater, container, false);

        receiptDialog = new AlertDialog.Builder(requireContext()).setCancelable(false)
                .setView(receiptDialogBinding.getRoot()).create();

        return binding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        utilities = new Utilities(requireContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //((AppCompatActivity) getActivity()).getSupportActionBar().showNewUserView();
    }

    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((PreLoginActivity) getActivity()).createBackButton("Sign up");*/
        transactionsViewModel = new TransactionsViewModel(repository, transactionsDao);
        transactionsViewModel.loadStateFactory = loadStateFactory;
        binding.calendarButton.setVisibility(View.VISIBLE);
        adapter = new TransactionAdapter(onClick);
        binding.rvInventories.setAdapter(adapter);
        transactionsViewModel.getTransactions().observe(getViewLifecycleOwner(), transactions -> {
            adapter.submitList(transactions);
            if (transactions.isEmpty()) {
                binding.tvNoResults.setVisibility(View.VISIBLE);
            } else {
                binding.tvNoResults.setVisibility(View.GONE);
            }

            binding.eodButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    printAll(transactions);
                }
            });
        });
        transactionsViewModel.getLoadState().observe(getViewLifecycleOwner(), loadState -> {
            if (loadState == LoadState.LOADING_ERROR) {
                Toast.makeText(requireContext(), "An error occurred while loading transactions", Toast.LENGTH_SHORT).show();
            }
        });
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onRefresh() {
                transactionsViewModel.forceRefresh();
                adapter.notifyDataSetChanged();
            }
        });

        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 12 || charSequence.length() == 22) {
                    Prefs.putString(FETCH_TRANSACTIONS_FROM_BACKEND_RRN, charSequence.toString());
                    Prefs.putString(MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, FETCH_VIA_RRN);
                    transactionsViewModel.fetchTransactionViaNewSource("byRrn", "", "", charSequence.toString());
                    transactionsViewModel.getTransactions().observe(getViewLifecycleOwner(), transactions -> {
                        Timber.d("ERROR=>%s", transactions.size());
                        adapter.submitList(transactions);
                        adapter.notifyDataSetChanged();
                        if (transactions.isEmpty()) {
                            binding.tvNoResults.setVisibility(View.VISIBLE);
                        } else {
                            binding.tvNoResults.setVisibility(View.GONE);
                        }
                        adapter.notifyDataSetChanged();
                    });

                    transactionsViewModel.getLoadState().observe(getViewLifecycleOwner(), loadState -> {
                        if (loadState == LoadState.LOADING_ERROR) {
                            Toast.makeText(requireContext(), "An error occurred while loading transactions", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        transactionsViewModel.forceRefresh();
        binding.tvTitle.setText(R.string.tv_transactions);
        binding.etSearch.setHint(getString(R.string.searchByRrnOrReference));
        Bundle b1 = getArguments();
        if (b1 != null) {
            customer_type = b1.getString("customer_type", "customer");
        }

        if (customer_type.equalsIgnoreCase("customer")) {
            binding.tvNoResults.setText("You don't have any customers yet");
        } else {
            binding.tvNoResults.setText("You don't have any suppliers yet");
        }
        binding.tvNoResults.setText(R.string.no_transactions_yet);
//        getTransactions();

        binding.calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCalendarDialog(requireContext());
            }
        });
        binding.setTransactionsViewModel(transactionsViewModel);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void searchTransactionsByDate(String startTime, String endTime) {
        Prefs.putString(MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, FETCH_VIA_DATE);
        Prefs.putString(FETCH_TRANSACTIONS_FROM_BACKEND_START_DATE, startTime);
        Prefs.putString(FETCH_TRANSACTIONS_FROM_BACKEND_END_DATE, endTime);
        transactionsViewModel.fetchTransactionViaNewSource("byDate", startTime, endTime, "");
        adapter.notifyDataSetChanged();
    }

    private void printAll(PagedList<Transactions> transactionsResp) {
        TransactionsUtils.INSTANCE.printEndOfDay(transactionsResp, requireContext());
    }

    private void showCalendarDialog(Context context) {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        calendar2.set(i, i1, i2);
                        String[] times = new String[2];
                        times[0] = CalendarUtil.INSTANCE.getEndOfDayTransactions(
                                calendar2.getTimeInMillis()
                        )[0];
                        times[1] = CalendarUtil.INSTANCE.getEndOfDayTransactions(
                                calendar2.getTimeInMillis()
                        )[1];
                        searchTransactionsByDate(times[0], times[1]);
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    private OnItemClickListener.OnItemClickCallback onClick = (view, position) -> {
        Transactions clickedTransaction = adapter.getCurrentList().get(position);

        assert clickedTransaction != null;
        printerViewModel.printReceipt(requireContext(), clickedTransaction);

//        if (Prefs.getString(PREF_PRINTER_SETTINGS, PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY).equals(PREF_VALUE_PRINT_SMS)) {
//            printerViewModel.variedPrinting(clickedTransaction, requireContext(), "customer", new Function0<Unit>() {
//                @Override
//                public Unit invoke() {
//                    return null;
//                }
//            });
//        } else if (Prefs.getString(PREF_PRINTER_SETTINGS, PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY).equals(PREF_VALUE_PRINT_ASK_BEFORE_PRINTING)) {
//            dialogPrintTypeBinding.
//                    cancel.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    printTypeDialog.dismiss();
//                }
//            });
//            dialogPrintTypeBinding.customer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    printerViewModel.variedPrinting(clickedTransaction, requireContext(), "customer", new Function0<Unit>() {
//                        @Override
//                        public Unit invoke() {
//                            return null;
//                        }
//                    });
//                }
//            });
//            dialogPrintTypeBinding.merchant.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    printerViewModel.variedPrinting(clickedTransaction, requireContext(), "merchant", new Function0<Unit>() {
//                        @Override
//                        public Unit invoke() {
//                            return null;
//                        }
//                    });
//                }
//            });
//            printTypeDialog.show();
//        } else if (Prefs.getString(PREF_PRINTER_SETTINGS, PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY).equals(PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY)) {
//            printerViewModel.variedPrinting(clickedTransaction, requireContext(), "customer", new Function0<Unit>() {
//                @Override
//                public Unit invoke() {
//                    return null;
//                }
//            });
//        } else if (Prefs.getString(PREF_PRINTER_SETTINGS, PREF_VALUE_PRINT_CUSTOMER_AND_MERCHANT_COPY).equals(PREF_VALUE_PRINT_CUSTOMER_AND_MERCHANT_COPY)) {
//            printerViewModel.variedPrinting(clickedTransaction, requireContext(), "both", new Function0<Unit>() {
//                @Override
//                public Unit invoke() {
//                    return null;
//                }
//            });
//        }
    };

    @Override
    public void onClick(View view) {

    }


    public void showProgressBar() {
        mProgressDialog = ProgressDialog.show(requireContext(), null, null);
        mProgressDialog.setContentView(R.layout.dialog_progress);
        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mProgressDialog.setCancelable(false);
    }

    public void dismissProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    public void getTransactions() {
        User user = SharedPrefManager.getUser();
        JsonObject jsonObject = new JsonObject();

        if (user.getUser_type() == USER_TYPE_AGENT) {
            jsonObject.addProperty("user_type", "agent");
        } else {
            jsonObject.addProperty("user_type", "merchant");
        }
        jsonObject.addProperty("user_id", user.getUser_id());

        //data.put(PN_USER_ID,"1");
        //getTransactions(jsonObject);
    }

    public void getTransactions(JsonObject data) {
        String userToken = SharedPrefManager.getUserToken();
        String stormId = JWTHelper.getStormId(userToken);
        compositeDisposable.add(
                StormAPIClient.getTransactions(stormId, requireContext())
                        .subscribe(res -> {

                        }));
    }

    private void resetTransactionFetchPreferences() {
        Prefs.putString(MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, FETCH_BY_DEFAULT);
        SharedPrefManager.setNextAgentTransactionsPageForByDate(0);
        SharedPrefManager.setNextAgentTransactionsPageForByRrn(0);
        SharedPrefManager.setNextMerchantTransactionsPageForByDate(0);
        SharedPrefManager.setNextMerchantTransactionsPageForByRrn(0);
    }

    @Override
    public void onPause() {
        super.onPause();
        resetTransactionFetchPreferences();
    }

    @Override
    public void onStop() {
        super.onStop();
        resetTransactionFetchPreferences();
    }

    //    @Override
//    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_fragment, menu);
//        Log.e("onCreateOptionsMenu","onCreateOptionsMenu");
//        *//*final MenuItem item = menu.findItem(R.id.action_search);
//        item.setVisible(true);*//*
//
//
//        MenuItem searchMenu = menu.findItem(R.id.action_search);
//        searchMenu.setVisible(true);
//
//        SearchManager searchManager =
//                (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
//
//        searchView = (SearchView) menu.findItem(R.id.action_search)
//                .getActionView();
//        searchView.setSearchableInfo(searchManager
//                .getSearchableInfo(getActivity().getComponentName()));
//
//
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                // filter recycler view when query submitted
//                if(adapter!=null)// && adapter.getItemCount()>0
//                {
//                    adapter.getFilter().filter(query);
//                }
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String query) {
//                // filter recycler view when text is changed
//                if(adapter!=null)// && adapter.getItemCount()>0
//                {
//                    adapter.getFilter().filter(query);
//                }
//                return false;
//            }
//        });
//
//        item.getActionView().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onOptionsItemSelected(item);
//                //Toast.makeText(getActivity(), "click on menu", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        final MenuItem itemNewCustomer = menu.findItem(R.id.action_notification);
//        itemNewCustomer.setVisible(true);
//        *//*itemNewCustomer.getActionView().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onOptionsItemSelected(itemNewCustomer);
//                //Toast.makeText(getActivity(), "click on menu", Toast.LENGTH_SHORT).show();
//            }
//        });*//*
//
//        MenuItem searchMenu = menu.findItem(R.id.action_edit);
//        searchMenu.setVisible(true);
//        MenuItem delete = menu.findItem(R.id.action_delete);
//        if(showDeleteMenu)
//        {
//            delete.setVisible(true);
//        }
//        else
//        {
//            delete.setVisible(false);
//        }
//
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle item selection
//        switch (item.getItemId()) {
//
//            case R.id.action_search:
//                //mDataBinding.btnUpdate.setVisibility(View.VISIBLE);
//
////                showAlertDialog(context,"Confirmation","Are you sure you want to logout?");
//                //showDailogLogout("Confirmation","Are you sure you want to logout?");
//                //Toast.makeText(context,"action_search",Toast.LENGTH_SHORT).show();
//                return true;
//
//            case R.id.action_new_customer:
//                //mDataBinding.btnUpdate.setVisibility(View.VISIBLE);
//
////                showAlertDialog(context,"Confirmation","Are you sure you want to logout?");
//                //showDailogLogout("Confirmation","Are you sure you want to logout?");
//
//                //Toast.makeText(context,"New Customer",Toast.LENGTH_SHORT).show();
//
//
//                //addFragmentWithoutRemove(R.id.container_main,new AddCustomerFragment(),AddCustomerFragment.class.getSimpleName());
//
//
//                return true;
//
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

}
