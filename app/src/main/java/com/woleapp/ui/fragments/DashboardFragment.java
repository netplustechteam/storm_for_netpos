package com.woleapp.ui.fragments;

import static android.content.Context.WIFI_SERVICE;
import static com.woleapp.util.Constants.TRANSACTION_CASH_IN;
import static com.woleapp.util.Constants.TRANSACTION_CASH_OUT;
import static com.woleapp.util.Constants.USER_TYPE_AGENT;
import static com.woleapp.util.Constants.USER_TYPE_MERCHANT;
import static com.woleapp.util.Constants.USER_TYPE_NONE;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.gms.common.util.Strings;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;
import com.woleapp.BuildConfig;
import com.woleapp.R;
import com.woleapp.adapters.DashboardAdapter;
import com.woleapp.databinding.LayoutDashboardBinding;
import com.woleapp.databinding.LayoutSelectUserTypeBinding;
import com.woleapp.model.Service;
import com.woleapp.model.User;
import com.woleapp.mqtt.MqttHelper;
import com.woleapp.network.StormAPIClient;
import com.woleapp.network.soap.request.FundsTransferRequestModel;
import com.woleapp.network.soap.request.TransactionModel;
import com.woleapp.ui.activity.HomeActivity;
import com.woleapp.ui.activity.MainActivity;
import com.woleapp.util.AES;
import com.woleapp.util.Constants;
import com.woleapp.util.OnItemClickListener;
import com.woleapp.util.SharedPrefManager;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

@AndroidEntryPoint
public class DashboardFragment extends BaseFragment implements View.OnClickListener {
    Context context;
    private LayoutDashboardBinding binding;
    DashboardAdapter adapter;
    List<Service> serviceList;
    User user;
    ProgressDialog mProgressDialog;

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", DashboardFragment.class.getSimpleName() + "--");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("onPause", DashboardFragment.class.getSimpleName() + "--");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Integer a1=null;
        String s1 = a1.toString();*/

        setHasOptionsMenu(true);
        context = getActivity();
        user = SharedPrefManager.getUser();
        if (user == null) {
            logout();
            return;
        }
        getSettings();
        try {
//            ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        } catch (Exception e) {
            //Crashlytics.log(e.toString());
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (user.getUser_type() != USER_TYPE_NONE)
            populate();
        else showUserTypeSelectionDialog();

        Prefs.putString("current_user", user.getNetplus_id());
        Log.e("APP TOKEN WITH PREFS", Prefs.getString("current_user", "token haha"));
        String strDouble = String.format("%.2f", 1.9999);

        String ip = getWifiIPAddress();
        Log.e("IP", ip + "---");
        HomeActivity activity = (HomeActivity) getActivity();
        String email = user.getEmail();
        Log.d("EMAIL", email);
        String name = email.substring(0, email.indexOf('@'));
        /*activity.setUserName(user.getName());*/
        activity.setUserName(name);
        activity.setAvailableBalance(user.getAvailableBalance());
        ((HomeActivity) getActivity()).setTitleWithNoNavigation("Dashboard");
    }

    public void showUserTypeSelectionDialog() {
        if (BuildConfig.FLAVOR.equals("zenith") || BuildConfig.FLAVOR.contains("merchant")) {
            String flavor = BuildConfig.FLAVOR;
            Timber.d("====>%s", flavor);
            // sets the type to merchant type if it's for zenith
            user.setUser_type(USER_TYPE_MERCHANT);
            SharedPrefManager.setUser(user);
            SharedPrefManager.setUserType(user.getUser_type());
            populate();
        } else {
            if (BuildConfig.FLAVOR.contains("agency")) {
                user.setUser_type((USER_TYPE_AGENT));
                SharedPrefManager.setUser(user);
                SharedPrefManager.setUserType(user.getUser_type());
                populate();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                LayoutSelectUserTypeBinding userTypeBinding = LayoutSelectUserTypeBinding.inflate(getLayoutInflater(), null, false);
                userTypeBinding.setLifecycleOwner(getViewLifecycleOwner());
                userTypeBinding.executePendingBindings();
                builder.setView(userTypeBinding.getRoot())
                        .setCancelable(false);
                AlertDialog dialog = builder.create();
                if (user.getUser_type() == USER_TYPE_NONE)
                    userTypeBinding.buttonCancel.setVisibility(View.GONE);

                View.OnClickListener clickListener = v -> {
                    int currentUserType = user.getUser_type();
                    if (v == userTypeBinding.userMerchant)
                        user.setUser_type(USER_TYPE_MERCHANT);
                    else user.setUser_type(USER_TYPE_AGENT);
                    if (currentUserType == user.getUser_type()) {
                        Toast.makeText(requireContext(), "Can't select this again", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (dialog.isShowing())
                        dialog.cancel();
                    user.setAvailableBalance(getActivityCast().getGetAvailableBalance());
                    user.setLedgerBalance(getActivityCast().getGetLedgerBalance());
                    SharedPrefManager.setUser(user);
                    SharedPrefManager.setUserType(user.getUser_type());
                    populate();
                };
                userTypeBinding.buttonCancel.setOnClickListener(v -> {
                    if (user.getUser_type() == USER_TYPE_NONE)
                        Toast.makeText(context, "Please select one to continue", Toast.LENGTH_SHORT).show();
                    else
                        dialog.cancel();
                });
                userTypeBinding.userAgent.setOnClickListener(clickListener);
                userTypeBinding.userMerchant.setOnClickListener(clickListener);
                dialog.show();
            }
        }
    }

    private HomeActivity getActivityCast() {
        return (HomeActivity) requireActivity();
    }

    public String getWifiIPAddress() {
        WifiManager wifiMgr = (WifiManager) getActivity().getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return Formatter.formatIpAddress(ip);
    }

    public void populate() {
        if (BuildConfig.FLAVOR == "zenith") {
            serviceList = new ArrayList<>();
            setMerchantServiceItems();
        } else {
            serviceList = new ArrayList<>();
            Service service;
            if (BuildConfig.FLAVOR.contains("agency")) {
                service = new Service(1, "CASH-IN / BANK TRANSFER", R.drawable.cash_in_new);
                serviceList.add(service);
                service = new Service(2, "CASH-OUT", R.drawable.cash_out_new);
                serviceList.add(service);
                service = new Service(3, "FUND WALLET", R.drawable.fund_wallet_new);
                serviceList.add(service);
                service = new Service(4, "VIEW TRANSACTIONS", R.drawable.trans);
                serviceList.add(service);

                service = new Service(5, "PAY BILLS", R.drawable.bill);
                serviceList.add(service);
                service = new Service(6, "DOKITOR", R.drawable.ic_dokitor);
                serviceList.add(service);
                service = new Service(7, "SETTINGS", R.drawable.ic_baseline_settings);
                serviceList.add(service);
            } else {
                setMerchantServiceItems();
            }
        }

        setAnimation();
        binding.rvItems.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        //gridLayoutManager.setSpanSizeLookup(customsSpanSizeLookUp);
        binding.rvItems.setLayoutManager(gridLayoutManager);

        adapter = new DashboardAdapter(getActivity(), serviceList, onClick, false);
        binding.rvItems.setAdapter(adapter);
    }

    private void setMerchantServiceItems() {
        ArrayList<Service> merchantService = new ArrayList<>();
        merchantService.add(new Service(1, "PURCHASE", R.drawable.ic_purchase));
//        merchantService.add(new Service(2, "MARKETPLACE", R.drawable.ic_online_shopping));
//        merchantService.add( new Service(3, "ORDERS", R.drawable.ic_order));
        merchantService.add(new Service(4, "VIEW TRANSACTIONS", R.drawable.trans));
        merchantService.add(new Service(5, "CASH-IN / BANK TRANSFER", R.drawable.cash_in_new));
        merchantService.add(new Service(6, "PAY BILLS", R.drawable.bill));
        merchantService.add(new Service(7, "Settings", R.drawable.ic_baseline_settings));
        serviceList.addAll(merchantService);

        setAnimation();
        binding.rvItems.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        //gridLayoutManager.setSpanSizeLookup(customsSpanSizeLookUp);
        binding.rvItems.setLayoutManager(gridLayoutManager);

        adapter = new DashboardAdapter(getActivity(), serviceList, onClick, false);
        binding.rvItems.setAdapter(adapter);
    }

    public void setAnimation() {

        int resId = R.anim.grid_layout_animation_from_bottom;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        animation.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        binding.rvItems.setLayoutAnimation(animation);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        //user = SharedPrefManager.getUser();
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_dashboard, container, false);
        //((GPHMainActivity)getActivity()).createBackButton(job_title);
        return binding.getRoot();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.clear();

    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.menu_fragment, menu);
//        MenuItem searchMenu = menu.findItem(R.id.action_edit);
//        searchMenu.setVisible(true);
        //MenuItem delete = menu.findItem(R.id.action_delete);
//        if(showDeleteMenu)
//        {
//            delete.setVisible(true);
//        }
//        else
//        {
//            delete.setVisible(false);
//        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            /*case R.id.action_edit:
                mDataBinding.btnUpdate.setVisibility(View.VISIBLE);
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {

    }

    private OnItemClickListener.OnItemClickCallback onClick = new OnItemClickListener.OnItemClickCallback() {
        @Override
        public void onItemClicked(View view, int position) {

            /*Toast.makeText(context,"This feature shall be available soon",Toast.LENGTH_SHORT).show();*/

            if (user.getUser_type() == USER_TYPE_AGENT) {
                if (position == 0) {
                    Bundle b1 = new Bundle();
                    b1.putInt("transaction_type", TRANSACTION_CASH_IN);
                    CashInOptionsFragment cin = new CashInOptionsFragment();
                    cin.setArguments(b1);
                    addFragmentWithoutRemove(R.id.container_main, cin, CashInOptionsFragment.class.getSimpleName());
                } else if (position == 1) {
                    Bundle b1 = new Bundle();
                    b1.putInt("transaction_type", TRANSACTION_CASH_OUT);
                    FundWalletFragment cin = new FundWalletFragment();
                    cin.setArguments(b1);
                    //addFragmentWithoutRemove(R.id.container_main,cin,CashInFragment.class.getSimpleName());
                    addFragmentWithoutRemove(R.id.container_main, cin, FundWalletFragment.class.getSimpleName());
                } else if (position == 2) {

                    addFragmentWithoutRemove(R.id.container_main, new FundWalletFragment(), FundWalletFragment.class.getSimpleName());

                } else if (position == 4) {

                    addFragmentWithoutRemove(R.id.container_main, new PayServicesFragment(), PayServicesFragment.class.getSimpleName());

                } else if (position == 3) {

                    Bundle b1 = new Bundle();
                    //b1.putInt("transaction_type", Constants.TRANSACTION_CASH_IN);
                    TransactionsFragment cin = new TransactionsFragment();
                    //CashInFragment cin = new CashInFragment();
                    cin.setArguments(b1);
                    addFragmentWithoutRemove(R.id.container_main, cin, TransactionsFragment.class.getSimpleName());
                } else if (position == 5) {
                    addFragmentWithoutRemove(R.id.container_main, new HealthCheckerFragment(), HealthCheckerFragment.class.getSimpleName());
                } else if (position == 6) {
                    addFragmentWithoutRemove(R.id.container_main, new SettingsFragment(), SettingsFragment.class.getSimpleName());
                } else {
                    Toast.makeText(requireContext(), "More", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (position == 0) {
                    addFragmentWithoutRemove(
                            R.id.container_main,
                            new QuickTransactionFragment(),
                            QuickTransactionFragment.class.getSimpleName()
                    );
                    //login();
                } else if (position == 1) {
                    Bundle b1 = new Bundle();
                    TransactionsFragment cin = new TransactionsFragment();
                    cin.setArguments(b1);
                    addFragmentWithoutRemove(R.id.container_main, cin, TransactionsFragment.class.getSimpleName());
                } else if (position == 2) {

                    Bundle b1 = new Bundle();
                    b1.putInt("transaction_type", TRANSACTION_CASH_IN);
                    CashInOptionsFragment cin = new CashInOptionsFragment();
                    cin.setArguments(b1);
                    addFragmentWithoutRemove(R.id.container_main, cin, CashInOptionsFragment.class.getSimpleName());
                } else if (position == 3) {
                    addFragmentWithoutRemove(R.id.container_main, new PayServicesFragment(), PayServicesFragment.class.getSimpleName());
                } else {
                    addFragmentWithoutRemove(R.id.container_main, new SettingsFragment(), SettingsFragment.class.getSimpleName());
                }
            }

        }
    };


    public FundsTransferRequestModel getRequestModelForFundingWallet() {
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setPin("kghxqwveJ3eSQJip/cmaMQ==");
        transactionModel.setBankCode("033");//063
        transactionModel.setAmount(5);
        transactionModel.setDescription("Fund Wallet");
        //transactionModel.setDestination(Constants.ACCOUNT_NUMBER_DESTINATION);//2063449787
        transactionModel.setDestination(Constants.ACCOUNT_NUMBER_SOURCE);//2063449787
        transactionModel.setReference(generateUUID());
        //transactionModel.setSenderName("Ajay:2063449787:Vijay");
        transactionModel.setEndPoint("M");
        transactionModel.setCurrency("NGN");
        FundsTransferRequestModel fundsTransferRequestModel = new FundsTransferRequestModel();
        fundsTransferRequestModel.setDirection("request");
        fundsTransferRequestModel.setAction("FT");
        fundsTransferRequestModel.setTerminalId("7000000001");
        fundsTransferRequestModel.setTransaction(transactionModel);

        return fundsTransferRequestModel;
    }

    public FundsTransferRequestModel getRequestModelForTransactionStatus() {
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setPin("kghxqwveJ3eSQJip/cmaMQ==");
        transactionModel.setBankCode("033");//063
        //transactionModel.setAmount(5);
        transactionModel.setDescription("Transaction Status");
        //transactionModel.setDestination(Constants.ACCOUNT_NUMBER_DESTINATION);//2063449787
        transactionModel.setDestination(Constants.ACCOUNT_NUMBER_SOURCE);//2063449787
        transactionModel.setLineType("Others");
        transactionModel.setReference("ETZ691572687709360");//generateUUID()
        //transactionModel.setSenderName("Ajay:2063449787:Vijay");
        //transactionModel.setEndPoint("M");
        //transactionModel.setCurrency("NGN");
        FundsTransferRequestModel fundsTransferRequestModel = new FundsTransferRequestModel();
        fundsTransferRequestModel.setDirection("request");
        fundsTransferRequestModel.setAction("TS");
        fundsTransferRequestModel.setTerminalId("7000000001");
        fundsTransferRequestModel.setTransaction(transactionModel);

        return fundsTransferRequestModel;
    }

    public FundsTransferRequestModel getRequestModelForWalletQuery() {
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setPin("kghxqwveJ3eSQJip/cmaMQ==");
        transactionModel.setBankCode("033");//063
        transactionModel.setAmount(5);
        transactionModel.setDescription("Wallet Query");
        //transactionModel.setDestination(Constants.ACCOUNT_NUMBER_DESTINATION);//2063449787
        transactionModel.setDestination(Constants.ACCOUNT_NUMBER_SOURCE);//2063449787
        transactionModel.setLineType("Others");
        transactionModel.setReference(generateUUID());//"ETZ691572677986285"
        //transactionModel.setSenderName("Ajay:2063449787:Vijay");
        //transactionModel.setEndPoint("M");
        //transactionModel.setCurrency("NGN");
        FundsTransferRequestModel fundsTransferRequestModel = new FundsTransferRequestModel();
        fundsTransferRequestModel.setDirection("request");
        fundsTransferRequestModel.setAction("AQ");
        fundsTransferRequestModel.setTerminalId("7000000001");
        fundsTransferRequestModel.setTransaction(transactionModel);

        return fundsTransferRequestModel;
    }

    public FundsTransferRequestModel getRequestModel() {
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setPin("kghxqwveJ3eSQJip/cmaMQ==");
        transactionModel.setBankCode("033");//063
        transactionModel.setAmount(5);
        transactionModel.setDescription("Payment Fund Transfer");
        transactionModel.setDestination(Constants.ACCOUNT_NUMBER_DESTINATION);//2063449787
        transactionModel.setReference(generateUUID());
        transactionModel.setSenderName("Ram:" + Constants.ACCOUNT_NUMBER_SOURCE + ":Laxman");
        transactionModel.setEndPoint("A");
        transactionModel.setCurrency("NGN");
        FundsTransferRequestModel fundsTransferRequestModel = new FundsTransferRequestModel();
        fundsTransferRequestModel.setDirection("request");
        fundsTransferRequestModel.setAction("FT");
        fundsTransferRequestModel.setTerminalId("7000000001");
        fundsTransferRequestModel.setTransaction(transactionModel);

        return fundsTransferRequestModel;
    }

    public void encryptDecrypt() {
        try {
            String encryptedVal = AES.encrypt("0006", "KEd4gDNSDdMBxCGliZaC8w==");
            Log.e("EncryptedVal", encryptedVal + "--");
            String decryptedVal = AES.decrypt(encryptedVal, "KEd4gDNSDdMBxCGliZaC8w==");
            Log.e("DecryptedVal", decryptedVal + "--");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (KeyException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String generateUUID() {

        Long tsLong = System.currentTimeMillis();///1000;//ETZ691572523694353//ETZ691572523588
        String transaction_ref_id = "ETZ" + user.getUser_id() + tsLong.toString(); // This is your Transaction Ref id - Here we used as a timestamp -

        //String sOrderId= tsLong +"UPI";// This is your order id - Here we used as a timestamp -

        Log.e("TR Reference ID==>", "" + transaction_ref_id);

        //UUID uniqueKey = UUID.randomUUID();

        //String uniqueID = UUID.randomUUID().toString();

        /*String uniqueID = String.valueOf(UUID.randomUUID());//.getMostSignificantBits());
        Log.e("uniqueID",uniqueID+"--");*/
        return transaction_ref_id;
    }

    public void showProgressBar() {
        mProgressDialog = ProgressDialog.show(context, null, null);
        mProgressDialog.setContentView(R.layout.dialog_progress);
        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#70ffffff")));
        mProgressDialog.setCancelable(false);

        /*if (mProgressDialog!=null && !mProgressDialog.isShowing()){
            mProgressDialog.show();
        }*/
    }

    public void dismissProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void logout() {
        Prefs.clear();
        MqttHelper.disconnect();
        Intent intent = new Intent(this.getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        this.getActivity().finish();
    }

    public void getSettings() {
//        String token = SharedPrefManager.getAppToken();
//
//        if (token == null || token.isEmpty() || JWTHelper.isExpired(token)) {
//            logout();
//            return;
//        }
        Disposable subscribe = StormAPIClient
                .getFee(context, "posFees")
                .flatMap(res -> {
                    if (res.code() != 200) {
                        SharedPrefManager.setPOSConvenienceFee(Float.valueOf("0.75"));
                    } else {
                        JSONObject payload = new JSONObject(new Gson().toJson(res.body()));
                        String value = payload.optString("value");
                        if (Strings.emptyToNull(value) != null)
                            SharedPrefManager.setPOSConvenienceFee(Float.valueOf(payload.getString("value")));
                        else
                            SharedPrefManager.setPOSConvenienceFee(Float.valueOf("0.75"));

                    }
                    return StormAPIClient.getFee(context, "netplusPayTopUpFee");
                })
                .flatMap(res -> {
                    if (res.code() != 200) {
                        SharedPrefManager.setNetPlusPayConvenienceFee(Float.valueOf("1.5"));
                    } else {
                        JSONObject payload = new JSONObject(new Gson().toJson(res.body()));
                        String value = payload.optString("value");
                        if (Strings.emptyToNull(value) != null)
                            SharedPrefManager.setNetPlusPayConvenienceFee(Float.valueOf(value));
                        else
                            SharedPrefManager.setNetPlusPayConvenienceFee(Float.valueOf("1.5"));

                    }
                    return StormAPIClient.getFee(context, "transferFees");
                })
                .subscribe(res -> {
                    if (res.code() != 200) {
                        SharedPrefManager.setTransfeeConvenienceFee(Float.valueOf("40"));
                    } else {
                        JSONObject payload = new JSONObject(new Gson().toJson(res.body()));
                        String value = payload.optString("value");
                        if (Strings.emptyToNull(value) != null)
                            SharedPrefManager.setTransfeeConvenienceFee(Float.valueOf(value));
                        else
                            SharedPrefManager.setTransfeeConvenienceFee(Float.valueOf("40"));
                        SharedPrefManager.setTransfeeConvenienceFee(Float.valueOf("40"));
                    }
                }, t -> {
                    Log.e(getTag(), "An unexpected error occurred", t);
                    Toast.makeText(context, "An unexpected error occured", Toast.LENGTH_LONG).show();
                });
    }
}
