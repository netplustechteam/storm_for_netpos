package com.woleapp.ui.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.*;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.woleapp.BuildConfig;
import com.woleapp.R;
import com.woleapp.databinding.DialogBinding;
import com.woleapp.databinding.LayoutBankDetailsBinding;
import com.woleapp.databinding.LayoutCheckOutPageBinding;
import com.woleapp.databinding.QrBottomSheetDialogBinding;
import com.woleapp.db.AppDatabase;
import com.woleapp.db.Injection;
import com.woleapp.db.LocalCache;
import com.woleapp.db.UserViewModel;
import com.woleapp.db.dao.MerchantTransactionDao;
import com.woleapp.model.CashTransaction;
import com.woleapp.model.MerchantTransaction;
import com.woleapp.model.User;
import com.woleapp.network.APIServiceClient;
import com.woleapp.network.MerchantsApiClient;
import com.woleapp.network.StormAPIClient;
import com.woleapp.ui.activity.HomeActivity;
import com.woleapp.ui.activity.PaymentProgressActivity;
import com.woleapp.util.Constants;
import com.woleapp.util.OnItemClickListener;
import com.woleapp.util.SharedPrefManager;
import com.woleapp.util.Utilities;
import com.woleapp.util.UtilsAndExtensionsKt;
import com.woleapp.util.mobilevision.BarcodeCaptureActivity;
import com.woleapp.viewmodels.PaymentViewModel;
import com.woleapp.viewmodels.PaymentViewModelKt;
import com.woleapp.viewmodels.SalesViewModel;

import org.json.JSONObject;


import java.text.DecimalFormat;


import static com.woleapp.ui.activity.PaymentProgressActivity.KEY_AMOUNT;
import static com.woleapp.util.Constants.USER_TYPE_AGENT;
import static com.woleapp.util.UtilsAndExtensionsKt.encodeQueryValue;
import static com.woleapp.util.UtilsAndExtensionsKt.encodeToBase64String;
import static com.woleapp.viewmodels.PaymentViewModelKt.PAYMENT_MODE_CARD;
import static com.woleapp.viewmodels.PaymentViewModelKt.PAYMENT_MODE_CASH;
import static com.woleapp.viewmodels.PaymentViewModelKt.PAYMENT_MODE_PAYLINK;
import static com.woleapp.viewmodels.PaymentViewModelKt.PAYMENT_MODE_POS;
import static com.woleapp.viewmodels.PaymentViewModelKt.PAYMENT_MODE_QR;
import static com.woleapp.viewmodels.PaymentViewModelKt.PAYMENT_MODE_TRANSFER;
import static com.woleapp.viewmodels.PaymentViewModelKt.PAYMENT_STATUS_PENDING;


public class PaymentModeFragment extends BaseFragment implements View.OnClickListener {
    Context context;
    private LayoutCheckOutPageBinding binding;
    String amount = "";
    double amt_dbl = 0.0;
    private User user;
    private UserViewModel mViewModel;
    private SalesViewModel salesViewModel;
    float amt = 0.0f;
    double transaction_amount = 0.0;
    int transaction_type = 1;
    final int SCAN_QR_CODE = 1001, PAY_VIA_SMARTPESA = 1024;
    int result0, result1, result2;
    String currency_symbol = "";
    private static final int PERMISSION_REQUEST_CODE_CAMERA = 200,
            PERMISSION_REQUEST_CODE_BLUETOOTH = 201;

    String commodityName;
    String description;
    String productName;
    String sellerId, sellerName;
    Integer productId;
    Integer quantity;
    private String transactionRef = "";
    private String customerName;
    private float posFee;
    private float netplusFee;

    DecimalFormat format = new DecimalFormat("###,###.##");
    Utilities utilities;
    ProgressDialog mProgressDialog;
    private boolean external = false;
    private PaymentViewModel viewModel;
    private String generatedPaylink = null;
    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            Log.e("PaymentModeFragment", "onReceive");
            user = SharedPrefManager.getUser();
        }
    };
    private String convenienceFee;

    @Override
    public void onStart() {
        Log.e("onStart", "onStart");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myReceiver,
                new IntentFilter("UpdateUser"));
        super.onStart();
    }

    @Override
    public void onStop() {

        Log.e("onStop", "onStop");
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myReceiver);
        super.onStop();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        context = getActivity();
        utilities = new Utilities(context);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        user = SharedPrefManager.getUser();
        ((HomeActivity) getActivity()).setTitleWithNoNavigation("Dashboard");

        mViewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(getActivity()))
                .get(UserViewModel.class);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        currency_symbol = context.getResources().getString(R.string.lbl_currency_naira);
        Bundle b1 = getArguments();
        productId = b1.getInt("PRODUCTID");
        productName = b1.getString("PRODUCTNAME");
        quantity = b1.getInt("QUANTITY");
        customerName = b1.getString("CUSTOMER_NAME", "");
        sellerId = b1.getString("SELLER_ID", null);
        sellerName = b1.getString("SELLER_NAME", null);
        posFee = SharedPrefManager.getPOSConvenienceFee();
        netplusFee = SharedPrefManager.getNetPlusPayConvenienceFee();
        external = b1.containsKey("external");

//        if(BuildConfig.FLAVOR.contains("merchant")) {
            binding.cardQRPay.setVisibility(View.GONE);
            binding.payWithPaylink.setVisibility(View.GONE);
//        }

        Log.e("TAG", "Product id: " + productId + " Product Name: " + productName + " Quantity: " + quantity + " Customer Name: " + customerName);

        if (b1.containsKey("amount")) {
            amount = b1.getString("amount", "");
            amount = amount.replaceAll(",", "").replace(currency_symbol, "");
            convenienceFee = format.format(Double.valueOf(amount) > 200000 ? 1000 : (SharedPrefManager.getPOSConvenienceFee() / 100) * Double.valueOf(amount));
            amt_dbl = Double.parseDouble(amount);
            double amountPosFee = (posFee * amt_dbl) / 100;
            binding.posFee.setText(getString(R.string.convenience_fee, amountPosFee));
            double amountNetPlusFee = (netplusFee * amt_dbl) / 100;
            binding.cardFee.setText(getString(R.string.convenience_fee, amountNetPlusFee));
            binding.paylinkFee.setText(getString(R.string.convenience_fee, amountNetPlusFee));
            binding.tvAmount.setText(utilities.getFormattedAmount(amt_dbl));
            //binding.tvConvenienceFee.setText("(Convenience Fee Included: " + currency_symbol + convenienceFee + ")");
            //binding.tvConvenienceFee.setVisibility(View.VISIBLE);
            //Log.e("TAG", ""+convenienceFee);

        }

        if (b1.containsKey("transaction_type")) {
            transaction_type = b1.getInt("transaction_type", 1);
        }
        Log.e("TAG", "" + transaction_type);

        if (transaction_type == Constants.TRANSACTION_FUND_WALLET) {
            binding.cardAccountTransfer.setVisibility(View.GONE);
            binding.cardCashCollection.setVisibility(View.GONE);

        } else if (transaction_type == Constants.TRANSACTION_QUICK) {
            binding.cardAccountTransfer.setVisibility(View.GONE);
            binding.cardCashCollection.setVisibility(View.VISIBLE);

        } else if (transaction_type == Constants.TRANSACTION_CASH_OUT) {
            binding.cardCashCollection.setVisibility(View.GONE);
            binding.cardAccountTransfer.setVisibility(View.VISIBLE);

        } else {
            binding.cardAccountTransfer.setOnClickListener(this);
        }

        if (external) {
            binding.cardAccountTransfer.setVisibility(View.GONE);
            binding.payWithStormWallet.setVisibility(View.VISIBLE);
            binding.cardPosPayment.setVisibility(View.GONE);
            binding.payWithPaylink.setVisibility(View.GONE);
            binding.cardCashCollection.setVisibility(View.GONE);
        }


        binding.cardPosPayment.setOnClickListener(this);
        binding.cardAccountTransfer1.setOnClickListener(this);
        binding.cardAccountTransfer.setOnClickListener(this);
        binding.cardQRPay.setOnClickListener(this);
        binding.btnBack.setOnClickListener(this);
        binding.cardCashCollection.setOnClickListener(this);
        binding.payWithPaylink.setOnClickListener(this);
        description = b1.getString("DESCRIPTION");
        commodityName = b1.getString("COMMODITY_NAME");
        viewModel = new ViewModelProvider(this).get(PaymentViewModel.class);
        viewModel.setStormAPIService(StormAPIClient.create(requireContext()));
        viewModel.setStormMerchantApiService(MerchantsApiClient.getMerchantApiService(requireContext()));
        viewModel.getLoadingStatus().observe(getViewLifecycleOwner(), booleanEvent -> {
            Boolean showLoading = booleanEvent.getContentIfNotHandled();
            if (showLoading == null)
                return;
            Log.e("show dialog", "" + showLoading);
            if (showLoading)
                showProgressBar();
            else
                dismissProgressBar();
        });
        viewModel.getProceedToPayment().observe(getViewLifecycleOwner(), paymentResponseEvent -> {
            Toast.makeText(context, "OKAY", Toast.LENGTH_SHORT).show();
            dismissProgressBar();
            MerchantTransaction merchantTransaction = paymentResponseEvent.getContentIfNotHandled();
            if (merchantTransaction != null) {
                proceedToPayment(merchantTransaction);
            } else {
                Toast.makeText(getContext(), "Failed, Try again", Toast.LENGTH_SHORT).show();
            }
        });
        viewModel.getPaymentResponse().observe(getViewLifecycleOwner(), booleanEvent -> {
            Boolean e = booleanEvent.getContentIfNotHandled();
            if (e != null && e) {
                addFragmentWithoutRemove(R.id.container_main, new PaymentSuccessFragment
                        (), PaymentModeFragment.class.getSimpleName());
            }
        });
    }

    private void proceedToPayment(MerchantTransaction merchantTransaction) {
        if (merchantTransaction.getPaymentMethod().equals(PAYMENT_MODE_CASH))
            viewModel.logCashTransaction();
        else if (merchantTransaction.getPaymentMethod().equals(PaymentViewModelKt.PAYMENT_MODE_TRANSFER)) {
            showDialogForAccountTransfer(merchantTransaction);
        } else if (merchantTransaction.getPaymentMethod().equals(PaymentViewModelKt.PAYMENT_MODE_POS)) {
            transactionRef = merchantTransaction.getReference();
            Intent broadcast = new Intent("UpdateUser");
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(broadcast);
            //TODO Removed the AccountActivation process for merchant
            requestPermissionForBluetooth();
        } else if (merchantTransaction.getPaymentMethod().equals(PAYMENT_MODE_PAYLINK)) {
            String paylinkBaseUrl = "https://paylink.netpluspay.com?p=";
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("a_id", user.getNetplus_id());
            jsonObject.addProperty("ref", merchantTransaction.getReference());
            //jsonObject.addProperty("amount", amount);
            String pName;
            if (transaction_type == Constants.TRANSACTION_FUND_WALLET) {
                pName = "FUND WALLET";
            } else if (transaction_type == Constants.TRANSACTION_CASH_OUT)
                pName = "CASH OUT";
            else {
                pName = productName == null ? "PRODUCT" : productName;
            }
            jsonObject.addProperty("amount", "" + amt_dbl);
            jsonObject.addProperty("p_name", pName);
            jsonObject.addProperty("type", "paylink");
            jsonObject.addProperty("agent", user.getBusiness_name());
            //jsonObject.addProperty("product_name", productName);
            Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(paylinkBaseUrl.concat(encodeQueryValue(encodeToBase64String(jsonObject.toString())))))
                    .setDomainUriPrefix("https://plink.netpluspay.com")
                    // Set parameters
                    // ...
                    .buildShortDynamicLink()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            this.generatedPaylink = shortLink.toString();
                            showGeneratedPaylinkShareIntent();
//                            Log.e("TAG shortlink", shortLink.toString());
//                            Uri flowchartLink = task.getResult().getPreviewLink();
//                            Log.e("TAG flowchart link", flowchartLink.toString());
                        } else {
                            Toast.makeText(context, "An error occurred", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else if (merchantTransaction.getPaymentMethod().equals(PAYMENT_MODE_QR)) {
            showQRBottomSheetDialog();
        } else if (merchantTransaction.getPaymentMethod().equals(PAYMENT_MODE_CARD)) {
            Bundle b1 = new Bundle();
            b1.putDouble(KEY_AMOUNT, amt_dbl);
            b1.putString("ref", merchantTransaction.getReference());
            b1.putString("a_id", merchantTransaction.getMerchantId());
            b1.putString("agent", user.getBusiness_name());
            b1.putString("type", "card");
            b1.putString("id", String.valueOf(productId));
            b1.putString("SELLER_ID", sellerId);
            b1.putString("SELLER_NAME", sellerName);
            proceedToCardPayment(b1);
        }
    }

    private void showQRBottomSheetDialog() {
        QrBottomSheetDialogBinding qrBottomSheetDialogBinding = QrBottomSheetDialogBinding.inflate(getLayoutInflater(), null, false);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireContext(), R.style.SheetDialog);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setContentView(qrBottomSheetDialogBinding.getRoot());
        qrBottomSheetDialogBinding.closeBtn.setOnClickListener(v -> bottomSheetDialog.cancel());
        bottomSheetDialog.show();
    }

    private void showDialogForAccountTransfer(MerchantTransaction merchantTransaction) {
        LayoutBankDetailsBinding bankDetailsBinding = LayoutBankDetailsBinding.inflate(getLayoutInflater(), null, false);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireContext(), R.style.SheetDialog);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setContentView(bankDetailsBinding.getRoot());
        String bank = "GTB";
        String accountNumber = "0597024646";
        String accountName = "NETPLUS/STORM";
        String accountNumber2 = "2684362099";
        String bank2 = "FCMB";
        bankDetailsBinding.accountNumber2.setText(accountNumber2);
        bankDetailsBinding.bank2.setText(bank2);
        bankDetailsBinding.accountNumber.setText(accountNumber);
        bankDetailsBinding.bank.setText(bank);
        bankDetailsBinding.accountName.setText(accountName);
        String ref = "Please use the code " + merchantTransaction.getReference() + " as a reference during transfer or payment";
        bankDetailsBinding.reference.setText(ref);
        bankDetailsBinding.accountNumber.setOnClickListener(v -> {
            UtilsAndExtensionsKt.copyTextToClipboard(requireContext(), "Account Number", accountNumber);
            Toast.makeText(requireContext(), "Account number copied to clipboard", Toast.LENGTH_SHORT).show();
        });
        bankDetailsBinding.accountNumber2.setOnClickListener(v -> {
            UtilsAndExtensionsKt.copyTextToClipboard(requireContext(), "Account Number", accountNumber2);
            Toast.makeText(requireContext(), "Account number copied to clipboard", Toast.LENGTH_SHORT).show();
        });
        bankDetailsBinding.tap.setOnClickListener(v -> {
            UtilsAndExtensionsKt.copyTextToClipboard(requireContext(), "Reference", "" + ref);
            Toast.makeText(requireContext(), "Reference copied to clipboard", Toast.LENGTH_SHORT).show();
        });
        bankDetailsBinding.btnDone.setOnClickListener(v -> {
            if (bottomSheetDialog.isShowing())
                bottomSheetDialog.cancel();
            showFragment(new DashboardFragment(), DashboardFragment.class.getSimpleName());
        });
        bottomSheetDialog.show();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        //user = SharedPrefManager.getInstance(context).getUser();
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_check_out_page, container, false);
        //((GPHMainActivity)getActivity()).createBackButton(job_title);
        return binding.getRoot();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);

        menu.clear();

    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {


        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void proceedToCardPayment(Bundle b1) {
        WebViewFragment webViewFragment = new WebViewFragment();
        webViewFragment.setArguments(b1);
        addFragmentWithoutRemove(R.id.container_main, webViewFragment, WebViewFragment.class.getSimpleName());
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {

        if (v == binding.cardPosPayment) //cardAccountTransfer // Etranzact Funds transfer API
        {
            Log.d("AAAA", "BBBBBBB");
            //Toast.makeText(context,"This feature shall be available soon",Toast.LENGTH_SHORT).show();

            if (transaction_type == Constants.TRANSACTION_FUND_WALLET || transaction_type == Constants.TRANSACTION_CASH_OUT) {
                //updateUser(amount);
                requestPermissionForBluetooth();
            } else {

                Log.d("AAAA", "BBBBBBB");
                MerchantTransaction merchantTransaction =
                        new MerchantTransaction(
                                UtilsAndExtensionsKt.getSixDigitRandomNumber(),
                                user.getNetplus_id(),
                                PAYMENT_MODE_POS,
                                customerName,
                                PAYMENT_STATUS_PENDING,
                                "" + amt_dbl,
                                "" + productId,
                                "" + quantity
                        );
                Gson gson = new Gson();
                Log.d("payloadd", gson.toJson(merchantTransaction));
                viewModel.setMerchantTransaction(merchantTransaction);
                viewModel.logNewTransaction();
            }

        } else if (v == binding.cardAccountTransfer1) {
            if (transaction_type == Constants.TRANSACTION_QUICK) {
                MerchantTransaction merchantTransaction =
                        new MerchantTransaction(
                                "SC".concat(sellerId.substring(0, 4)).concat(UtilsAndExtensionsKt.getSixDigitRandomNumber()),
                                user.getNetplus_id(),
                                PAYMENT_MODE_CARD,
                                customerName,
                                PAYMENT_STATUS_PENDING,
                                "" + amt_dbl,
                                "" + productId,
                                "" + quantity
                        );
                viewModel.setMerchantTransaction(merchantTransaction);
                viewModel.logNewTransaction();
            } else {
                Bundle b1 = new Bundle();
                b1.putDouble(KEY_AMOUNT, amt_dbl);
                b1.putString("ref", "SC".concat(user.getNetplus_id().substring(0, 4)).concat(UtilsAndExtensionsKt.getSixDigitRandomNumber()));
                b1.putString("a_id", user.getNetplus_id());
                b1.putString("type", "card");
                b1.putString("agent", user.getBusiness_name());
                proceedToCardPayment(b1);
            }
        } else if (v == binding.cardQRPay) {
            MerchantTransaction merchantTransaction =
                    new MerchantTransaction(
                            UtilsAndExtensionsKt.getSixDigitRandomNumber(),
                            user.getNetplus_id(),
                            PAYMENT_MODE_QR,
                            customerName,
                            PAYMENT_STATUS_PENDING,
                            "" + amt_dbl,
                            "" + productId,
                            "" + quantity
                    );
            viewModel.setMerchantTransaction(merchantTransaction);
            viewModel.logNewTransaction();

        } else if (v == binding.btnBack) {
            getActivity().getSupportFragmentManager().popBackStack();
        } else if (v == binding.payWithPaylink) {
            if (generatedPaylink != null) {
                showGeneratedPaylinkShareIntent();
                return;
            }
            MerchantTransaction merchantTransaction =
                    new MerchantTransaction(
                            "SP".concat(user.getNetplus_id().substring(0, 4)).concat(UtilsAndExtensionsKt.getSixDigitRandomNumber()),
                            user.getNetplus_id(),
                            PAYMENT_MODE_PAYLINK,
                            customerName,
                            PAYMENT_STATUS_PENDING,
                            "" + amt_dbl,
                            "" + productId,
                            "" + quantity
                    );
            viewModel.setMerchantTransaction(merchantTransaction);
            if (transaction_type == Constants.TRANSACTION_FUND_WALLET || transaction_type == Constants.TRANSACTION_CASH_OUT)
                proceedToPayment(merchantTransaction);
            else
                viewModel.logNewTransaction();
            //Toast.makeText(context, "Generating a PayLink, Please Wait", Toast.LENGTH_SHORT).show();

        } else if (v == binding.cardCashCollection) {
            Toast.makeText(getContext(), "Cash collection", Toast.LENGTH_SHORT).show();
            Toast.makeText(context, "name" + customerName, Toast.LENGTH_SHORT).show();
            /*if (productId == 0 || productId == null) {
                addFragmentWithoutRemove(R.id.container_main, new PaymentSuccessFragment
                        (), PaymentSuccessFragment.class.getSimpleName());
                return;
            }*/
            if (productId != null) {
                MerchantTransactionDao transactionsDao = AppDatabase.getInstance(requireContext()).transactionsDao();
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                DialogBinding dialogBinding = DialogBinding.inflate(getLayoutInflater(), null, false);
                builder.setView(dialogBinding.getRoot());
                AlertDialog dialog = builder.create();
                dialogBinding.tvTitle.setText("Payment Confirmation");
                dialogBinding.tvMessage.setText("Tap yes to confirm this transaction");
                dialogBinding.tvNo.setOnClickListener(v12 -> {
                    if (dialog.isShowing())
                        dialog.cancel();
                });
                long time = System.currentTimeMillis();
                dialogBinding.tvYes.setOnClickListener(v1 -> {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    CashTransaction cashTransaction =
                            new CashTransaction(
                                    user.getNetplus_id(),
                                    user.getTerminal_id(),
                                    user.getBusiness_name(),
                                    "csh-" + time,
                                    "UTL-" + time,
                                    "Success",
                                    amt_dbl,
                                    "Sales-Cash",
                                    description,
                                    "Sales",
                                    "storm_app",
                                    ""

                            );
                    MerchantTransaction merchantTransaction =
                            new MerchantTransaction(
                                    UtilsAndExtensionsKt.getSixDigitRandomNumber(),
                                    user.getNetplus_id(),
                                    PAYMENT_MODE_CASH,
                                    customerName,
                                    PAYMENT_STATUS_PENDING,
                                    "" + amt_dbl,
                                    "" + productId,
                                    "" + quantity,
                                    sellerId
                            );
                    viewModel.setMerchantTransaction(merchantTransaction);
                    viewModel.setCashTransaction(cashTransaction);
                    viewModel.logNewTransaction();
                    //showProgressBar();
                });
                dialog.show();
            }
        } else if (v == binding.cardAccountTransfer) {
            MerchantTransaction merchantTransaction =
                    new MerchantTransaction(
                            "",
                            user.getNetplus_id(),
                            PAYMENT_MODE_TRANSFER,
                            customerName,
                            PAYMENT_STATUS_PENDING,
                            "" + amt_dbl,
                            "" + productId,
                            "" + quantity
                    );
            viewModel.setMerchantTransaction(merchantTransaction);
            viewModel.getSessionCode(transaction_type);
        }
    }

    private void showGeneratedPaylinkShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        try {
            sendIntent.putExtra(Intent.EXTRA_TEXT, generatedPaylink);
        } catch (Exception e) {
            e.printStackTrace();
            sendIntent.putExtra(Intent.EXTRA_TEXT, generatedPaylink);
        }
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }


    public void showProgressBar() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(context, null, null);
            mProgressDialog.setContentView(R.layout.dialog_progress);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#70ffffff")));
            mProgressDialog.setCancelable(false);


        } else {
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }
    }

    public void dismissProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void notifyWalletCredits(JsonObject jsonObject) {


        APIServiceClient.notifyWalletCredits(jsonObject, new APIServiceClient.ApiCallback2() {
            @Override
            public void onStart() {
                showProgressBar();
                Log.e("onStart", "onStart");
            }

            @Override
            public void onEnd() {
                Log.e("onEnd", "onEnd");
                dismissProgressBar();
            }


            @Override
            public void onResponse(Object response)//ResponseEnvelope response
            {
                if (response instanceof JSONObject) {
                    JSONObject jsonObject1 = (JSONObject) response;
                    String message = jsonObject1.optString("message", "No response obtained");
                    if (!message.equalsIgnoreCase("No response obtained")) {
                        String amount = jsonObject1.optString("amount", "0.0");
                        message = "Your wallet has been credited. New amount is " + utilities.getFormattedAmount(Double.valueOf(amount));
                        ((HomeActivity) getActivity()).showD(true, message);
                    }
                }
            }

            @Override
            public void onFail(String msg) {
                Log.e("onFail", msg + "###");
                Toast.makeText(context, msg + "", Toast.LENGTH_SHORT).show();
                dismissProgressBar();
            }
        }, context);
    }

    private void proceedToPayment() {

        Double amt = Double.parseDouble(amount.replaceAll(",", "").replace(currency_symbol, ""));
        Double fee = Double.parseDouble(convenienceFee.replaceAll(",", "").replace(currency_symbol, ""));

        Bundle bundle = new Bundle();
        bundle.putDouble(KEY_AMOUNT, amt);
        bundle.putDouble("convenience_fee", fee);
        bundle.putInt("transaction_type", Constants.TRANSACTION_CASH_OUT);
        bundle.putString("COMMODITY_NAME", commodityName);
        bundle.putString("DESCRIPTION", description);
        bundle.putInt("QUANTITY", quantity);
        bundle.putInt("PRODUCTID", productId);
        bundle.putString("PRODUCTNAME", productName);
        bundle.putString("TRANSACTION_REF", transactionRef);
        bundle.putString("SELLERID", sellerId);
        Intent i = new Intent(getActivity(), PaymentProgressActivity.class);
        i.putExtras(bundle);
        requireActivity().startActivityForResult(i, PAY_VIA_SMARTPESA);
    }

    public void updateUser(String amount) {

        if (amount != null) {

            amt = Float.parseFloat(amount) + Float.parseFloat(user.getAvailableBalance());

        } else {

            amt = Float.parseFloat(amount);

        }

        //user.setIs_verified(true);
        LocalCache.UpdateCallback callback = new LocalCache.UpdateCallback() {
            @Override
            public void updateFinished(int result) {
                if (result > 0) {
                    user.setAvailableBalance(String.valueOf(amt));
                    SharedPrefManager.setUser(user);
                    //Toast.makeText(context,"Registration successful",Toast.LENGTH_SHORT).show();

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            addFragmentWithoutRemove(R.id.container_main, new PaymentSuccessFragment(), PaymentModeFragment.class.getSimpleName());
                            ((HomeActivity) getActivity()).setAvailableBalance(String.valueOf(amt));

                        }
                    });
                    //showFragment(new LoginFragment(), LoginFragment.class.getSimpleName());
                    //showFragment(new OTPFragment(),OTPFragment.class.getSimpleName());
                    //addFragmentWithoutRemove(R.id.container_main,new OTPFragment(), OTPFragment.class.getSimpleName());
                } else {
                    /*getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            utilities.showAlertDialogOk("Record already exists. Please login instead.");
                        }
                    });*/

                }
            }
        };

        Log.e("UserIdToBeUpdated", user.getUser_id() + "--");
        mViewModel.updateWalletAmount(user.getUser_id(), String.valueOf(amt), callback);
    }

    private OnItemClickListener.OnItemClickCallback onClick = new OnItemClickListener.OnItemClickCallback() {
        @Override
        public void onItemClicked(View view, int position) {

            if (user.getUser_type() == USER_TYPE_AGENT) {
                if (position == 0) {
                    addFragmentWithoutRemove(R.id.container_main, new FundWalletFragment(), FundWalletFragment.class.getSimpleName());
                }
            } else {
                if (position == 0) {
                    addFragmentWithoutRemove(R.id.container_main, new QuickTransactionFragment(), QuickTransactionFragment.class.getSimpleName());
                }
            }

        }
    };

    private void requestPermissionForBluetooth() {
        result0 = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result0 == PackageManager.PERMISSION_GRANTED) {
            proceedToPayment();
        } else {
            String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION};//, Manifest.permission.BLUETOOTH
            requestPermissions(
                    permissions,
                    PERMISSION_REQUEST_CODE_BLUETOOTH);
        }

    }

    private void requestPermissionForCamera() {

        //ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_CAMERA);

        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_CAMERA);
        //,Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_NETWORK_STATE
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE_CAMERA:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (cameraAccepted)// && phoneStateAccepted && networkAccepted)
                    {

                        Intent i1 = new Intent(getActivity(), BarcodeCaptureActivity.class);
                        startActivityForResult(i1, SCAN_QR_CODE);

                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_NETWORK_STATE},
                                    PERMISSION_REQUEST_CODE_CAMERA);
                        }
                    }
                }
                break;

            case PERMISSION_REQUEST_CODE_BLUETOOTH:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //showLoginActivity();
                            proceedToPayment();
                        }
                    }, 300);
                } else {
                    Toast.makeText(context, "Cannot proceed until the permission is granted", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void scanQRCode() {
        try {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                result0 = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);

                if (result0 == 0)// && result1 == 0 && result2 == 0)//result0 == 0 && result1 == 0
                {

                    Intent i1 = new Intent(getActivity(), BarcodeCaptureActivity.class);
                    startActivityForResult(i1, SCAN_QR_CODE);
                } else {
                    requestPermissionForCamera();
                }
            } else {

                Intent i1 = new Intent(getActivity(), BarcodeCaptureActivity.class);
                startActivityForResult(i1, SCAN_QR_CODE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//Barcode read: MERCHANT_MERCHANT#1
        Log.e("PaymentModeFragment", "onActivityResult: " + requestCode + " , " + resultCode);
        if (data != null) {

        }
        if (requestCode == SCAN_QR_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);

                    String result = barcode.displayValue;
                    if (result != null) {
                        Log.e("QR", "Barcode read: " + result);////                try
                        Toast.makeText(context, "Your Payment to merchant " + result + " was successful", Toast.LENGTH_LONG).show();
                        showFragment(new DashboardFragment(), DashboardFragment.class.getSimpleName());
                    } else {
                        //Log.e("QR", "Barcode read: " + result);////                try
                        Toast.makeText(context, "Payment Failed", Toast.LENGTH_SHORT).show();
                    }
                    /*try
                    {
                        JSONObject jobj = new JSONObject(result);
                        String propertyId = jobj.optString("propertyId","");
                        getPropertyDetails(propertyId);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        showDialog("");
                    }*/

                } else {
                    Log.e("QR", "No barcode captured, intent data is null");
                    //showDialog("");
                }
            } else {
//                statusMessage.setText(String.format(getString(R.string.barcode_error),
//                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    private Bitmap textToImage(String text, int width, int height) throws WriterException, NullPointerException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.DATA_MATRIX.QR_CODE,
                    width, height, null);
        } catch (IllegalArgumentException Illegalargumentexception) {
            return null;
        }

        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();
        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        int colorWhite = 0xFFFFFFFF;
        int colorBlack = 0xFF000000;

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;
            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ? colorBlack : colorWhite;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }
}
