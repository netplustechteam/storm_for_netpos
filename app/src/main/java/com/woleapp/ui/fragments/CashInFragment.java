package com.woleapp.ui.fragments;

import static com.woleapp.ui.activity.PaymentProgressActivity.KEY_AMOUNT;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.woleapp.R;
import com.woleapp.Repository;
import com.woleapp.databinding.LayoutCashInBinding;
import com.woleapp.model.TransferModel;
import com.woleapp.model.User;
import com.woleapp.model.kayodeStormImplementation.DebitUserWalletRequestBody;
import com.woleapp.model.kayodeStormImplementation.GetBankListResponse;
import com.woleapp.model.kayodeStormImplementation.VerifyAccountNameRequestBody;
import com.woleapp.ui.activity.HomeActivity;
import com.woleapp.ui.activity.SignatureActivity;
import com.woleapp.ui.activity.SuccessActivity;
import com.woleapp.util.Constants;
import com.woleapp.util.RandomNumUtil;
import com.woleapp.util.SharedPrefManager;
import com.woleapp.util.Singletons;
import com.woleapp.util.Utilities;
import com.woleapp.viewmodels.NewStormServiceViewModel;
import com.woleapp.viewmodels.TransferViewModel;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

@AndroidEntryPoint
public class CashInFragment extends BaseFragment implements View.OnClickListener, Constants {
    @Inject
    Repository repository;

    NewStormServiceViewModel newServiceViewModel;

    HashMap<String, String> supportedBanks = new HashMap<>();

    List<String> supportedBankCodes = new ArrayList<>();
    List<String> supportedBankName = new ArrayList<>();

    @Inject
    public CashInFragment() {
    }

    ListView simpleListView;

    Context context;
    private LayoutCashInBinding binding;

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    User user;
    Drawable customErrorDrawable;

    Signature mSignature;
    View signature_view;
    Bitmap bitmap;
    boolean hasSigned = false;

    LinearLayout mContent;
    File file;
    int result0, transaction_type = 0;
    private static final int PERMISSION_REQUEST_CODE = 200;
    String mode = "", selected_bank_code = "";
    double amount = 0.0;
    Utilities utilities;
    String[] bankCodes;
    ProgressDialog mProgressDialog;
    TransferViewModel viewModel;

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", CashInFragment.class.getSimpleName() + "--");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("onPause", CashInFragment.class.getSimpleName() + "--");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        context = getActivity();
        utilities = new Utilities(context);
        viewModel = new ViewModelProvider(this).get(TransferViewModel.class);
        newServiceViewModel = new NewStormServiceViewModel(repository);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        user = SharedPrefManager.getUser();
        customErrorDrawable = context.getResources().getDrawable(R.drawable.error_small);
        customErrorDrawable.setBounds(0, 0, customErrorDrawable.getIntrinsicWidth(), customErrorDrawable.getIntrinsicHeight());
        bankCodes = context.getResources().getStringArray(R.array.bank_codes);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //populate();

        binding.btnVerifyAccountName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call get account name
                getAccountName();
            }
        });

        Bundle b1 = getArguments();
        if (b1 != null) {
            if (b1.containsKey("transaction_type")) {
                transaction_type = b1.getInt("transaction_type", 0);
                mode = b1.getString("mode", "");
                if (mode.equalsIgnoreCase("cash")) {
                    binding.etFee.setVisibility(View.GONE);
                }
                amount = b1.getDouble(KEY_AMOUNT);
                binding.tvAmount.setText(utilities.getFormattedAmount(amount));
                if (transaction_type == Constants.TRANSACTION_CASH_IN) {
                    binding.etFee.setVisibility(View.VISIBLE);
                    binding.tvTitle.setText("CASH-IN / BANK TRANSFER");
                } else {
                    binding.tvTitle.setText("CASH-OUT");
                }
            }
        }
        setListeners();

        setSpinner();

        prepareCanvas();

        if (checkPermissions()) {
            String filePath = getFilePath();
            file = new File(filePath);
        } else {
            requestPermission();
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_cash_in, container, false);
        return binding.getRoot();
    }

    public void setListeners() {
        binding.tvRetake.setOnClickListener(this);
        binding.etFee.setKeyListener(null);
        binding.btnContinue.setOnClickListener(this);
        binding.etAccountNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    return;
                }

                if (selected_bank_code == null || selected_bank_code.isEmpty()) {
                    return;
                }
                //return if bank account not selected
                String acc_no = binding.etAccountNo.getText().toString().trim();
                if (acc_no == null || acc_no.isEmpty() || acc_no.length() != 10) {
                    return;
                }

            }
        });

    }

    public void setSpinner() {
        // Create the observer which updates the UI.
        newServiceViewModel.getSupportedBanks();
        final Observer<List<GetBankListResponse>> observer = new Observer<List<GetBankListResponse>>() {
            @Override
            public void onChanged(@Nullable final List<GetBankListResponse> supported) {
                // Update the UI, in this case, a TextView.
                List<GetBankListResponse> supportedBank = RandomNumUtil.INSTANCE.sortListOfBanksAlphabetically(supported);
                int count = 0;
                while (count < supportedBank.size()) {
                    supportedBankCodes.add(supportedBank.get(count).getBank_code());
                    supportedBankName.add(supportedBank.get(count).getBank_name().toUpperCase(Locale.ROOT));
                    count++;
                }
            }
        };
        newServiceViewModel.getSupportedBanks().observe(requireActivity(), observer);
//        ArrayAdapter<String> adapters = ArrayAdapter.(getActivity(), supportedBanks.values().toArray(), R.layout.spinner_view_bank);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.banks, R.layout.spinner_view_bank);


        List<String> listOfString = new ArrayList<>();

        for (Map.Entry<String, String> each : supportedBanks.entrySet()) {
            String value = each.getValue();
            listOfString.add(value);
        }

        String[] arrayOfStringBankCode = new String[supportedBankCodes.size()];

        for (int i = 0; i < supportedBankCodes.size(); i++) {
            arrayOfStringBankCode[i] = supportedBankCodes.get(i);
        }
        String[] arrayOfStringBankName = new String[supportedBankName.size()];

        for (int i = 0; i < supportedBankName.size(); i++) {
            arrayOfStringBankName[i] = supportedBankName.get(i).toUpperCase(Locale.ROOT);
        }

        ArrayAdapter<String> newAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, arrayOfStringBankName);
        AutoCompleteTextView textView = (AutoCompleteTextView)
                binding.spnBank;
//        textView.setAdapter(newAdapter);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.item_text_view, supportedBankName);

        adapter.setDropDownViewResource(R.layout.item_drop_down_bank);
        newAdapter.setDropDownViewResource(R.layout.item_drop_down_bank);
//        arrayAdapter.setDropDownViewResource(R.layout.item_drop_down_bank);

        binding.spnBank.setAdapter(arrayAdapter);

        binding.spnBank.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("Position", i + "---");
                selected_bank_code = supportedBankCodes.get(i);
            }
        });

        binding.spnBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.e("Position", position + "---");
                if (position > 0) {

                    selected_bank_code = arrayOfStringBankCode[position - 1];
                    Log.e("selected_bank_code", selected_bank_code + "---");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spnBank.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    return;

                //return if bank code not selected
                if (selected_bank_code == null || selected_bank_code.isEmpty()) {
                    return;
                }
                //return if bank account not selected
                String acc_no = binding.etAccountNo.getText().toString().trim();
                if (acc_no == null || acc_no.isEmpty() || acc_no.length() != 10) {
                    return;
                }
            }
        });
    }

    private void getAccountName() {

        String appToken = SharedPrefManager.getAppToken();
        String acc_no = binding.etAccountNo.getText().toString().trim();

        VerifyAccountNameRequestBody accountToVerify = new VerifyAccountNameRequestBody(
                acc_no, selected_bank_code, SharedPrefManager.getUser().getNetplus_id()
        );

        showProgressBar();
        compositeDisposable.add(
                repository.verifyAccountName(
                        accountToVerify
                )
                        .subscribe(res -> {

                            if (res.code() != 200) {
                                Toast.makeText(context, "Unable to retrieve bank account name at this time please try later", Toast.LENGTH_LONG).show();
                                dismissProgressBar();
                                return;
                            }
                            String accountName = "";
                            if (res.body().getMessage() != null) {
                                accountName = res.body().getMessage();
                            }

                            binding.etName.setText(accountName);
                            dismissProgressBar();

                        }, t -> {
                            Log.e(getTag(), "An unexpected error occurred", t);
                            Toast.makeText(context, "An unexpected error occured", Toast.LENGTH_LONG).show();
                            ;
                            dismissProgressBar();
                        })
        );

    }

    private void onFocuOut() {

    }

    public boolean priceValidation(String price) {
        //^[1-9][0-9]{12,16}$ (old regular expression)
//		String regex = "^[+][0-9]{12,16}$";
        String regex = "[+-]?([0-9]*[.])?[0-9]+";//"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)";
        //String regex = "^[+][0-9]{10,13}$";
        Pattern numberPattern = Pattern.compile(regex);
        boolean result = numberPattern.matcher(price).matches();
        Log.e("Result: ", result + "--");
        return result;
    }

    public void prepareCanvas() {

        //mContent = (LinearLayout) dialog.findViewById(R.id.linearLayout);
        mContent = binding.linearCanvas;
        mSignature = new Signature(context, null);
        mSignature.setBackgroundColor(Color.WHITE);
        //mSignature.getParent().requestDisallowInterceptTouchEvent(true);

        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        //binding.linearNext.setEnabled(false);
        hasSigned = false;
//        mClear = (Button) dialog.findViewById(R.id.clear);
//        mGetSign = (Button) dialog.findViewById(R.id.getsign);
//        mGetSign.setEnabled(false);
//        mCancel = (Button) dialog.findViewById(R.id.cancel);

//        LinearLayout temp = mContent;
//        temp.setBackgroundResource(0);
//        signature_view = temp;
//        signature_view.layout();

        signature_view = mContent; // Original

//        mClear.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Log.v("log_tag", "Panel Cleared");
//                mSignature.clear();
//                mGetSign.setEnabled(false);
//            }
//        });
//
//        mGetSign.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//
//                Log.v("log_tag", "Panel Saved");
//                signature_view.setDrawingCacheEnabled(true);
//                //mSignature.save(signature_view, StoredPath);
//                Log.e("Path",file.getPath()+"");
//                mSignature.save(signature_view, file.getPath());
//
//                dialog.dismiss();
//                Toast.makeText(getApplicationContext(), "Successfully Saved", Toast.LENGTH_SHORT).show();
//                // Calling the same class
//                recreate();
//
//            }
//        });
//
//        mCancel.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Log.v("log_tag", "Panel Canceled");
//                dialog.dismiss();
//                // Calling the same class
//                recreate();
//            }
//        });
//        dialog.show();
    }

    public boolean checkPermissions() {
        try {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                result0 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                //result0 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);
                //result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (result0 == 0)//result0 == 0 && result1 == 0
                {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    //boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    //boolean phoneStateAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    //boolean networkAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (storageAccepted) {
                        String filePath = getFilePath();
                        file = new File(filePath);//getOutputMediafile(1);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PERMISSION_REQUEST_CODE);
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.clear();

//        menu.findItem(R.id.action_edit).setVisible(true);
//        menu.findItem(R.id.action_search).setVisible(false);
        //menu.removeItem(R.id.action_search);//clear();

        /*if(showDeleteMenu)
        {
            menu.findItem(R.id.action_search).setVisible(false);
            menu.removeItem(R.id.action_search);//clear();
        }
        else
        {
            menu.clear();

//            menu.removeItem(R.id.action_search);//clear();
//            menu.removeItem(R.id.action_search);//clear();
//
//            menu.findItem(R.id.action_search).setVisible(false);
//
//            menu.findItem(R.id.action_search).setVisible(false);
        }*/

    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            /*case R.id.action_edit:
                mDataBinding.btnUpdate.setVisibility(View.VISIBLE);
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        if (v == binding.btnContinue) {

            //return if bank code not selected
            if (selected_bank_code == null || selected_bank_code.isEmpty()) {
                Toast.makeText(context, "An unexpected error occured", Toast.LENGTH_LONG).show();
                return;
            }

            validateAndProceed();

        } else if (v == binding.linearCanvas) {
            //proceedToSignature();
            showSignaturePad();
        } else if (v == binding.tvRetake) {
            mSignature.clear();
            hasSigned = false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        Log.e("RequestCode: " + requestCode, "ResultCode " + resultCode);
    }

    public void proceedToSignature() {
        Intent i1 = new Intent(getActivity(), SignatureActivity.class);
        getActivity().startActivityForResult(i1, 1204);
    }

    public void hideKeyBoard() {
        try {
            if (context != null) {
                InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

                // check if no view has focus:
                View v = ((Activity) context).getCurrentFocus();
                if (v == null) return;

                inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void validateAndProceed() {

        //String amount = binding.etPrice.getText().toString().trim();
        String name = binding.etName.getText().toString().trim();
        String acc_no = binding.etAccountNo.getText().toString().trim();
        String selected_bank = selected_bank_code;

        Log.e("selected_bank_pos: ", selected_bank + "---");

        if (TextUtils.isEmpty(name)) {
            binding.etName.setError("Name is required", customErrorDrawable);
            binding.etName.requestFocus();
        } else if (binding.etTransactionPin.getText().toString().trim().length() < 4) {
            binding.etTransactionPin.setError("Transaction pin can't less than 4 characters");
            binding.etTransactionPin.requestFocus();
        } else if (selected_bank.isEmpty()) //<0
        {

            //TextView errorText = (binding.spnBank.getVie().findViewById(android.R.id.text1));
            TextView errorText = binding.spnBank.getRootView().findViewById(android.R.id.text1);
            errorText.setError("Select a Bank", customErrorDrawable);
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText("Select a Bank");//changes the selected item text to this

        } else if (TextUtils.isEmpty(acc_no)) {
            binding.etAccountNo.setError("Acccount number is required", customErrorDrawable);
            binding.etAccountNo.requestFocus();
        } else if (acc_no.length() < 10) {
            binding.etAccountNo.setError("Invalid Acccount number", customErrorDrawable);
            binding.etAccountNo.requestFocus();
        } else if (!hasSigned) {
            showOrHideSignatureAlert(true);
        } else {
            transferFunds(name, acc_no);

        }
        //return false;
    }

    public String generateUUID() {

        Long tsLong = System.currentTimeMillis();///1000;//ETZ691572523694353//ETZ691572523588
        String transaction_ref_id = "ETZ" + user.getUser_id() + tsLong.toString(); // This is your Transaction Ref id - Here we used as a timestamp -

        Log.e("TR Reference ID==>", "" + transaction_ref_id);

        return transaction_ref_id;
    }

    public void showProgressBar() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(context, null, null);
            mProgressDialog.setContentView(R.layout.dialog_progress);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#70ffffff")));
            mProgressDialog.setCancelable(false);

        /*if (mProgressDialog!=null && !mProgressDialog.isShowing()){
            mProgressDialog.show();
        }*/
        } else {
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }
    }

    public void dismissProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void transferFunds(String name, String account_no) {
        String agent_name = Objects.requireNonNull(SharedPrefManager.getUser()).getName();
        DebitUserWalletRequestBody transferPayload = new DebitUserWalletRequestBody(
                account_no,
                (int) amount,
                selected_bank_code,
                "A",
                binding.etTransactionPin.getText().toString().trim(),
                name,
                agent_name + ":" + Constants.ACCOUNT_NUMBER_SOURCE + ":" + name,
                SharedPrefManager.getUser().getNetplus_id()
        );

        showProgressBar();

        TransferModel transferModel = new TransferModel(
                account_no,
                binding.etName.getText().toString(),
                agent_name + ":" + Constants.ACCOUNT_NUMBER_SOURCE + ":" + name,
                "" + amount,
                "",
                "Payment Fund Transfer"

        );
        Timber.e(transferPayload.toString());
        compositeDisposable.add(repository.debitUserWallet(SharedPrefManager.getUser().getNetplus_id(), transferPayload)
                .subscribe(res -> {
                    String message;
                    if (res.code() == 401) {
                        transferModel.setStatus(false);
                        message = "Incorrect Pin";
                        dismissProgressBar();
                        transferModel.setMessage(message);
                        startAc(transferModel);

                        return;
                    } else {

                        JSONObject payload = new JSONObject(new Gson().toJson(res.body()));

                        if (res.body() != null && res.body().getCode().equals("0")) {
                            transferModel.setStatus(true);
                            message = payload.getString("message");
                            //utilities.showAlertDialogOk("Transaction Failure", message, ((HomeActivity) getActivity()), false);
                            dismissProgressBar();
                            transferModel.setMessage(message);
                            startAc(transferModel);
                            return;
                        }
                        transferModel.setStatus(false);
                        transferModel.setMessage(payload.getString("message"));
                        dismissProgressBar();
                        startAc(transferModel);
                    }

                }, t -> {
                    Log.e(getTag(), "An unexpected error occurred", t);
                    dismissProgressBar();
                    Toast.makeText(context, "An unexpected error occured", Toast.LENGTH_LONG).show();
                }));

    }

    private void startAc(TransferModel transferModel) {
        context.startActivity(new Intent(context, SuccessActivity.class).putExtra("data", Singletons.getGsonInstance().toJson(transferModel)));
        ((HomeActivity) context).getSupportFragmentManager().popBackStack(DashboardFragment.class.getSimpleName(), 0);

    }


    public void showOrHideSignatureAlert(boolean isError) {
        if (isError) {
            binding.tvSign.setText(context.getResources().getString(R.string.lbl_signature_required));
            binding.tvSign.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_small, 0);
            binding.tvSign.setTextColor(context.getResources().getColor(R.color.red));
        } else {
            binding.tvSign.setText(context.getResources().getString(R.string.lbl_signature));
            binding.tvSign.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            binding.tvSign.setTextColor(context.getResources().getColor(R.color.colorBlack));
        }
    }

    public String getFilePath() {

        final File folder = new File(getActivity().getExternalFilesDir(null), "/signature");
        boolean directoryExists = false;
        if (folder.exists()) {
            directoryExists = true;

        } else {
            directoryExists = folder.mkdir();
        }

        if (directoryExists) {
            String path = folder.getAbsolutePath();
            path = path + "/" + "IMG_" + System.currentTimeMillis() + ".png";// path where pdf will be stored
            return path;
        }
        return null;

    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e("Base64", encImage);
        return encImage;
    }

    /*public class Signature extends View
    {

        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void save(View v, String StoredPath)
        {

            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            if (bitmap == null)
            {

                bitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);

            }
            Canvas canvas = new Canvas(bitmap);
            try
            {
                // Output the file
                FileOutputStream mFileOutStream = new FileOutputStream(StoredPath);
                v.draw(canvas);

                // Convert the output file to Image such as .png
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mFileOutStream.flush();
                mFileOutStream.close();

            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }

        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            //binding.linearNext.setEnabled(true);
            hasSigned = true;
            this.getParent().requestDisallowInterceptTouchEvent(true);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    *//*binding.scrollView.setEnableScrolling(false);*//*
                    Log.e("Action","ACTION_DOWN");
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:
                    Log.e("Action","ACTION_MOVE");

                case MotionEvent.ACTION_POINTER_UP:
                    *//*binding.scrollView.setEnableScrolling(true);*//*
                    Log.e("Action","ACTION_POINTER_UP");
                    break;
                case MotionEvent.ACTION_UP:
                    //binding.scrollView.setEnableScrolling(true);
                    Log.e("Action","ACTION_UP");
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {

            Log.v("log_tag", string);

        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }*/

    public class Signature extends View {

        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void save(View v, String StoredPath) {

            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            if (bitmap == null) {

                bitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);

            }
            Canvas canvas = new Canvas(bitmap);
            try {
                // Output the file
                FileOutputStream mFileOutStream = new FileOutputStream(StoredPath);
                v.draw(canvas);

                // Convert the output file to Image such as .png
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mFileOutStream.flush();
                mFileOutStream.close();

            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }

        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            //binding.linearNext.setEnabled(true);
            this.getParent().requestDisallowInterceptTouchEvent(true);
            hasSigned = true;
            //binding.tvSign.setError(null);
            showOrHideSignatureAlert(false);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {

            Log.v("log_tag", string);

        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    public void showSignaturePad() {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
        //final BottomSheetDialog dialog = new BottomSheetDialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //R.anim.bottom_up_animation;
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());


//        Window window = dialog.getWindow();
//        dialog.setCanceledOnTouchOutside(true);
//        dialog.setCancelable(true);
//        window.setType(WindowManager.LayoutParams.FIRST_SUB_WINDOW);
//        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        window.setBackgroundDrawable(new ColorDrawable(
//                Color.TRANSPARENT));


        LinearLayout linearCanvas = dialog.findViewById(R.id.linearCanvas);
        Button btnRetake = dialog.findViewById(R.id.btnRetake);
        Button btnDone = dialog.findViewById(R.id.btnDone);


        btnRetake.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.2f;
        window.setAttributes(lp);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        //soldshipDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}