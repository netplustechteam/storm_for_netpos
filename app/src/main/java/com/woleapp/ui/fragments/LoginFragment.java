package com.woleapp.ui.fragments;

import static com.woleapp.util.Constants.USER_TYPE_AGENT;
import static com.woleapp.util.Constants.USER_TYPE_MERCHANT;
import static com.woleapp.util.Constants.USER_TYPE_NONE;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.woleapp.BuildConfig;
import com.woleapp.R;
import com.woleapp.Repository;
import com.woleapp.databinding.DialogResetPasswordBinding;
import com.woleapp.databinding.DialogSupportBinding;
import com.woleapp.databinding.LayoutLoginBinding;
import com.woleapp.db.Injection;
import com.woleapp.db.UserViewModel;
import com.woleapp.model.Business;
import com.woleapp.model.User;
import com.woleapp.model.kayodeStormImplementation.StormLogin;
import com.woleapp.model.kayodeStormImplementation.StormLoginResponse;
import com.woleapp.mqtt.MqttHelper;
import com.woleapp.network.StormAPIClient;
import com.woleapp.nibss.NetPosTerminalConfig;
import com.woleapp.ui.activity.HomeActivity;
import com.woleapp.util.ConnectivityReceiver;
import com.woleapp.util.JWTHelper;
import com.woleapp.util.SharedPrefManager;
import com.woleapp.util.Utilities;

import org.json.JSONObject;

import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import timber.log.Timber;

@AndroidEntryPoint
public class LoginFragment extends BaseFragment implements View.OnClickListener {

    public LoginFragment() {

    }

    @Inject
    Repository repository;

    private static final String tag = "LoginFragment";
    Context context;
    Utilities utilities;
    private LayoutLoginBinding binding;
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    int user_type = 1;
    private UserViewModel mViewModel;
    Drawable customErrorDrawable;
    ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        context = getActivity();
        utilities = new Utilities(context);
        SharedPrefManager.setXapiKey("MmHEsexfbDQIvkP1");
        mViewModel = new ViewModelProvider(this, Injection.provideViewModelFactory(getActivity()))
                .get(UserViewModel.class);
        customErrorDrawable = context.getResources().getDrawable(R.drawable.error_small);
        customErrorDrawable.setBounds(0, 0, customErrorDrawable.getIntrinsicWidth(), customErrorDrawable.getIntrinsicHeight());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        checkIfUserTokenHasNotExpired();

        binding.etPwd.setTypeface(Typeface.DEFAULT);
        binding.etPwd.setTransformationMethod(new PasswordTransformationMethod());

        binding.etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    binding.textInputUname.setHint(context.getResources().getString(R.string.hint_email));
                } else {
                    binding.textInputUname.setHint(context.getResources().getString(R.string.hint_enter_email));

                }
            }
        });

        binding.etPwd.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                //binding.textInputPwd.setPasswordVisibilityToggleEnabled(true);
                binding.textInputPwd.setHint(context.getResources().getString(R.string.hint_password));
            } else {
                //binding.textInputPwd.setPasswordVisibilityToggleEnabled(false);
                if (TextUtils.isEmpty(binding.etPwd.getText())) {
                    binding.etPwd.setError(null);
                    //binding.textInputPwd.setPasswordVisibilityToggleEnabled(false);
                    binding.etPwd.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.drawable_pwd, 0);
                    binding.textInputPwd.setHint(context.getResources().getString(R.string.hint_enter_password));

                } else {
                    //binding.textInputPwd.setPasswordVisibilityToggleEnabled(true);
                    binding.textInputPwd.setHint(context.getResources().getString(R.string.hint_password));
                }
                //binding.textInputPwd.setHint(context.getResources().getString(R.string.hint_enter_password));//

                //editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.drawableRight, 0);
                //binding.textInputPwd.setDrawa
            }
        });
        binding.btnMerchant.setOnClickListener(this);
        binding.signUpButton.setOnClickListener(this);
        binding.tvSignUp.setOnClickListener(this);
        binding.btnLogin.setOnClickListener(this);
        binding.txtForgotPwd.setOnClickListener(this);
        getFCMToken();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        //user = SharedPrefManager.getUser();
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_login, container, false);
        //((GPHMainActivity)getActivity()).createBackButton(job_title);
        return binding.getRoot();
    }

    public void showProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            return;
        mProgressDialog = ProgressDialog.show(context, null, null);
        mProgressDialog.setContentView(R.layout.dialog_progress);
        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mProgressDialog.setCancelable(false);

    }

    public void dismissProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.clear();

    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            /*case R.id.action_edit:
                mDataBinding.btnUpdate.setVisibility(View.VISIBLE);
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean isValidEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {


        if (v == binding.txtForgotPwd) {
            showDialogReset();
        } else if (v == binding.signUpButton) {
            if (user_type != USER_TYPE_AGENT) {
                //binding.txtForgotPwd.setVisibility(View.VISIBLE);
                user_type = USER_TYPE_AGENT;
                binding.signUpButton.setCardElevation(3f);
                binding.signUpButton.setElevation(3f);
                binding.signUpButton.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

                binding.btnMerchant.setCardElevation(8f);
                binding.btnMerchant.setElevation(8f);
                binding.btnMerchant.setCardBackgroundColor(context.getResources().getColor(R.color.text_color_gray_dark));
            }


//            binding.btnAgent.setBackgroundResource(R.drawable.ripple_theme2);
//            binding.btnMerchant.setBackgroundResource(R.drawable.ripple_gray2);
        } else if (v == binding.btnMerchant) {
            if (user_type != USER_TYPE_MERCHANT) {
                //binding.txtForgotPwd.setVisibility(View.GONE);
                user_type = USER_TYPE_MERCHANT;
                binding.btnMerchant.setCardElevation(3f);
                binding.btnMerchant.setElevation(3f);
                binding.btnMerchant.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

                binding.signUpButton.setCardElevation(8f);
                binding.signUpButton.setElevation(8f);
                binding.signUpButton.setCardBackgroundColor(context.getResources().getColor(R.color.text_color_gray_dark));
            }

//            binding.btnAgent.setBackgroundResource(R.drawable.ripple_gray2);
//            binding.btnMerchant.setBackgroundResource(R.drawable.ripple_theme2);
        } else if (v == binding.tvSignUp) {
            showFragment(new SignupFragment(), SignupFragment.class.getSimpleName());
        } else if (v == binding.btnLogin) {
            validateInputsAndProceed();
        }
    }

    private void checkIfUserTokenHasNotExpired() {
        String token = SharedPrefManager.getAppTokenForNewStormService();
        if (token != null) {
            if (!token.isEmpty()) {
                if (!JWTHelper.isExpired(token)) {
                    MqttHelper.init(requireActivity().getApplicationContext());
                    NetPosTerminalConfig.Companion.init(requireActivity().getApplicationContext(), false);
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        }
    }

    public void getFCMToken() {
        FirebaseMessaging.getInstance().getToken().addOnSuccessListener(SharedPrefManager::saveDeviceToken);
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String notificationID = instanceIdResult.getToken();
//                Log.e("newToken", notificationID + "--");
//                SharedPrefManager.saveDeviceToken(notificationID);
//                /*String pushToken = SharedPrefManager.getInstance(getApplicationContext()).getDeviceToken();*/
//
//            }
//        });

        /*String id  = FirebaseInstanceId.getInstance().getId();
        Log.e("FB_Id",id+"--");*/
    }

    public void validateInputsAndProceed() {
        String appToken = SharedPrefManager.getAppToken();
//        if (appToken == null || JWTHelper.isExpired(appToken)) {
//            ((MainActivity) requireActivity()).checkToken(true);
//            showProgressBar();
//        }
        String email = binding.etEmail.getText().toString().trim();
        String password = binding.etPwd.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            binding.etEmail.setError("Email is required", customErrorDrawable);
            binding.etEmail.requestFocus();
        } else if (!isValidEmail(email)) {
            binding.etEmail.setError("Invalid Email Address", customErrorDrawable);
            binding.etEmail.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            binding.etPwd.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            binding.etPwd.setError("Password is required", customErrorDrawable);
            binding.etPwd.requestFocus();
        } else {
            if (ConnectivityReceiver.isConnected()) {
                String appType;
                if (BuildConfig.FLAVOR.contains("agency")) {
                    appType = "agent_1";
                } else {
                    appType = "merchant";
                }
                StormLogin loginCredentials = new StormLogin(email, password, appType);
                login(loginCredentials);
            } else {
                utilities.showDialogNoNetwork("You need an active internet connection to proceed. Would you like to enable it ?", getActivity());
            }
        }
    }

    private void gotoHomeScreen(User user) {
        if (user.getUser_type() == USER_TYPE_MERCHANT && user.getQRRegistered()) {
            mViewModel.getBusinessInfo(user.getUser_id()).observe(LoginFragment.this, new Observer<Business>() {
                @Override
                public void onChanged(Business business) {
                    user.setBusiness_info(business);
                    SharedPrefManager.setUser(user);
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                }
            });
        } else {
            SharedPrefManager.setUser(user);
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            getActivity().finish();
        }

    }

    public void showDialogReset() {
        DialogResetPasswordBinding binding;
        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*dialog2.setContentView(R.layout.dialog_record_payment);
        binding = DataBindingUtil.setContentView( R.layout.dialog_record_payment);*/
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_reset_password, null, false);
        dialog2.setContentView(binding.getRoot());
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        binding.btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = binding.etEmail.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(context, "Email is required", Toast.LENGTH_SHORT).show();
                } else if (!isValidEmail(email)) {
                    Toast.makeText(context, "Invalid Email Address", Toast.LENGTH_SHORT).show();
                } else {
                    dialog2.dismiss();
//                    resetPassword(email);
                }

            }
        });

        dialog2.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        dialog2.setCanceledOnTouchOutside(false);
        dialog2.setCancelable(true);
        dialog2.getWindow().setType(WindowManager.LayoutParams.FIRST_SUB_WINDOW);
        dialog2.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog2.show();
    }

    public void showDialogMailSent(String message) {
        DialogSupportBinding binding;
        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_support, null, false);
        dialog2.setContentView(binding.getRoot());
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        binding.tvMessage.setText(message);
        binding.btnDone.setVisibility(View.VISIBLE);
        binding.btnDone.setText("Dismiss");
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        binding.btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        dialog2.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        dialog2.setCanceledOnTouchOutside(false);
        dialog2.setCancelable(true);
        dialog2.getWindow().setType(WindowManager.LayoutParams.FIRST_SUB_WINDOW);
        dialog2.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog2.show();
    }

    public void resetPassword(String email) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", email);

        String appToken = SharedPrefManager.getAppToken();
        showProgressBar();
        Disposable subscribe = StormAPIClient
                .passwordReset(jsonObject, context)
                .subscribe(res -> {
                    if (res.code() != 200) {
                        Toast.makeText(context, "Reset failed, please check email and try again.", Toast.LENGTH_SHORT).show();
                        dismissProgressBar();
                        return;
                    }

                    JSONObject payload = new JSONObject(new Gson().toJson(res.body()));

                    if (!payload.getBoolean("success")) {
                        Toast.makeText(context, "Reset failed, please check email and try again.", Toast.LENGTH_SHORT).show();
                        dismissProgressBar();
                        return;
                    }

                    dismissProgressBar();
                    showDialogMailSent("Check your registered email address for your new password");

                }, t -> {
                    dismissProgressBar();
                    Log.e(getTag(), "An unexpected error occurred", t);
                    Toast.makeText(context, "An unexpected error occured", Toast.LENGTH_LONG).show();
                });

    }

    public void login(StormLogin jsonObject) {
        Log.e(tag, jsonObject.toString());
        showProgressBar();
        Single.fromObservable(
                Objects.requireNonNull(repository
                        .loginUser(jsonObject)))
                .doFinally(this::dismissProgressBar)
                .subscribe(new SingleObserver<Response<StormLoginResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Response<StormLoginResponse> res) {
                        StormLoginResponse body = res.body();
                        Log.e(tag, "on success called " + body);
                        try {

                            JSONObject payload = new JSONObject(new Gson().toJson(body));
//
                            User user = new User();
                            if (body != null) {
                                user.setAccount_number(body.getUser().getAccount_number());
//                            user.setBank(payload.optString("bankName", ""));
                                user.setUser_type_for_new_storm_service(body.getUser().getType());
                                user.setIs_verified(true);
                                user.setBusiness_name(body.getUser().getBusiness_name());
                                user.setEmail(body.getUser().getEmail());
                                user.setName(body.getUser().getBusiness_name());
                                user.setNetplus_id(body.getUser().getStorm_id());
                                if (body.getUser().getTerminal_id() != null) {
                                    user.setTerminal_id(body.getUser().getTerminal_id().toString());
                                } else {
                                    user.setTerminal_id("");
                                }
                                user.setPhone(body.getUser().getMobile_number());
//                                user.setUser_type(body.getUser().getType());
                                SharedPrefManager.setAppTokenForNewStormService(body.getToken());
                                SharedPrefManager.setUserToken(body.getToken());
                                String token = SharedPrefManager.getUserToken();

                                Timber.d("STRING_USER_TOKEN====>%s", token);
//
                                if (user_type == USER_TYPE_AGENT && JWTHelper.isAgent(token)) {
                                    user.setUser_type(USER_TYPE_AGENT);
                                }
//
                                if (user_type == USER_TYPE_MERCHANT && JWTHelper.isMerchant(token)) {
                                    user.setUser_type(USER_TYPE_MERCHANT);
                                }
                                if (SharedPrefManager.getLastLoggedInUser() != null && SharedPrefManager.getLastLoggedInUser().equals(user.getNetplus_id())) {
                                    user.setUser_type(USER_TYPE_NONE);
                                } else if (SharedPrefManager.getLastLoggedInUser() != null && SharedPrefManager.getLastLoggedInUser().equals(user.getNetplus_id())) {
                                    user.setUser_type(SharedPrefManager.getUserType());
                                } else
                                    user.setUser_type(USER_TYPE_NONE);

                                SharedPrefManager.setUser(user);
                                SharedPrefManager.setLoginStatus(true);
//                            Log.d(tag, "user type " + user.getUser_type());
                                dismissProgressBar();
                                SharedPrefManager.setLoginStatus(true);
                            }

                            MqttHelper.init(requireActivity().getApplicationContext());
                            NetPosTerminalConfig.Companion.init(requireActivity().getApplicationContext(), false);
                            Intent intent = new Intent(getActivity(), HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("TAG", "" + e.getCause());
                            Log.e(tag, "SHOW_ME" + e.getLocalizedMessage());
                            //Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast.makeText(context, getString(R.string.error_login), Toast.LENGTH_LONG).show();
                            SharedPrefManager.setUserToken(null);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissProgressBar();
                        Log.e(tag, "login error occurred", e);
                        SharedPrefManager.setUserToken(null);
                        if (e.getMessage().equals("failed")) {
                            Toast.makeText(context, "Authentication failed, Please check your login credentials and try again.", Toast.LENGTH_LONG).show();
                            return;
                        } else if (e.getMessage().equals("Error")) {
                            Toast.makeText(context, "An unexpected error occurred, please try again later.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        Toast.makeText(context, "Please try again and ensure you're connected", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
