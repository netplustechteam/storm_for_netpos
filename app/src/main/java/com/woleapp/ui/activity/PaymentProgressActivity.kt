@file:Suppress("DEPRECATION")

package com.woleapp.ui.activity

import android.app.ProgressDialog
import android.content.*
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.danbamitale.epmslib.entities.TransactionType
import com.google.android.material.snackbar.Snackbar
import com.woleapp.R
import com.woleapp.databinding.ActivityPaymentProgressBinding
import com.woleapp.databinding.DialogPrintTypeBinding
import com.woleapp.databinding.DialogTransactionResultBinding
import com.woleapp.kozenext.showCardDialog
import com.woleapp.model.MerchantTransaction
import com.woleapp.model.User
import com.woleapp.network.MerchantsApiClient.getMerchantApiService
import com.woleapp.network.StormAPIClient
import com.woleapp.nibss.CONFIGURATION_ACTION
import com.woleapp.nibss.CONFIGURATION_STATUS
import com.woleapp.nibss.NetPosTerminalConfig
import com.woleapp.util.Constants.*
import com.woleapp.util.SharedPrefManager
import com.woleapp.util.Utilities
import com.woleapp.viewmodels.*
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class PaymentProgressActivity @Inject constructor() : AppCompatActivity() {
    var progressDialog: ProgressDialog? = null
    private lateinit var alertDialog: AlertDialog
    private var user: User? = null
    var amount = 0.0
    var commodityName: String? = null
    var description: String? = null
    private lateinit var binding: ActivityPaymentProgressBinding
    var utilities: Utilities? = null
    var transactionType = 0
    var context: Context = this
    var reference: String? = null
    var productId = 0
    var quantity = 0
    var sellerId: String? = null
    private lateinit var viewModel: SalesViewModel
    private lateinit var receiptDialogBinding: DialogTransactionResultBinding
    private lateinit var receiptDialog: AlertDialog
    private lateinit var dialogPrintTypeBinding: DialogPrintTypeBinding
    private lateinit var printTypeDialog: AlertDialog
    private lateinit var paymentViewModel: PaymentViewModel
    private var transactionDone = false
    private var logSalesOrderDone = true
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            // Toast.makeText(context, "received", Toast.LENGTH_LONG).show()
            intent?.let {
                when (it.getIntExtra(CONFIGURATION_STATUS, -1)) {
                    1 -> {
                        dismissProgressDialogIfShowing()
                        makePayment()
                    }
                    0 -> showProgressDialog("Please wait")
                    -1 -> {
                        dismissProgressDialogIfShowing()
                        showAlertDialog(
                            if (NetPosTerminalConfig.getTerminalId()
                                .isNullOrEmpty()
                            ) "No terminal ID associated with the storm account logged in on this terminal" else "An unexpected error occurred."
                        )
                    }
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(receiver, IntentFilter(CONFIGURATION_ACTION))
        if (NetPosTerminalConfig.isConfigurationInProcess)
            showProgressDialog("Please wait")
        scanTerminal()
    }

    private fun scanTerminal() {
        if (NetPosTerminalConfig.configurationStatus != 1) {
            NetPosTerminalConfig.init(context.applicationContext)
        } else
            makePayment()
    }

    private fun makePayment() {
        dismissProgressDialogIfShowing()
        showCardDialog(this, amount.toLong(), 0)
            .observe(this) { event ->
                event.getContentIfNotHandled()?.let {
                    it.error?.let { error ->
                        Timber.e("Error: ${error.localizedMessage}")
                        Toast.makeText(context, error.localizedMessage, Toast.LENGTH_SHORT)
                            .show()
                        setResult(
                            RESULT_CANCELED,
                            Intent().apply {
                            }
                        )
                        finish()
                    }
                    it.cardData?.let { cardData ->
                        viewModel.setCardScheme(it.cardScheme!!)
                        viewModel.setCustomerName(it.customerName ?: "Customer")
                        viewModel.setAccountType(it.accountType!!)
                        viewModel.cardData = cardData
                        viewModel.validateField()
                        // showProgressDialog("Please wait")
                        viewModel.makePayment(context, TransactionType.PURCHASE)
                    }
                }
            }
    }

    private fun dismissProgressDialogIfShowing() {
        progressDialog?.dismiss()
    }

    private fun showProgressDialog(message: String? = null) {
        message?.let {
            progressDialog?.setMessage(it)
        }
        progressDialog?.show()
    }

    private fun showAlertDialog(message: String) {
        alertDialog.apply {
            setMessage(message)
            show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(SalesViewModel::class.java)
//        viewModel.setTransactionsDao(
//            AppDatabase.getInstance(applicationContext).agentTransactionDao()
//        )

        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_progress)
        receiptDialogBinding =
            DialogTransactionResultBinding.inflate(layoutInflater, null, false)
                .apply { executePendingBindings() }
        dialogPrintTypeBinding = DialogPrintTypeBinding.inflate(layoutInflater, null, false).apply {
            executePendingBindings()
        }
        paymentViewModel = ViewModelProvider(this).get(PaymentViewModel::class.java)
        paymentViewModel.setStormMerchantApiService(getMerchantApiService(this))
        paymentViewModel.setStormAPIService(StormAPIClient.create(this))
        user = SharedPrefManager.getUser()
        utilities = Utilities(this)
        binding.receiptBtn.visibility = View.INVISIBLE
        binding.closeBtn.visibility = View.INVISIBLE
        amount = intent.extras!!.getDouble(KEY_AMOUNT)
        transactionType = intent.extras!!.getInt("transaction_type", 0)
        commodityName = intent.extras!!.getString("COMMODITY_NAME")
        description = intent.extras!!.getString("DESCRIPTION")
        productId = intent.extras!!.getInt("PRODUCTID")
        quantity = intent.extras!!.getInt("QUANTITY")
        reference = intent.extras!!.getString("TRANSACTION_REF")
        sellerId = intent.extras!!.getString("SELLERID")
        viewModel.amount.value = amount.toString()
        viewModel.refreshNibssKeys.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                NetPosTerminalConfig.init(applicationContext, true)
            }
        }
        viewModel.setDescription(description)
        Timber.e(description)
        val convenienceFee =
            java.lang.Double.toString(intent.extras!!.getDouble("convenience_fee")).toFloat()
        binding.amountTv.text = "Amount: " + utilities!!.getFormattedAmount(amount)
//        binding.tvConvenienceFee.visibility = View.VISIBLE
//        binding.tvConvenienceFee.text =
//            "Convenience Fee Included: " + utilities!!.getFormattedAmount(convenienceFee.toDouble())
        progressDialog = ProgressDialog(this)
            .apply {
                setCancelable(false)
                setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel") { d, _ ->
                    d.dismiss()
                    setResult(RESULT_CANCELED, Intent())
                    finish()
                }
            }
        alertDialog = AlertDialog.Builder(this).run {
            setCancelable(false)
            title = "Message"
            setPositiveButton("Retry") { dialog, _ ->
                NetPosTerminalConfig.init(applicationContext)
                dialog.dismiss()
            }
            setNegativeButton("Cancel") { dialog, _ ->
                Toast.makeText(applicationContext, "cancelled", Toast.LENGTH_LONG)
                    .show()
                dialog.dismiss()
            }
            create()
        }
        viewModel.transactionState.observe(this) { event ->
            event.getContentIfNotHandled()?.let { transactionState ->
                when (transactionState) {
                    STATE_PAYMENT_STAND_BY -> {
                        dismissProgressDialogIfShowing()
                    }
                    STATE_PAYMENT_STARTED -> {
                        showProgressDialog("Processing Transaction")
                    }
                    STATE_PAYMENT_DONE_LOGGING_TO_BACKEND -> {
                        showProgressDialog("Logging Transaction")
                    }
                    STATE_PAYMENT_APPROVED -> {
                        dismissProgressDialogIfShowing()
                    }
                    STATE_PAYMENT_APPROVED_BUT_NOT_LOGGED -> {
                        dismissProgressDialogIfShowing()
                        showSnackBar("Transaction Approved but not logged to database")
                    }
                }
            }
        }

        viewModel.message.observe(this) {
            it.getContentIfNotHandled()?.let { s ->
                showSnackBar(s)
            }
        }
        viewModel.toastMessage.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        }
        receiptDialog = AlertDialog.Builder(this).setCancelable(false).apply {
            setView(receiptDialogBinding.root)
            receiptDialogBinding.apply {
                closeBtn.setOnClickListener {
                    receiptDialog.dismiss()
                }
                sendButton.setOnClickListener {
                    if (receiptDialogBinding.telephone.text.toString().length != 11) {
                        Toast.makeText(
                            this@PaymentProgressActivity,
                            "Please enter a valid phone number",
                            Toast.LENGTH_LONG
                        ).show()
                        return@setOnClickListener
                    }
                    viewModel.sendSmS(
                        receiptDialogBinding.telephone.text.toString()
                    )
                    progress.visibility = View.VISIBLE
                    sendButton.isEnabled = false
                }
            }
        }.create()
        printTypeDialog = AlertDialog.Builder(this).setCancelable(false)
            .apply {
                setView(dialogPrintTypeBinding.root)
                dialogPrintTypeBinding.apply {
                    cancel.setOnClickListener {
                        printTypeDialog.dismiss()
                        viewModel.finishTransaction()
                    }
                    customer.setOnClickListener {
                        viewModel.printReceipt(
                            this@PaymentProgressActivity,
                            isMerchantCopy = false,
                            selected = true
                        )
                    }
                    merchant.setOnClickListener {
                        viewModel.printReceipt(
                            this@PaymentProgressActivity,
                            isMerchantCopy = true,
                            selected = true
                        )
                    }
                }
            }.create()
        viewModel.showReceiptTypeDialog.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                if (it)
                    printTypeDialog.show()
            }
        }
        viewModel.smsSent.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                receiptDialogBinding.progress.visibility = View.GONE
                receiptDialogBinding.sendButton.isEnabled = true
                if (it) {
                    Toast.makeText(this, "Sent Receipt", Toast.LENGTH_LONG).show()
                    alertDialog.dismiss()
                    viewModel.finishTransaction()
                }
            }
        }
        viewModel.showPrinterError.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                AlertDialog.Builder(this)
                    .apply {
                        setTitle("Printer Error")
                        setIcon(R.drawable.ic_warning)
                        setMessage(it)
                        setPositiveButton("Send Receipt") { d, _ ->
                            d.dismiss()
                            viewModel.showReceiptDialog()
                        }
                        setNegativeButton("Dismiss") { d, _ ->
                            d.dismiss()
                        }
                    }.show()
            }
        }

        receiptDialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        viewModel.showPrintDialog.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                receiptDialog.apply {
                    receiptDialogBinding.transactionContent.text = it
                    show()
                }
                receiptDialogBinding.apply {
                    progress.visibility = View.GONE
                    sendButton.isEnabled = true
                }
            }
        }
        viewModel.transactionDone.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                transactionDone = it
                if (transactionDone && logSalesOrderDone) {
                    setResult(RESULT_OK, Intent())
                    finish()
                }
            }
        }
        paymentViewModel.loadingStatus.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                logSalesOrderDone = it
                if (logSalesOrderDone && transactionDone) {
                    setResult(RESULT_OK, Intent())
                    finish()
                }
            }
        }
        viewModel.logSalesOrder.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                val merchantTransaction = MerchantTransaction(
                    reference!!,
                    user!!.netplus_id,
                    PAYMENT_MODE_POS,
                    "",
                    if (it.responseCode == "00") PAYMENT_STATUS_SUCCESS else PAYMENT_STATUS_FAILED,
                    "" + amount,
                    "" + productId,
                    "" + quantity,
                    sellerId
                )
                paymentViewModel.logSalesOrder(
                    merchantTransaction,
                    quantity.toString(),
                    productId.toString()
                )
            }
        }

        // fakePayment();
    }

    private fun fakePayment() {
        val intent = Intent(context, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("completed", true)
        intent.putExtra("productId", productId)
        intent.putExtra("quantity", quantity)
        intent.putExtra("reference", reference)
        intent.putExtra("amount", amount)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun printReceipt(transactionId: UUID) {}
    fun showDialog(message: String?) {
        AlertDialog.Builder(context)
            .setTitle("Alert")
            .setMessage(message)
            .setPositiveButton("Ok") { dialog, which -> dialog.cancel() }
            .show()
    }

    // print receipt start here
    // start the transaction when the bluetooth device is selected
    // close the printer bluetooth list if already one is present
    private fun closeDialogFragment() {
        val dialogBluetoothList = supportFragmentManager.findFragmentByTag(BLUETOOTH_FRAGMENT_TAG)
        if (dialogBluetoothList != null) {
            val dialogFragment = dialogBluetoothList as DialogFragment?
            dialogFragment?.dismiss()
        }
    }

    companion object {
        const val KEY_AMOUNT = "amount"
        private const val BLUETOOTH_FRAGMENT_TAG = "bluetooth"
    }

    private fun showSnackBar(message: String) {
        dismissProgressDialogIfShowing()
        if (message == "Transaction not approved") {
            AlertDialog.Builder(this)
                .apply {
                    setTitle("Response")
                    setMessage(message)
                    show()
                }
        }
        Snackbar.make(
            findViewById(
                R.id.payment_progress_container
            ),
            message, Snackbar.LENGTH_LONG
        ).show()
    }
}
