package com.woleapp.ui.activity

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.woleapp.R
import com.woleapp.databinding.DialogTransactionResultBinding
import com.woleapp.model.TransferModel
import com.woleapp.util.Singletons
import com.woleapp.viewmodels.TransferViewModel
import timber.log.Timber

class SuccessActivity : AppCompatActivity() {
    var context: Context = this
    lateinit var imgStatus: ImageView
    lateinit var tvStatus: TextView
    lateinit var toolbar: Toolbar
    var transactionStatus: Boolean? = null
    var displayText: String? = null
    private lateinit var transferModel: TransferModel
    private lateinit var transferViewModel: TransferViewModel
    private lateinit var receiptDialogBinding: DialogTransactionResultBinding
    private lateinit var receiptDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)
        receiptDialogBinding =
            DialogTransactionResultBinding.inflate(layoutInflater, null, false)
                .apply { executePendingBindings() }
        transferViewModel = ViewModelProvider(this).get(TransferViewModel::class.java)
        transferViewModel.showPrinterError.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                AlertDialog.Builder(this)
                    .apply {
                        setTitle("Printer Error")
                        setIcon(R.drawable.ic_warning)
                        setMessage(it)
                        setPositiveButton("Send Receipt") { d, _ ->
                            d.dismiss()
                            transferViewModel.showReceiptDialog()
                        }
                        setNegativeButton("Dismiss") { d, _ ->
                            d.dismiss()
                        }
                    }.show()
            }
        }

        transferViewModel.toastMessage.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        }
        receiptDialog = AlertDialog.Builder(this).setCancelable(false).apply {
            setView(receiptDialogBinding.root)
            receiptDialogBinding.apply {
                closeBtn.setOnClickListener {
                    receiptDialog.dismiss()
                }
                sendButton.setOnClickListener {
                    if (receiptDialogBinding.telephone.text.toString().length != 11) {
                        Toast.makeText(
                            this@SuccessActivity,
                            "Please enter a valid phone number",
                            Toast.LENGTH_LONG
                        ).show()
                        return@setOnClickListener
                    }
                    transferViewModel.sendSmS(
                        receiptDialogBinding.telephone.text.toString()
                    )
                    progress.visibility = View.VISIBLE
                    sendButton.isEnabled = false
                }
            }
        }.create()

        transferViewModel.smsSent.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                receiptDialogBinding.progress.visibility = View.GONE
                receiptDialogBinding.sendButton.isEnabled = true
                if (it) {
                    Toast.makeText(this, "Sent Receipt", Toast.LENGTH_LONG).show()
                    receiptDialog.dismiss()
                    // transferViewModel.finishTransaction()
                }
            }
        }

        receiptDialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        transferViewModel.showPrintDialog.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                receiptDialog.apply {
                    receiptDialogBinding.transactionContent.text = it
                    show()
                }
                receiptDialogBinding.apply {
                    progress.visibility = View.GONE
                    sendButton.isEnabled = true
                }
            }
        }

        if (intent != null) {
            Timber.e(intent.getStringExtra("data"))
            transferModel = Singletons.getGsonInstance()
                .fromJson(intent.getStringExtra("data"), TransferModel::class.java)
            Timber.e(transferModel.toString())
            transactionStatus = transferModel.status
            displayText = transferModel.message
            inIt()
            transferViewModel.printTransfer(this, transferModel)
        }
    }

    private fun inIt() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        tvStatus = findViewById(R.id.tvTransactionStatus)
        imgStatus = findViewById(R.id.imgTransactionStatus)
        tvStatus.text = displayText
        imgStatus.setImageResource(
            if (transactionStatus!!) R.drawable.tick else R.drawable.ic_cancel_black_24dp
        )
//        imgStatus.setImageDrawable(
//            if (transactionStatus!!) resources.getDrawable(R.drawable.tick) else resources.getDrawable(
//                R.drawable.ic_cancel_black_24dp
//            )
//        )
    }
}
