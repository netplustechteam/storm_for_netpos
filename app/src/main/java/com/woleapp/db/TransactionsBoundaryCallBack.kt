package com.woleapp.db

import android.util.Log
import androidx.paging.PagedList
import com.pixplicity.easyprefs.library.Prefs
import com.woleapp.BuildConfig
import com.woleapp.Repository
import com.woleapp.db.dao.TransactionsDao
import com.woleapp.model.Transactions
import com.woleapp.model.kayodeStormImplementation.GetTransactionsResponse
import com.woleapp.util.Constants.* // ktlint-disable no-wildcard-imports
import com.woleapp.util.SharedPrefManager
import com.woleapp.util.TransactionsUtils.mapToGetTransactionResp
import com.woleapp.util.disposeWith
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

const val PAGE_SIZE = 20
const val PAGE_NUMBER = 0

enum class LoadState {
    LOADING_INITIAL,
    LOADING_MORE,
    LOADING_FINISHED,
    LOADING_ERROR
}

typealias loadStateListener = (LoadState) -> Unit

class TransactionsBoundaryCallBack @AssistedInject constructor(
    private val repository: Repository,
    private val transactionsDao: TransactionsDao,
    @Assisted private val loadStateListener: loadStateListener,
) : PagedList.BoundaryCallback<Transactions>() {

    private var isLoading = false
    private var hasLoadedAllItems = false
    private var compositeDisposable = CompositeDisposable()

    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        fetchTransactions(LoadState.LOADING_INITIAL)
    }

    override fun onItemAtEndLoaded(itemAtEnd: Transactions) {
        super.onItemAtEndLoaded(itemAtEnd)
        fetchTransactions(LoadState.LOADING_MORE)
    }

    private fun insertOneTransaction(transaction: Transactions) {
        insertTransactionsToDatabase(listOf(transaction))
    }

    private fun fetchTransactions(loadingState: LoadState) {
        if (isLoading || hasLoadedAllItems)
            return
        val fetchStartDate = Prefs.getString(FETCH_TRANSACTIONS_FROM_BACKEND_START_DATE, "").trim()
        val fetchEndDate = Prefs.getString(FETCH_TRANSACTIONS_FROM_BACKEND_END_DATE, "").trim()
        val fetchRrn = Prefs.getString(FETCH_TRANSACTIONS_FROM_BACKEND_RRN, "").trim()
        val stormId = SharedPrefManager.getUser().netplus_id.trim()
        Log.d("FETCH_MODE", Prefs.getString(MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, ""))
        Log.d("STARTDATE", fetchStartDate)
        Log.d("END_DATE", fetchEndDate)
        Log.d("FETCH_RRN", fetchRrn)
        val fetchSource: Single<GetTransactionsResponse> =
            when (Prefs.getString(MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, "")) {
                FETCH_VIA_DATE -> repository.getTransactionFromBackendByDate(
                    stormId,
                    if (BuildConfig.FLAVOR.contains("agency", true))
                        SharedPrefManager.getNextAgentTransactionsPageForByDate()
                    else SharedPrefManager.getNextMerchantTransactionsPageForByDate(),
                    fetchStartDate,
                    fetchEndDate
                ).flatMap {
                    Single.just(it.mapToGetTransactionResp())
                }
                FETCH_VIA_RRN -> repository.getTransactionFromBackendByRrn(
                    stormId,
                    if (BuildConfig.FLAVOR.contains("agency", true))
                        SharedPrefManager.getNextAgentTransactionsPageForByRrn()
                    else SharedPrefManager.getNextMerchantTransactionsPageForByRrn(),
                    fetchRrn
                ).flatMap {
                    Single.just(it.mapToGetTransactionResp())
                }
                else -> repository.getTransactionFromBackend(
                    SharedPrefManager.getUser().netplus_id,
                    if (BuildConfig.FLAVOR.contains("agency", true)) {
                        Log.d("CALLED_NOW", "YESS")
                        SharedPrefManager.getNextAgentTransactionsPage()
                    } else SharedPrefManager.getNextMerchantTransactionsPage()
                )
            }

        fetchSource.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { isLoading = false }
            .subscribe(object : SingleObserver<GetTransactionsResponse> {
                override fun onSuccess(t: GetTransactionsResponse) {
                    Timber.d("T=>$t")
                    if (t.transaction.isEmpty()) {
                        hasLoadedAllItems = true
                        loadStateListener.invoke(LoadState.LOADING_FINISHED)
                        return
                    }
                    if (BuildConfig.FLAVOR.contains("agency")) {
                        when (Prefs.getString(MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, "")) {
                            FETCH_VIA_DATE -> {
                                SharedPrefManager.setNextAgentTransactionsPageForByDate(
                                    SharedPrefManager.getNextAgentTransactionsPageForByDate() + 1
                                )
                            }
                            FETCH_VIA_RRN -> {
                                SharedPrefManager.setNextAgentTransactionsPageForByRrn(
                                    SharedPrefManager.getNextAgentTransactionsPageForByRrn() + 1
                                )
                            }
                            else -> {
                                SharedPrefManager.setNextAgentTransactionsPage(SharedPrefManager.getNextAgentTransactionsPage() + 1)
                            }
                        }
                    } else {
                        when (Prefs.getString(MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, "")) {
                            FETCH_VIA_DATE -> {
                                SharedPrefManager.setNextMerchantTransactionsPageForByDate(
                                    SharedPrefManager.getNextMerchantTransactionsPageForByDate() + 1
                                )
                            }
                            FETCH_VIA_RRN -> {
                                SharedPrefManager.setNextMerchantTransactionsPageForByRrn(
                                    SharedPrefManager.getNextMerchantTransactionsPageForByRrn() + 1
                                )
                            }
                            else -> {
                                SharedPrefManager.setNextMerchantTransactionsPage(SharedPrefManager.getNextMerchantTransactionsPageForByDate() + 1)
                            }
                        }
                    }
                    if (!(
                        Prefs.getString(
                                MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND,
                                ""
                            ) == FETCH_VIA_RRN || Prefs.getString(
                                MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND, ""
                            ) == FETCH_VIA_DATE
                        )
                    ) {
                        Log.d("CALLED_A", "YES")
                        insertTransactionsToDatabase(t.transaction.map { it.toAppTransactionStructure() })
                    }
                }

                override fun onSubscribe(d: Disposable) {
                    isLoading = true
                    loadStateListener.invoke(loadingState)
                    d.disposeWith(compositeDisposable)
                }

                override fun onError(e: Throwable) {
                    Timber.d("THRW$e")
                    loadStateListener.invoke(LoadState.LOADING_ERROR)
                }
            })
    }

    private fun insertTransactionsToDatabase(transactions: List<Transactions>) {
        Timber.d("GOTHERE=>$transactions")
        transactionsDao.insertTransactions(transactions)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { _, _ ->
                loadStateListener.invoke(LoadState.LOADING_FINISHED)
            }.disposeWith(compositeDisposable)
    }

    fun loadItemsAfterRefresh() {
        isLoading = false
        hasLoadedAllItems = false
        fetchTransactions(LoadState.LOADING_MORE)
    }
}
