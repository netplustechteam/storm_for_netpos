package com.woleapp.db.dao

import androidx.paging.DataSource
import androidx.room.* // ktlint-disable no-wildcard-imports
import com.woleapp.model.TrackTransactionTable
import com.woleapp.model.Transactions
import io.reactivex.Single

@Dao
interface TransactionsDao {
    @Query("SELECT * FROM transactions")
    fun getTransactions(): DataSource.Factory<Int, Transactions>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTransactions(transactions: List<Transactions>): Single<List<Long>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSingleTransaction(transactions: Transactions): Single<Long>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertSingleTransactionForLaterTracking(transactions: TrackTransactionTable): Single<Long>

    @Query("SELECT * FROM transRespForTracking WHERE push_to_backendStatus = 0 ORDER BY transactionTimeInMillis ASC")
    fun getAllTrackingTransactions(): List<TrackTransactionTable>

    @Delete
    fun deleteSingleConfirmedTransactions(transToDelete: TrackTransactionTable)

    @Query("DELETE FROM transRespForTracking WHERE push_to_backendStatus = 1")
    fun deleteAllConfirmedTransactions(): Single<Int>

    @Query("DELETE FROM transactions")
    fun deleteOldTransactions(): Single<Int>
}
