package com.woleapp.db.typeConverters

import androidx.room.TypeConverter
import com.danbamitale.epmslib.utils.IsoAccountType

class AccountTypeTypeConverter {

    @TypeConverter
    fun fromAccountType(accountType: IsoAccountType): String =
        accountType.name

    @TypeConverter
    fun toAccountType(accountTypeName: String): IsoAccountType =
        IsoAccountType.valueOf(accountTypeName)
}
