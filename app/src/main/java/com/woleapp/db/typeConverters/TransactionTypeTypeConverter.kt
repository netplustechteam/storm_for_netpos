package com.woleapp.db.typeConverters

import androidx.room.TypeConverter
import com.danbamitale.epmslib.entities.TransactionType

class TransactionTypeTypeConverter {
    @TypeConverter
    fun fromTransactionType(transactionType: TransactionType): String =
        transactionType.name

    @TypeConverter
    fun toTransactionType(transactionTypeName: String): TransactionType =
        TransactionType.valueOf(transactionTypeName)
}
