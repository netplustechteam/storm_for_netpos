package com.woleapp.db.typeConverters

import androidx.room.TypeConverter
import com.danbamitale.epmslib.entities.PosMode

class PosModeTypeConverter {
    @TypeConverter
    fun fromPosMode(posMode: PosMode): String =
        posMode.name

    @TypeConverter
    fun toPosMode(posModeName: String): PosMode =
        PosMode.valueOf(posModeName)
}
