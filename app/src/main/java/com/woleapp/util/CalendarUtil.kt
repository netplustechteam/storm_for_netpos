package com.woleapp.util

import android.app.DatePickerDialog
import android.content.Context
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.* // ktlint-disable no-wildcard-imports

object CalendarUtil {

    private fun getBeginningOfDay(timeStamp: Long? = null): Long =
        (
            if (timeStamp == null) ZonedDateTime.now() else ZonedDateTime.of(
                Date(timeStamp).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
                ZoneId.systemDefault()
            )
            ).with(LocalTime.MIN).toEpochSecond() * 1000

    fun showCalendarDialog(context: Context, action: (lowerTime: String, upperTime: String) -> Unit) {
        val calendar = Calendar.getInstance()
        DatePickerDialog(
            context,
            { _, i, i2, i3 ->
                val times = getEndOfDayTransactions(
                    Calendar.getInstance().apply { set(i, i2, i3) }.timeInMillis
                )
                action(times[0], times[1])
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    fun getEndOfDayTransactions(timeInMillis: Long): Array<String> {
        val be: Long = getBeginningOfDay(timeInMillis)
        val be1: Long = Timestamp.from(Instant.ofEpochMilli(be).plusSeconds(86400)).time
        val df = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        val startTime = df.format(be)
        val endDate = df.format(be)

        return arrayOf(startTime, endDate)
    }
}
