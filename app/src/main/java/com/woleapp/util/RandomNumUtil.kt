package com.woleapp.util

import android.annotation.SuppressLint
import com.danbamitale.epmslib.entities.TransactionResponse
import com.danbamitale.epmslib.entities.isApproved
import com.woleapp.model.TransactionResponseX
import com.woleapp.model.Transactions
import com.woleapp.model.kayodeStormImplementation.GetBankListResponse
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object RandomNumUtil {
    fun generateRandomRrn(length: Int): String {
        val random = Random()
        var digits = ""
        digits += (random.nextInt(9) + 1).toString()
        for (i in 1 until length) {
            digits += (random.nextInt(10) + 0).toString()
        }
        return digits
    }

    @SuppressLint("ConstantLocale")
    val formattedTime =
        SimpleDateFormat("hh:mm:ss", Locale.getDefault()).format(System.currentTimeMillis())
            .format(Date())

    @SuppressLint("SimpleDateFormat")
    fun getDateFromMilliseconds(milliSecondsString: String): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'")

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSecondsString.toLong()
        return formatter.format(calendar.time)
    }

    @SuppressLint("SimpleDateFormat")
    fun getDateFromMilliseconds(milliSecondsString: Long): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm")

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSecondsString
        return formatter.format(calendar.time)
    }

    @SuppressLint("SimpleDateFormat")
    fun getDate(): String {
        val dateFormatter: DateFormat = SimpleDateFormat("ddMM")
        val today = Date()
        return dateFormatter.format(today)
    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentDateTime(): String {
        val dateFormatter: DateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val today = Date()
        return dateFormatter.format(today)
    }

    fun mapTransactionResponseToTransaction(transResp: TransactionResponse): Transactions {
        return Transactions().apply {
            amount = transResp.amount.toDouble().div(100)
            id = transResp.merchantId
            transaction_id = transResp.RRN
            reference_no_Etranzact = transResp.RRN
            transaction_type = transResp.transactionType.name
            transaction_date = transResp.transactionTimeInMillis.toString()
            description = "POS PAYMENT"
            beneficiary_name = transResp.cardHolder
            status = if (transResp.isApproved) "Success" else "Failed"
            destination_account = transResp.acquiringInstCode
        }
    }

    fun mapTransactionResponseToString(transResp: TransactionResponse) =
        "CardHolder: ${transResp.cardHolder} " +
            "\nCardType: ${transResp.cardLabel}" +
            "\nAmount: ${transResp.amount} " +
            "\nRRN: ${transResp.RRN} \n" +
            "ResponseCode: ${transResp.responseCode} \n" +
            "TransmissionDateTime: ${transResp.transmissionDateTime} \n" +
            "Time in Millis: ${transResp.transactionTimeInMillis}"

    @SuppressLint("SimpleDateFormat")
    fun getDate(milliSeconds: Long): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }

    fun mapDanbamitaleResponseToResponseX(input: TransactionResponse): TransactionResponseX {
        with(input) {
            return TransactionResponseX(
                AID = AID,
                rrn = RRN,
                STAN = STAN,
                TSI = TSI,
                TVR = TVR,
                accountType = accountType.name,
                acquiringInstCode = acquiringInstCode,
                additionalAmount_54 = additionalAmount_54,
                amount = amount.toInt(),
                appCryptogram = appCryptogram,
                authCode = authCode,
                cardExpiry = cardExpiry,
                cardHolder = cardHolder,
                cardLabel = cardLabel,
                id = id.toInt(),
                localDate_13 = localDate_13,
                localTime_12 = localTime_12,
                maskedPan = maskedPan,
                merchantId = merchantId,
                originalForwardingInstCode = originalForwardingInstCode,
                otherAmount = otherAmount.toInt(),
                otherId = otherId,
                responseCode = responseCode,
                responseDE55 = responseDE55 ?: "",
                terminalId = terminalId,
                transactionTimeInMillis = transactionTimeInMillis,
                transactionType = transactionType.name,
                transmissionDateTime = getCurrentDateTime()
            )
        }
    }

    fun sortListOfBanksAlphabetically(input: List<GetBankListResponse>) =
        input.sortedBy { it.bank_name }

    @SuppressLint("SimpleDateFormat")
    fun dateStr2Long(dateStr: String): Long {
        return try {
            val c = Calendar.getInstance()
            val newDate = dateStr.replace("T", " ").replace(".000Z", "")
            c.time = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .parse(newDate)!!
            c.timeInMillis
        } catch (e: ParseException) {
            e.printStackTrace()
            0
        }
    }
}
