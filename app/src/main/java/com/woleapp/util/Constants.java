package com.woleapp.util;

public interface Constants {
    String NEW_STORM_SERVICE_BASE_URL = "https://storm.app.netpluspay.com";
    String STRING_EOD_TITLE_HEADER = "         RRN           ST  TIME  AMOUNT";
    String ISW_TOKEN = "TokenForIsw";
    int STATE_PAYMENT_STAND_BY = 0;
    int STATE_PAYMENT_STARTED = 1;
    int STATE_PAYMENT_DONE_LOGGING_TO_BACKEND = 11;
    int STATE_PAYMENT_APPROVED = 2;
    int STATE_PAYMENT_APPROVED_BUT_NOT_LOGGED = 3;

    int PAGING_SOURCE_STARTING_PAGE_INDEX = 0;

    int ASCII_VALUE_OF_POINT = 46;
    int ASCII_VALUE_OF_ZERO = 48;
    int ASCII_VALUE_OF_COMMA = 44;
    int ASCII_VALUE_OF_SPACE = 32;

    int TRANSACTION_FUND_WALLET = 1;
    int TRANSACTION_CASH_IN = 2;
    int TRANSACTION_CASH_OUT = 3;
    int TRANSACTION_QUICK = 4;

    int USER_TYPE_AGENT = 1;
    int USER_TYPE_MERCHANT = 2;
    int USER_TYPE_NONE = -1;

    String PN_DATA = "Data";
    String PN_DATA_2 = "data";
    String PN_STATUS = "Status";
    String PN_MESSAGE = "message";
    String PREF_REPRINT_PASSWORD = "reprint_password";
    String MODE_OF_FETCHING_TRANSACTIONS_FROM_BACKEND = "endPointToGetTransactionsFrom";
    String FETCH_TRANSACTIONS_FROM_BACKEND_START_DATE = "fetchTransactionsStartDate";
    String FETCH_TRANSACTIONS_FROM_BACKEND_END_DATE = "fetchTransactionsEndDate";
    String FETCH_TRANSACTIONS_FROM_BACKEND_RRN = "fetchTransactionsRrn";
    String FETCH_VIA_DATE = "byDate";
    String FETCH_VIA_RRN = "byRrn";
    String FETCH_BY_DEFAULT = "byDefault";
    String PREF_PRINTER_SETTINGS = "pref_printer_settings";
    String PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY = "print_customer_copy_only";
    String PREF_VALUE_PRINT_CUSTOMER_AND_MERCHANT_COPY = "print_merchant_and_customer_copy";
    String PREF_VALUE_PRINT_ASK_BEFORE_PRINTING = "ask_before_printing";
    String PREF_VALUE_PRINT_SMS = "send_sms";

    String PN_ERROR = "error";
    String PN_DIRECTION = "direction";
    String PN_DIRECTION_VALUE = "request";
    String PN_REQUEST = "request";
    String PN_RESPONSE = "response";

    String PN_TERMINAL_ID = "terminalId";
    String PN_ACTION = "action";
    String PN_TRANSACTION = "transaction";
    String PN_PIN = "pin";

    String PN_REFERENCE = "reference";
    String PN_AMOUNT = "amount";
    String PN_CURRENCY = "currency";

    String PN_BANK_CODE = "bankCode";
    String PN_DESCRIPTION = "description";
    String PN_DESTINATION = "destination";

    String ACTION_ACCOUNT_QUERY = "AQ";
    String ACTION_FUNDS_TRANSFER = "FT";
    String ACTION_TRANSACTION_STATUS = "TS";
    String ACTION_PAY_BILL = "PB";
    String ACTION_VIRTUAL_TOPUP = "VTU";
    String ACTION_BALANCE_ENQUIRY = "BE";

    String DESTINATION_ENDPOINT_MOBILE = "M";
    String DESTINATION_ENDPOINT_ACCOUNT = "A";

    String ACCOUNT_NUMBER_SOURCE = "2063449787";
    String ACCOUNT_NUMBER_DESTINATION = "2077939278";
    String PREF_LAST_LOCATION = "pref_last_location";
}
