package com.woleapp.util;

import static com.woleapp.ui.fragments.MarketplaceProductListFragmentKt.MARKETPLACE_STORE;
import static com.woleapp.util.Constants.USER_TYPE_NONE;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pixplicity.easyprefs.library.Prefs;
import com.woleapp.model.Marketplace;
import com.woleapp.model.User;

import java.lang.reflect.Type;

public class SharedPrefManager {

    private static final String TAG_TOKEN = "push_token";

    public static void setMerchantStore(Marketplace marketplace) {
        Prefs.putString(MARKETPLACE_STORE, Singletons.getGsonInstance().toJson(marketplace));
    }

    public static Marketplace getMerchantStore() {
        return Singletons.getGsonInstance().fromJson(Prefs.getString(MARKETPLACE_STORE, null), Marketplace.class);
    }

    public static void setNetPlusPayConvenienceFee(Float netpluspayConvenienceFee) {
        Prefs.putFloat("netpluspay_convenience_fee", netpluspayConvenienceFee);
    }

    public static Float getNetPlusPayConvenienceFee() {
        return Prefs.getFloat("netpluspay_convenience_fee", 1.5f);
    }

    // Merchant Transaction Page
    public static void setNextMerchantTransactionsPage(int lastLoadedAgentTransactionsPage) {
        Prefs.putInt("LLMTP", lastLoadedAgentTransactionsPage);
    }

    public static int getNextMerchantTransactionsPage() {
        return Prefs.getInt("LLMTP", 0);
    }

    // Merchant Transaction page by Date
    public static void setNextMerchantTransactionsPageForByDate(int lastLoadedMerchantTransactionsPageForByDate) {
        Prefs.putInt("LLMTPBD", lastLoadedMerchantTransactionsPageForByDate);
    }

    public static int getNextMerchantTransactionsPageForByDate() {
        return Prefs.getInt("LLMTPBD", 0);
    }

    // Merchant Transaction page by RRN
    public static void setNextMerchantTransactionsPageForByRrn(int lastLoadedMerchantTransactionsPageForByRrn) {
        Prefs.putInt("LLMTPBRn", lastLoadedMerchantTransactionsPageForByRrn);
    }

    public static int getNextMerchantTransactionsPageForByRrn() {
        return Prefs.getInt("LLMTPBRn", 0);
    }

    // Agent Transaction page
    public static void setNextAgentTransactionsPage(int lastLoadedAgentTransactionsPage) {
        Prefs.putInt("LLATP", lastLoadedAgentTransactionsPage);
    }

    public static int getNextAgentTransactionsPage() {
        return Prefs.getInt("LLATP", 0);
    }

    // Agent Transaction page by Date
    public static void setNextAgentTransactionsPageForByDate(int lastLoadedAgentTransactionsPageForByDate) {
        Prefs.putInt("LLATPBD", lastLoadedAgentTransactionsPageForByDate);
    }

    public static int getNextAgentTransactionsPageForByDate() {
        return Prefs.getInt("LLATPBD", 0);
    }

    // Agent Transaction page by RRN
    public static void setNextAgentTransactionsPageForByRrn(int lastLoadedAgentTransactionsPageForByRrn) {
        Prefs.putInt("LLATPBRn", lastLoadedAgentTransactionsPageForByRrn);
    }

    public static int getNextAgentTransactionsPageForByRrn() {
        return Prefs.getInt("LLATPBRn", 0);
    }

    public static void setLoginStatus(boolean status) {
        if (!status) {
            setUser(null);
        }
        Prefs.putBoolean("is_login", status);
    }

    public static boolean isLogin() {
        return Prefs.getBoolean("is_login", false);
    }

    public static void setAppToken(String appToken) {
        Prefs.putString("app_token", appToken);
    }

    public static String getAppToken() {
        return Prefs.getString("app_token", null);
    }

    public static void setAppTokenForNewStormService(String appToken) {
        Prefs.putString("app_token_for_new_storm_service", appToken);
    }

    public static String getAppTokenForNewStormService() {
        return Prefs.getString("app_token_for_new_storm_service", null);
    }

    public static String getLastLoggedInUser() {
        return Prefs.getString("last_logged_in_user", null);
    }

    public static void setLastLoggedInUser(String lastLoggedInUserId) {
        Prefs.putString("last_logged_in_user", lastLoggedInUserId);
    }

    public static void setUserType(int loginType) {
        Prefs.putInt("user_login_type", loginType);
    }

    public static int getUserType() {
        return Prefs.getInt("user_login_type", USER_TYPE_NONE);
    }

    public static void setXapiKey(String xapiKey) {
        Prefs.putString("service_x_api_key", xapiKey);
    }

    public static String getXapiKey() {
        return Prefs.getString("service_x_api_key", "");
    }

    public static Boolean hasAppToken() {
        return Prefs.getString("app_token", null) != null;
    }

    public static void setUserToken(String userToken) {
        Prefs.putString("user_token", userToken);
    }

    public static String getUserToken() {
        return Prefs.getString("user_token", null);
    }

    public static void setPOSConvenienceFee(Float convenience_fee) {
        Prefs.putFloat("pos_convenience_fee", convenience_fee);
    }

    public static Float getPOSConvenienceFee() {
        return Prefs.getFloat("pos_convenience_fee", 0.0f);
    }

    public static void setTransfeeConvenienceFee(Float convenience_fee) {
        Prefs.putFloat("transfer_convenience_fee", convenience_fee);
    }

    public static Float getTransfeeConvenienceFee() {
        return Prefs.getFloat("transfer_convenience_fee", 0.0f);
    }

    //this method will save the device token to shared preferences
    public static boolean saveDeviceToken(String token) {
        Prefs.putString(TAG_TOKEN, token);
        return true;
    }

    //this method will fetch the device token from shared preferences
    public static String getDeviceToken() {
        return Prefs.getString(TAG_TOKEN, null);
    }


    public static User getUser() {
        String userJSONString = Prefs.getString("user", "");
        if (TextUtils.isEmpty(userJSONString))
            return null;
        Type type = new TypeToken<User>() {
        }.getType();
        User user = new Gson().fromJson(userJSONString, type);
        return user;
    }

    public static void temp(String temp) {
        Prefs.putString("temp", temp);
    }

    public static String getTemp() {
        return Prefs.getString("temp", null);
    }

    public static void setUser(User user) {
        String userJSONString = new Gson().toJson(user);
        Prefs.putString("user", userJSONString);
    }
}
