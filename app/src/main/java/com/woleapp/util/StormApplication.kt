package com.woleapp.util

import android.content.pm.ApplicationInfo
import android.util.Log
import androidx.multidex.MultiDexApplication
import androidx.work.* // ktlint-disable no-wildcard-imports
import com.github.piasy.biv.BigImageViewer
import com.github.piasy.biv.loader.glide.GlideCustomImageLoader
import com.github.piasy.biv.loader.glide.GlideModel
import com.google.firebase.FirebaseApp
import com.netpluspay.netpossdk.NetPosSdk.init
import com.netpluspay.netpossdk.NetPosSdk.loadProvidedCapksAndAids
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.pixplicity.easyprefs.library.Prefs
import com.woleapp.model.Inventory
import com.woleapp.work.PushToBackendWorker
import com.woleapp.work.StormWork
import dagger.hilt.android.HiltAndroidApp
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber
import timber.log.Timber.DebugTree
import java.util.concurrent.TimeUnit

/**
 * Created by asus on 13-Jul-18.
 */
@HiltAndroidApp
class StormApplication : MultiDexApplication() {
    private val disposable: CompositeDisposable = CompositeDisposable()

    // private SharedPreferences appPreferences;
    // private Post post;
    var isCheck_new_session = false
    var selectedInventory: Inventory? = null

    // private String mGlobalCheckButton = null;
    override fun onCreate() {
        super.onCreate()
        instance = this
        FirebaseApp.initializeApp(this)
        Prefs.Builder()
            .setContext(this)
            .setMode(MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
        init()
        if (!Prefs.contains("load_provided")) {
            loadProvidedCapksAndAids()
            Prefs.putBoolean("load_provided", true)
        }
        Timber.plant(DebugTree())
        RxJavaPlugins.setErrorHandler { throwable: Throwable ->
            throwable.printStackTrace()
            Timber.e("%s", throwable.localizedMessage)
        }
        // Setup transaction data synchronization with the backend
        val pushToBackendWorkRequest: PeriodicWorkRequest =
            PeriodicWorkRequestBuilder<PushToBackendWorker>(15, TimeUnit.MINUTES)
                .setInitialDelay(15, TimeUnit.MINUTES)
                .setConstraints(
                    Constraints.Builder()
                        .setRequiredNetworkType(
                            NetworkType.CONNECTED
                        ).build()
                ).build()
        WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
            PushToBackendWorker::class.java.simpleName,
            ExistingPeriodicWorkPolicy.REPLACE,
            pushToBackendWorkRequest
        )

        // setupLeakCanary();
        val stormWorkRequest: PeriodicWorkRequest =
            PeriodicWorkRequestBuilder<StormWork>(1, TimeUnit.HOURS)
                .setInitialDelay(30, TimeUnit.MINUTES)
                .setConstraints(
                    Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build()
                )
                .build()
        WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
            StormWork::class.java.simpleName,
            ExistingPeriodicWorkPolicy.REPLACE,
            stormWorkRequest
        )

        setupLogLevel()
        BigImageViewer.initialize(
            GlideCustomImageLoader.with(
                applicationContext,
                GlideModel::class.java
            )
        )
        // ProcessLifecycleOwner.get().getLifecycle().addObserver(new AppLifeCycleObserver());
        // setupMainSettings();
    }

    /*public static Boolean isInDebugMode() {
        return BuildConfig.DEBUG;
    }*/
    /*private void setupLeakCanary() {
        if (LeakCanary.INSTANCE.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        LeakCanary.install(this);
        // Normal app init code...
    }*/

    override fun onTerminate() {
        super.onTerminate()
        disposable.clear()
    }

    private fun setupLogLevel() {
        if (isInDebugMode) {
            Logger.addLogAdapter(AndroidLogAdapter())
        }
    }

    private val isInDebugMode: Boolean
        private get() {
            val isDebug = this.applicationInfo.flags and
                ApplicationInfo.FLAG_DEBUGGABLE != 0
            Log.e("isDebugMode: ", "$isDebug--")
            return isDebug
        }

    companion object {
        /*private void setupMainSettings() {
        appPreferences = getSharedPreferences(AppConstant.APP_PREF_FILE, MODE_PRIVATE);
        post = new PostImpl(this);

    }

    public SharedPreferences getAppPreferences() {
        return appPreferences;
    }


    public Post getPost() {
        return post;
    }

*/
        @JvmStatic
        @get:Synchronized
        var instance: StormApplication? = null
            private set
    }
}
