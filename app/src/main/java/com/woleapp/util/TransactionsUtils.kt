package com.woleapp.util

import android.content.Context
import android.widget.Toast
import androidx.paging.PagedList
import com.danbamitale.epmslib.entities.TransactionResponse
import com.danbamitale.epmslib.entities.TransactionType
import com.danbamitale.epmslib.utils.IsoAccountType
import com.woleapp.model.TrackTransactionTable
import com.woleapp.model.Transactions
import com.woleapp.model.kayodeStormImplementation.FinalDataClassForSearch
import com.woleapp.model.kayodeStormImplementation.GetNewResponse
import com.woleapp.model.kayodeStormImplementation.GetTransactionsResponse
import com.woleapp.util.RandomNumUtil.dateStr2Long
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

object TransactionsUtils {
    fun printEndOfDay(transactions: PagedList<Transactions>, context: Context) {
        val isEndOfDay = transactions.groupBy {
            it.transaction_date.split("T").first()
        }.size == 1

        transactions.toList().map {
            transactionsToTransactionResponse(it)
        }.toList().printEndOfDay(context, isEndOfDay)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ printResp ->
                Timber.e(printResp.toString())
            }, { err ->
                Toast.makeText(
                    context,
                    err.localizedMessage,
                    Toast.LENGTH_LONG
                )
                    .show()
            }).disposeWith(CompositeDisposable())
    }

    fun transactionsToTransactionResponse(trans: Transactions): TransactionResponse =
        TransactionResponse().apply {
            this.AID = ""
            RRN = trans.reference_no_Etranzact
            STAN = ""
            TSI = ""
            TVR = ""
            accountType = IsoAccountType.parseStringAccountType(trans.transaction_type)
            acquiringInstCode = ""
            additionalAmount_54 = ""
            amount = (trans.amount).toLong()
            appCryptogram = ""
            authCode = ""
            cardExpiry = ""
            cardHolder = ""
            cardLabel = ""
            id = 0L
            localDate_13 = ""
            localTime_12 = ""
            maskedPan = ""
            merchantId = SharedPrefManager.getUser().netplus_id
            originalForwardingInstCode = ""
            otherAmount = 0
            otherId = ""
            responseCode = if (trans.status.contains("approved", true)) "00" else "99"
            responseDE55 = ""
            terminalId = SharedPrefManager.getUser().terminal_id
            transactionTimeInMillis = dateStr2Long(trans.transaction_date)
            transactionType = if (trans.transaction_type.contains(
                    "credit",
                    true
                )
            ) TransactionType.PURCHASE else TransactionType.TRANSFER
            transmissionDateTime = (trans.transaction_date).replace("T", " ").replace(".000Z", "")
        }

    // Map GetTransactions object to an object of TransactionResponse
    fun GetNewResponse.mapToGetTransactionResp() =
        GetTransactionsResponse(transaction.map { it.toTransactionNormalStructure() })

    fun FinalDataClassForSearch.mapToGetTransactionResp() =
        GetTransactionsResponse(
            listOf(
                transaction.toTransactionNormalStructure()
            )
        )

    fun TrackTransactionTable.mapTransactionToTransactionResponse() {
        TransactionResponse(
            this.transactionType,
            maskedPan,
            amount,
            transmissionDateTime,
            STAN,
            RRN,
            localTime_12,
            localDate_13,
            otherAmount,
            acquiringInstCode,
            originalForwardingInstCode,
            authCode,
            responseCode,
            additionalAmount_54
        )
    }

    fun TransactionResponse.mapTransactionToTransactionResponse(
        transactionStatus: String,
        routingChannel: String,
        pushStatus: Boolean
    ) =
        TrackTransactionTable(
            this.transactionType,
            maskedPan,
            amount,
            transmissionDateTime,
            STAN,
            RRN,
            localTime_12,
            localDate_13,
            otherAmount,
            acquiringInstCode,
            originalForwardingInstCode,
            authCode,
            responseCode,
            additionalAmount_54,
            echoData,
            cardLabel,
            cardExpiry,
            cardHolder,
            TVR,
            TSI,
            AID,
            appCryptogram,
            transactionTimeInMillis,
            accountType,
            terminalId,
            merchantId,
            otherId,
            source = this.source,
            interSwitchThreshold = this.interSwitchThreshold,
            responseDE55 = this.responseDE55,
            push_to_backendStatus = pushStatus,
            transactionStatus = transactionStatus,
            routingChannel = routingChannel
        )
}
