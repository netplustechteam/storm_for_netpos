package com.woleapp.util;

import android.content.Context;
import android.text.TextUtils;

public class PreferredTerminalUtils {

    PreferenceHelper mPreferenceHelper;
    String DEFAULT_DEVICE;

    public PreferredTerminalUtils(Context context, String merchantCode, String operatorCode) {
        mPreferenceHelper = new PreferenceHelper(context);
        DEFAULT_DEVICE = merchantCode + operatorCode;
    }

    public String getPreferredTerminalName() {
        String defaultTerminal = mPreferenceHelper.getString(DEFAULT_DEVICE);
        if (TextUtils.isEmpty(defaultTerminal)) {
            return "";
        } else {
            return defaultTerminal;
        }
    }


    public void clearTerminal() {
        mPreferenceHelper.putString(DEFAULT_DEVICE, "");
    }
}
