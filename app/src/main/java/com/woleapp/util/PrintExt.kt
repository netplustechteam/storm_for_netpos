package com.woleapp.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.widget.Toast
import com.danbamitale.epmslib.entities.TransactionResponse
import com.danbamitale.epmslib.entities.responseMessage
import com.danbamitale.epmslib.extensions.formatCurrencyAmount
import com.danbamitale.epmslib.extensions.formatCurrencyAmountTrimSpace
import com.netpluspay.netpossdk.NetPosSdk
import com.netpluspay.netpossdk.printer.PrinterResponse
import com.netpluspay.netpossdk.printer.ReceiptBuilder
import com.netpluspay.netpossdk.utils.DeviceConfig
import com.pos.sdk.printer.POIPrinterManage
import com.pos.sdk.printer.models.BitmapPrintLine
import com.pos.sdk.printer.models.PrintLine
import com.pos.sdk.printer.models.TextPrintLine
import com.woleapp.BuildConfig
import com.woleapp.R
import com.woleapp.kozenext.formatDate
import com.woleapp.util.Constants.STRING_EOD_TITLE_HEADER
import com.woleapp.util.RandomNumUtil.getDateFromMilliseconds
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.Single
import io.reactivex.SingleEmitter
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.* // ktlint-disable no-wildcard-imports

fun newEndOfDayPrintImplementation(
    index: Int,
    it: TransactionResponse,
    printerManager: POIPrinterManage,
    textPrintLine: TextPrintLine,
) {
    val status = if (it.responseCode == "00") {
        if (it.RRN.startsWith("F", true)) "APPROVED" else "A"
    } else {
        if (it.RRN.startsWith("F", true)) "DECLINED" else "D"
    }
    val formattedAmount = it.amount.formatCurrencyAmountTrimSpace("\u20A6")
    val formattedTime =
        getDateFromMilliseconds(it.transactionTimeInMillis).split("T").last()
    textPrintLine.apply {
        content = if (it.RRN.startsWith(
                "F",
                true
            )
        ) "${it.RRN}  \n$status  $formattedTime $formattedAmount" else "${it.RRN}  $status   $formattedTime  $formattedAmount"
    // "$formattedAmount  $status   $formattedTime\n${it.RRN}" else "$formattedAmount   $status    ${it.RRN}  $formattedTime"
    }
    printerManager.appendTextEntity(textPrintLine)

    if (index >= 9) {
        if (index == 9) {
            drawStraightLine(printerManager, textPrintLine)
        } else {
            if (index > 10 && index % 10 == 0) {
                drawStraightLine(printerManager, textPrintLine)
            }
        }
    }
}

private fun drawStraightLine(
    printerManager: POIPrinterManage,
    textPrintLine: TextPrintLine
) {
    textPrintLine.apply {
        content = "-----------------------------------------------"
    }
    printerManager.appendTextEntity(textPrintLine)
}

fun previousEndOfDayPrintImplementation(
    index: Int,
    it: TransactionResponse,
    printerManager: POIPrinterManage,
    textPrintLine: TextPrintLine
) {
    textPrintLine.apply {
        isBold = true
        content = if (it.responseCode == "00") "Approved" else "Declined"
    }
    printerManager.appendTextEntity(textPrintLine)
    textPrintLine.apply {
        isBold = false
        content = "Amount: ${it.amount.formatCurrencyAmount("\u20A6")}"
    }
    printerManager.appendTextEntity(textPrintLine)
    textPrintLine.apply {
        content = "RRN: ${it.RRN}"
    }
    printerManager.appendTextEntity(textPrintLine)
    textPrintLine.apply {
        content = "Date: ${it.transactionTimeInMillis.formatDate()}"
    }
    printerManager.appendTextEntity(textPrintLine)
    drawStraightLine(printerManager, textPrintLine)
}

fun List<TransactionResponse>.printEndOfDay(
    context: Context,
    isEndOfDay: Boolean /* whether we are printing EndOfDay or we are just printing all transactions without*/
): Single<PrinterResponse> {
    if (!(this.isEmpty() || this == null)) {
        if (Build.MODEL.equals("mini", true) || Build.MODEL.equals("p5", true))
            return Single.error(Throwable("Device cannot print"))

        val printerManager = NetPosSdk.getPrinterManager(context).apply {
            if (DeviceConfig.Device == DeviceConfig.DEVICE_PRO) {
                try {
                    setPrintGray(Integer.valueOf("5000"))
                    setLineSpace(Integer.valueOf("1"))
                    cleanCache()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    Timber.e("Error: ${e.localizedMessage}")
                }
            }
        }
        val textPrintLine = TextPrintLine().apply {
            type = TextPrintLine.TEXT
        }
        var amountApproved: Long = 0
        var amountDeclined: Long = 0

        val bitmapPrintLine = BitmapPrintLine()
        bitmapPrintLine.type = PrintLine.BITMAP
        bitmapPrintLine.position = PrintLine.CENTER
        val bitmap: Bitmap =
            BitmapFactory.decodeResource(context.resources, R.drawable.logo)
        bitmapPrintLine.bitmap = Bitmap.createScaledBitmap(bitmap, 180, 120, false)
        printerManager.addPrintLine(bitmapPrintLine)

        var emitter: SingleEmitter<PrinterResponse>? = null

        if (isEndOfDay) {
            textPrintLine.apply {
                isBold = false
                content = "EOD FOR: ${
                SimpleDateFormat(
                    "yyyy-MM-dd",
                    Locale.getDefault()
                ).format(this@printEndOfDay.first().transactionTimeInMillis)
                }"
            }
            printerManager.appendTextEntity(textPrintLine)
        }

        textPrintLine.apply {
            isBold = false
            content = "Terminal ID: ${this@printEndOfDay.first().terminalId}"
        }
        printerManager.appendTextEntity(textPrintLine)

        textPrintLine.apply {
            isBold = false
            content = "MID: ${this@printEndOfDay.first().merchantId}"
        }
        printerManager.appendTextEntity(textPrintLine)

        drawStraightLine(printerManager, textPrintLine)

        if (isEndOfDay) {
            textPrintLine.apply {
                content = STRING_EOD_TITLE_HEADER // "AMOUNT   S          RRN          TIME"
            }
            printerManager.appendTextEntity(textPrintLine)
            textPrintLine.apply {
                position = PrintLine.LEFT
                content = "-----------------------------------------------"
            }
            printerManager.appendTextEntity(textPrintLine)
        }

        forEachIndexed { index, it ->
            if (it.responseCode == "00") amountApproved =
                amountApproved.plus(it.amount) else amountDeclined =
                amountDeclined.plus(it.amount)
            if (isEndOfDay) newEndOfDayPrintImplementation(
                index,
                it,
                printerManager,
                textPrintLine
            ) else previousEndOfDayPrintImplementation(index, it, printerManager, textPrintLine)
        }
        textPrintLine.apply {
            position = PrintLine.CENTER
            content = "SUMMARY"
        }
        printerManager.appendTextEntity(textPrintLine)
        textPrintLine.apply {
            position = PrintLine.LEFT
            content = "-----------------------------------------------"
        }
        printerManager.appendTextEntity(textPrintLine)

        textPrintLine.apply {
            content = "Approved: ${amountApproved.formatCurrencyAmount("\u20A6")}"
        }
        printerManager.appendTextEntity(textPrintLine)
        textPrintLine.apply {
            content = "Declined: ${amountDeclined.formatCurrencyAmount("\u20A6")}"
        }
        printerManager.appendTextEntity(textPrintLine)

        textPrintLine.apply {
            content = " \n\n\n"
        }
        printerManager.appendTextEntity(textPrintLine)

        val printerListener = object : POIPrinterManage.IPrinterListener {
            override fun onError(p0: Int, p1: String?) {
                emitter?.let {
                    if (it.isDisposed.not())
                        it.onError(Throwable("message: $p1 - code: $p0"))
                }
            }

            override fun onFinish() {
                emitter?.let {
                    if (it.isDisposed.not())
                        it.onSuccess(PrinterResponse())
                }
            }

            override fun onStart() {
                Timber.e("Printing started")
            }
        }
        // printerManager.addPrintLine(listOfTextPrintLine)
        return Single.create {
            emitter = it
            printerManager.beginPrint(printerListener)
        }
    } else {
        Toast.makeText(context, R.string.nothingToPrint, Toast.LENGTH_LONG).show()
        return Single.just(PrinterResponse())
    }
}

fun POIPrinterManage.appendTextEntity(printLine: TextPrintLine) {
    addPrintLine(printLine)
}

fun List<TransactionResponse>.printAll(
    context: Context,
    isMerchantCopy: Boolean
): Observable<PrinterResponse> {
    var emitter: ObservableEmitter<PrinterResponse>? = null
    val printerListener = object : POIPrinterManage.IPrinterListener {
        override fun onError(p0: Int, p1: String?) {
            emitter?.let {
                if (it.isDisposed.not())
                    it.onError(Throwable("message: $p1 - code: $p0"))
            }
        }

        override fun onFinish() {
            emitter?.let {
                if (it.isDisposed.not())
                    it.onNext(PrinterResponse())
            }
        }

        override fun onStart() {
            Timber.e("Printing started")
        }
    }
    return Observable.create {
        emitter = it
        forEach { transactionResponse ->
            if (it.isDisposed.not())
                transactionResponse.print(printerListener, context, isMerchantCopy)
        }
        it.onComplete()
    }
}

fun TransactionResponse.print(
    printerListener: POIPrinterManage.IPrinterListener,
    context: Context,
    isMerchantCopy: Boolean = true
) {
    buildReceipt(context, isMerchantCopy).print(printerListener)
}

fun TransactionResponse.print(
    context: Context,
    remark: String? = null,
    isMerchantCopy: Boolean = false,
    isReprint: Boolean = false
) =
    buildReceipt(
        remark = remark,
        context = context,
        isMerchantCopy = isMerchantCopy,
        isReprint = isReprint
    ).print()

fun TransactionResponse.builder() = StringBuilder().apply {
    append("Merchant Name: ").append(Singletons.getCurrentlyLoggedInUser()!!.business_name)
    append("\nTERMINAL ID: ").append(terminalId).append("\n")
    append(transactionType).append("\n")
    append("DATE/TIME: ").append(transactionTimeInMillis.formatDate()).append("\n")
    append("AMOUNT: ").append(amount.div(100).formatCurrencyAmount("\u20A6")).append("\n")
    append(cardLabel).append(" Ending with ").append(maskedPan.substring(maskedPan.length - 4))
        .append("\n")
    append("RESPONSE CODE: ").append(responseCode).append("\n").append(
        " : ${
        try {
            responseMessage
        } catch (ex: Exception) {
            "Error"
        }
        }"
    )
}

fun TransactionResponse.buildSMSText(s: String? = null): StringBuilder = StringBuilder().apply {
    append("POS $transactionType ${if (responseCode == "00") "Approved" else "Declined"}\n\n")
    append("Response Code: $responseCode\n")
    append(
        "Message: ${
        try {
            responseMessage
        } catch (e: java.lang.Exception) {
            ""
        }
        }\n"
    )
    append("Amount: ${amount.div(100).formatCurrencyAmount("\u20A6")}\n")
    append("Date/Time: ${transactionTimeInMillis.formatDate()}\n")
    s?.let {
        append("Remark: $it\n")
    }
    append("Auth Code: $authCode\n")
    append("RRN: $RRN\n")
    append("STAN: $STAN\n")
    append("Card: $cardLabel - $maskedPan\n")
    append("Card Owner: $cardHolder\n")
    append("Merchant: ${Singletons.getCurrentlyLoggedInUser()?.business_name}\n")
    append("Terminal ID: $terminalId\n")
}

fun TransactionResponse.buildReceipt(
    context: Context,
    isMerchantCopy: Boolean = false,
    remark: String? = null,
    isReprint: Boolean = false
) =
    ReceiptBuilder(
        NetPosSdk.getPrinterManager(context).apply {
            cleanCache()
            setPrintGray(2000)
            setLineSpace(1)
        }
    ).also { builder ->
        builder.appendLogo(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.ic_print_logo
            )
        )
        builder.appendAID(AID)
        builder.appendMerchantName(Singletons.getCurrentlyLoggedInUser()!!.business_name)
        builder.appendAmount(
            amount.div(100).formatCurrencyAmount("\u20A6")
        )
        remark?.let {
            builder.appendRemark(it)
        }
        builder.appendAppName("NetPOS")
        builder.appendAppVersion(BuildConfig.VERSION_NAME)
        builder.appendAuthorizationCode(authCode)
        builder.appendCardHolderName(cardHolder)
        builder.appendCardNumber(maskedPan)
        builder.appendCardScheme(cardLabel)
        builder.appendDateTime(transactionTimeInMillis.formatDate())
        builder.appendRRN(RRN)
        builder.appendStan(STAN)
        builder.appendTerminalId(terminalId)
        builder.appendTransactionType(transactionType.name)
        builder.appendTransactionStatus(if (responseCode == "00") "Approved" else "Declined")
        builder.appendResponseCode(
            "${responseCode}\nMessage: ${
            try {
                responseMessage
            } catch (ex: Exception) {
                "Error"
            }
            }"
        )
        builder.isReprint = isReprint
        if (isMerchantCopy)
            builder.isMerchantCopy
        else builder.isCustomerCopy
    }
