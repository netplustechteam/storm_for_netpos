package com.woleapp

import androidx.paging.DataSource
import com.google.gson.JsonObject
import com.woleapp.db.dao.TransactionsDao
import com.woleapp.model.GetPartnerInterSwitchThresholdResponse
import com.woleapp.model.TrackTransactionTable
import com.woleapp.model.Transactions
import com.woleapp.model.kayodeStormImplementation.*
import com.woleapp.network.GetThresholdService
import com.woleapp.network.NewStormApiService
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(
    @Named("defaultApiService") private val newStormApiService: NewStormApiService,
    @Named("getThresholdService") private val thresholdApiService: GetThresholdService,
    private val transactionsDao: TransactionsDao
) {
    fun loginUser(loginCredentials: StormLogin): Observable<Response<StormLoginResponse>> =
        newStormApiService.loginUser(loginCredentials)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun sendTransactionToServer(
        stormId: String,
        payload: JsonObject
    ): Single<Response<JsonObject>> =
        newStormApiService.sendTransactionToServer(stormId, payload)

    fun registerNewUser(
        payload: JsonObject
    ): Observable<Response<Any>> =
        newStormApiService.registerNewUser(payload)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getWalletBalanceFromNewStormService(
        stormId: String
    ): Observable<Response<GetStormWalletBalanceResponseBody>> =
        newStormApiService.getWalletBalanceFromNewStormService(stormId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getListOfBanksSupportedByeTransact(): Observable<Response<List<GetBankListResponse>>> =
        newStormApiService.getListOfBanksSupportedByeTransact()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun verifyAccountName(
        payload: VerifyAccountNameRequestBody
    ): Observable<Response<VerifyAccountNameResponseBody>> =
        newStormApiService.verifyAccountName(payload)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun debitUserWallet(
        stormId: String,
        payload: DebitUserWalletRequestBody
    ): Single<Response<DebitUserWalletResponseBody>> =
        newStormApiService.debitUserWallet(stormId, payload)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    // Local database implementation starts here
    fun getTransactions(): DataSource.Factory<Int, Transactions> =
        transactionsDao.getTransactions()

    fun insertTransactions(transactions: List<Transactions>): Single<List<Long>> =
        transactionsDao.insertTransactions(transactions)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun insertSingleTransaction(transactions: Transactions): Single<Long> =
        transactionsDao.insertSingleTransaction(transactions)

    fun insertSingleTransactionForLaterTracking(transactionForTracking: TrackTransactionTable): Single<Long> =
        transactionsDao.insertSingleTransactionForLaterTracking(transactionForTracking)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getAllTrackingTransactions(): List<TrackTransactionTable> =
        transactionsDao.getAllTrackingTransactions()

    fun deleteOldTransactions(): Single<Int> = deleteOldTransactions()

    fun getThreshold(): Single<GetPartnerInterSwitchThresholdResponse?> {
        val pidToUse = if (BuildConfig.FLAVOR.contains(
                "agency",
                true
            )
        ) BuildConfig.STRING_STORM_FOR_AGENT_PID else BuildConfig.STRING_STORM_FOR_MERCHANT_PID
        return thresholdApiService.getPartnerInterSwitchThreshold(pidToUse)
    }

    fun getTransactionFromBackend(stormId: String, page: Int): Single<GetTransactionsResponse> =
        newStormApiService.getDebitTransactions(stormId, page)

    fun getTransactionFromBackendByDate(
        stormId: String,
        page: Int,
        startDate: String,
        endDate: String
    ): Single<GetNewResponse> =
        newStormApiService.searchTransactionsByDate(stormId, page, startDate, endDate)

    fun getTransactionFromBackendByRrn(
        stormId: String,
        page: Int,
        rrn: String,
    ): Single<FinalDataClassForSearch> =
        if (rrn.contains("FT")) newStormApiService.searchTransactionsByRrnOrReference(
            stormId,
            page,
            "",
            rrn
        ) else newStormApiService.searchTransactionsByRrnOrReference(stormId, page, rrn, "")
}
